FB_LOG = 'fb-log';

FB_TOKEN = '';

function appendFBLog(text) {
    FB_LOG.value = text + '\r\n----\r\n' + FB_LOG.value;
}

function facebookInitialize() {
    FB_LOG = document.getElementById(FB_LOG);

    // window.facebookAsyncInit = function() {
        FB.init({
            appId: '479552689258009',
            autoLogAppEvents: true,
            xfbml: true,
            version: 'v3.3',
        });

        FB.getLoginStatus(function(response) {
            // Called after the JS SDK has been initialized.
            facebookStatusChangeCallback(response); // Returns the login status.
        });
    // };

    FB_TOKEN = localStorage.getItem('FB_TOKEN');
    appendFBLog(JSON.stringify(FB_TOKEN));
}

function facebookQueryUserData() {
    FB.api('/me?fields=name,email', function(response) {
        appendFBLog(JSON.stringify(response));
    });
}

function facebookCheckLoginState() {
    FB.getLoginStatus(function(response) {
        facebookStatusChangeCallback(response);
    });
}

function facebookStatusChangeCallback(response) {
    console.log('[FB] statusChangeCallback');
    console.log(response);

    if (response.status === 'connected') {
        FB_TOKEN = response.authResponse.accessToken;
        appendFBLog(JSON.stringify(response));
        appendFBLog(FB_TOKEN);

        localStorage.setItem('FB_TOKEN', FB_TOKEN);
    } else {
        appendFBLog('Please log into facebook.');
    }
}

function onFacebookLogin() {
    FB.login(
        function(response) {
            facebookStatusChangeCallback(response);       
        },
        { scope: 'public_profile, email' },
    );
}
