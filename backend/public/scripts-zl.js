ZL_LOG = 'zl-log';
ZL_REDIRECT_URL = 'https://honda.stdio.vn/login';

function appendZLLog(text) {
    ZL_LOG.value = text + '\r\n----\r\n' + ZL_LOG.value;
}

function zaloInitialize() {
    ZL_LOG = document.getElementById(ZL_LOG);

    console.log('Zalo initialize');

    Zalo.init({
        version: '2.0',
        appId: '2970099256358239421',
        redirectUrl: ZL_REDIRECT_URL,
    });
    Zalo.getLoginStatus(zaloStatusChangeCallback);
}

function zaloStatusChangeCallback(response) {
    if (response.status === 'connected') {
        appendZLLog(JSON.stringify(response));
        Zalo.api(
            '/me',
            'GET',
            {
                fields: 'id,name',
            },
            function(response) {
                appendZLLog(JSON.stringify(response));
            },
        );
    } else {
        appendZLLog('Please login Zalo first');
    }
}

function onZaloLogin() {
    Zalo.login('', 'get_profile,get_friends,send_message,post_feed');
}
