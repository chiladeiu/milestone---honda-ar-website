import admin, { auth, firestore, CollectionReference, DocumentReference } from '../firebase';
import axios from 'axios';
import moment from 'moment';
import { DELAY_INIT, EVENT_BEGIN_DATE, CURRENT_WEEK_ID, CURRENT_WEEK_DATA, FB_APP_TOKEN } from '../config';
import { FacebookAPI } from '../apis/facebook';
import { Video, User } from '../data-type';
import { VideosService } from './videos';
import _ from 'lodash';
import { ZaloZPI } from '../apis/zalo';
import { HEAD_DATA } from '../head-data';

const USER_ALLOW_FIELDS = ['name', 'phone', 'email', 'facebookName', 'facebookEmail', 'facebookId', 'createdAt'];

class UsersService {
    isInitialized: boolean;

    usersCollection: CollectionReference;
    facebookConfigDocument: DocumentReference;

    facebookAppToken: string;
    facebookConfig: any;

    constructor() {
        console.log('[US] Construct User Service');
        this.isInitialized = false;
        this.facebookConfig = {};
    }

    async initialize() {
        if (this.isInitialized) return;

        console.log('[US] Initialize User Service');

        this.usersCollection = firestore.collection('users');
        // this.facebookConfigDocument = firestore.collection('config').doc('facebook');

        // try {
        //     const doc = await this.facebookConfigDocument.get();

        //     if (!doc.exists) {
        //         const { access_token, token_type } = (await FacebookAPI.requestAccessToken()).data;

        //         this.facebookConfig.appToken = access_token;
        //         await this.facebookConfigDocument.set({ ...this.facebookConfig });
        //     } else {
        //         this.facebookConfig = doc.data();
        //     }

        //     console.log('this.facebookConfig ', this.facebookConfig);
        // } catch (error) {
        //     console.log('Error', error);
        // }

        this.isInitialized = true;
    }

    async prepareFacebookLoginToken(fbToken: string) {
        // Validate and get basic user info
        const validateData = (await FacebookAPI.verifyToken(fbToken, FB_APP_TOKEN)).data.data;
        const { is_valid, user_id, error } = validateData;
        if (!is_valid) {
            console.log('Invalid FB token');
            throw Error(error);
        }

        const userId = `fb${user_id}`;

        try {
            await auth.getUser(userId);
        } catch (error) {
            console.log('Error code: ', error.code);

            if (error.code === 'auth/user-not-found') {
                console.log(`FB User with id ${userId} not exist. Create one`);
                await auth.createUser({ uid: userId });

                const userData = (await FacebookAPI.queryUserData(fbToken)).data;
                console.log('FB user data', userData);
                const { id, name = '', email = '', first_name = '', last_name = '' } = userData;
                await this.usersCollection.doc(userId).set({
                    facebookId: id,
                    facebookName: name,
                    facebookEmail: email,
                    facebookFirstName: first_name,
                    facebookLastName: last_name,
                    createdAt: moment().unix(),
                });

                await auth.updateUser(userId, { displayName: name });
            }
        }

        return await auth.createCustomToken(userId);
    }

    // NOT USE ZALO
    // async prepareZaloLoginToken(zlToken: string) {
    //     // Validate and get basic user info
    //     const userData = (await ZaloZPI.queryUserData(zlToken)).data;
    //     console.log('ZL Data', userData);
    //     const { id, name } = userData;

    //     if (_.isEmpty(id)) {
    //         throw new Error('Invalid id. Login error');
    //     }

    //     const userId = `zl${id}`;

    //     try {
    //         await auth.getUser(userId);
    //     } catch (error) {
    //         console.log('Error code: ', error.code);
    //         if (error.code === 'auth/user-not-found') {
    //             console.log(`Zalo User with id ${userId} not exist. Create one`);
    //             console.log('Zalo user data', userData);

    //             await auth.createUser({ uid: userId });
    //             await this.usersCollection.doc(userId).set({
    //                 zaloId: id,
    //                 zaloName: name,
    //                 createdAt: moment().unix(),
    //             });
    //         }
    //     }

    //     return await auth.createCustomToken(userId);
    // }

    async getAllUsers(filterEmptyUser = false) {
        let userRecords = null;

        if (filterEmptyUser)
            userRecords = await this.usersCollection
                .where('name', '>', '')
                .orderBy('name', 'asc')
                .orderBy('createdAt', 'desc')
                .get();
        else userRecords = await this.usersCollection.orderBy('createdAt', 'desc').get();

        return userRecords.docs.map((u: any) => ({
            id: u.id,
            ...u.data(),
        }));
    }

    async getAllUsersAsDict() {
        const userRecords = await this.usersCollection.get();
        return userRecords.docs.reduce((acc: any, user: any) => {
            acc[user.id] = {
                id: user.id,
                ...user.data(),
            };
            return acc;
        }, {});
    }

    async getUser(userId: string) {
        const userRecord = await this.usersCollection.doc(userId).get();
        if (!userRecord.exists) throw new Error('User not exist. With UID: ' + userId);

        const userData = userRecord.data();
        if (!userData.kolVoted) {
            console.log('Update KOL user data');
            userData.kolVoted = await VideosService.getVotedKOLsByUser(userId);
            await this.update(userId, { kolVoted: userData.kolVoted });
        }

        // const currentWeek = CURRENT_WEEK_DATA(moment().unix());

        // if (userData.lastUploaded >= currentWeek.start && userData.lastUploaded < currentWeek.end) {
        //     userData.joinWeeklyContest = false;
        // } else {
        //     userData.joinWeeklyContest = true;
        // }

        userData.joinWeeklyContest = false;


        return {
            id: userId,
            ...userData,
        };
    }

    // NOT USE
    // async getUserByName(name: string) {
    //     const userRecords = await this.usersCollection.where('name', '==', name).get();
    //     if (userRecords.empty) return [];

    //     return userRecords.docs.reduce((acc: any, user: any) => {
    //         acc[user.id] = {
    //             id: user.id,
    //             ...user.data(),
    //         };
    //         return acc;
    //     }, {});
    // }

    async update(userId: string, userData: any) {
        // Validate HEAD
        const { headCode = '' } = userData;
        if (!_.isEmpty(headCode)) {
            const headData = _.find(HEAD_DATA, h => h.headCode == headCode);
            if (_.isEmpty(headData)) throw new Error('Invalid head code');

            userData.headData = headData;
        } else {
            userData.headData = null;
        }

        const userDoc = this.usersCollection.doc(userId);
        await userDoc.update(userData);

        return userId;
    }

    // For admin
    // TODO: store counter value in database
    async countUsers() {
        const userRecords = await this.usersCollection.get();
        if (userRecords.empty) return 0;

        return userRecords.size;
    }

    async getWithPaging(limit: number, lastUserId: string = null) {
        let query = this.usersCollection.orderBy('createdAt', 'desc');
        if (!_.isEmpty(lastUserId)) query = query.startAfter(await this.usersCollection.doc(lastUserId).get());
        if (limit > 0) query = query.limit(limit);

        const querySnapshot = await query.get();
        if (querySnapshot.empty) return [];

        return querySnapshot.docs.map((u: any) => ({
            id: u.id,
            ..._.pick(u.data(), USER_ALLOW_FIELDS),
        }));
    }
}

const us = new UsersService();
export { us as UsersService };
