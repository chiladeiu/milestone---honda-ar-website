import admin, { auth, firestore, CollectionReference, DocumentReference } from '../firebase';
import axios from 'axios';
import { DELAY_INIT } from '../config';
import { FacebookAPI } from '../apis/facebook';
import { Video, User, RewardType } from '../data-type';
import { VideosService } from './videos';
import _ from 'lodash';
import { getWeekInTimestamp } from '../lib/helper';
import { UsersService } from './users';
import { database } from 'firebase-functions/lib/providers/firestore';

class RewardsService {
    isInitialized: boolean;

    rewardsCollection: CollectionReference;

    constructor() {
        console.log('[Service] Construct User Service');
        this.isInitialized = false;
    }

    initialize() {
        if (this.isInitialized) return;

        console.log('[RS] Initialize');

        this.rewardsCollection = firestore.collection('rewards');
        this.isInitialized = true;
    }

    async getWeeklyReward(type: RewardType, weekId: number = -1) {
        const query = this.rewardsCollection.where('week', '==', weekId).where('type', '==', type);

        const rewardsSnapshot = await query.get();
        if (rewardsSnapshot.empty) {
            console.log('Empty set');
            return {};
        }

        const rewardsData = rewardsSnapshot.docs.shift().data();

        if (type == RewardType.USER) {
            return {
                ...rewardsData,
                data: await Promise.all(
                    rewardsData.data.map(async (record: any) => ({
                        ...record,
                        video: await VideosService.getOne(record.video, true),
                    })),
                ),
            };
        }

        if (type == RewardType.KOL) {
            return {
                ...rewardsData,
                data: await Promise.all(rewardsData.data.map(async (uid: any) => await UsersService.getUser(uid))),
            };
        }

        return {};
    }
}

const rs = new RewardsService();
export { rs as RewardsService };
