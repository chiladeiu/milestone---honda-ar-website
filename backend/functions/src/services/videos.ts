import { firestore, CollectionReference, auth, FieldValue } from '../firebase';
import { Video, KOLVideoVote, User, VoteData } from '../data-type';

import moment from 'moment';
import _ from 'lodash';
import { UsersService } from './users';
import { WEEK_DATA, WEEK_COUNT, TIMEZONE_DIFF, CURRENT_WEEK_ID } from '../config';
import { getWeekInTimestamp, nonAccentVietnamese } from '../lib/helper';

const USER_ALLOW_FIELDS = ['name', 'phone', 'email', 'facebookName', 'facebookEmail', 'facebookId', 'createdAt'];

class VideosService {
    userVideoCollection: CollectionReference;
    kolVideoCollection: CollectionReference;
    kolVideoVoteCollection: CollectionReference;

    isInitialized: boolean;

    constructor() {
        console.log('[VS] Construct video service');
        this.isInitialized = false;
    }

    initialize() {
        if (this.isInitialized) return;

        this.isInitialized = true;

        console.log('[VS] Init video service');
        this.userVideoCollection = firestore.collection('user-videos');
        this.kolVideoCollection = firestore.collection('kol-videos');
        this.kolVideoVoteCollection = firestore.collection('kol-videos-vote');
    }

    async get(limit = 0, lastVideoId: string = null, populateUsers = false, filterInvalid = true) {
        let query = this.userVideoCollection.orderBy('createdAt', 'desc');

        if (filterInvalid) query = query.where('isValid', '==', true);

        console.log(lastVideoId);
        if (!_.isEmpty(lastVideoId)) query = query.startAfter(await this.userVideoCollection.doc(lastVideoId).get());

        if (limit > 0) query = query.limit(limit);

        const querySnapshot = await query.get();
        if (querySnapshot.empty) return [];

        const users = await UsersService.getAllUsersAsDict();
        return querySnapshot.docs.map(doc => {
            const video = doc.data();
            const voted = populateUsers
                ? video.voted.map((v: any) => ({
                    createdAt: v.createdAt,
                    user: _.pick(users[v.user], ['id', 'name']),
                }))
                : video.voted.map((v: any) => v.user);

            return {
                ...video,
                user: _.pick(users[video.user], USER_ALLOW_FIELDS),
                voted,
            };
        });
    }

    async getOne(videoId: string, populateUser: boolean = true) {
        const docSnapshot = await this.userVideoCollection.doc(videoId).get();
        if (!docSnapshot.exists) throw new Error('Invalid video id');

        const data = docSnapshot.data();

        if (!data.isValid) {
            throw new Error(`Video has invalid content ${videoId}`);
        }

        if (populateUser) {
            return {
                ...data,
                voted: data.voted.map((v: any) => v.user),
                user: await UsersService.getUser(data.user),
            };
        }

        return data;
    }

    async getMostVoted(weekId: number = undefined, limit: number = 6) {
        const { weekStart, weekEnd } = getWeekInTimestamp(weekId);
        const query = this.userVideoCollection
            .where('isValid', '==', true)
            .where('createdAt', '>=', weekStart)
            .where('createdAt', '<', weekEnd);

        const querySnapshot = await query.get();

        if (querySnapshot.empty) {
            return [];
        }

        const users = await UsersService.getAllUsersAsDict();
        const videosData = _.orderBy(querySnapshot.docs.map(doc => doc.data()), 'voteCount', 'desc');
        const actualLimit = _.isNaN(limit) ? 6 : limit;

        return videosData.slice(0, actualLimit).map(video => {
            const voted = video.voted.map((v: any) => v.user);
            return {
                ...video,
                voted,
                user: _.pick(users[video.user], ['id', 'name']),
            };
        });
    }

    async getVideoByUser(userId: string, weekId: number = undefined) {
        let query = null;
        query = this.userVideoCollection.where('isValid', '==', true).orderBy('createdAt', 'desc');

        if (!_.isUndefined(weekId)) {
            const { weekStart, weekEnd } = getWeekInTimestamp(weekId);
            query = this.userVideoCollection.where('createdAt', '>=', weekStart).where('createdAt', '<', weekEnd);
        }

        query = query.where('user', '==', userId);
        const querySnapshot = await query.get();

        if (querySnapshot.empty) return [];

        return querySnapshot.docs.map(doc => {
            const video = doc.data();
            const voted = (video.voted || []).map((v: any) => v.user);

            return {
                voted,
                ...video,
            };
        });
    }

    async getVideoByUsername(name: string) {
        const users = await UsersService.getAllUsers(true);
        const filteredUsers = users.filter((user: User) => {
            return (
                user.name &&
                _.includes(nonAccentVietnamese(user.name.toLowerCase()), nonAccentVietnamese(name.toLowerCase()))
            );
        });

        const userIds = filteredUsers.map(user => user.id);

        const results = await Promise.all(
            userIds.map(uid =>
                this.userVideoCollection
                    .where('isValid', '==', true)
                    .where('user', '==', uid)
                    .get(),
            ),
        );

        return results.reduce((acc: any, result: any, idx: any) => {
            const videos = result.docs.map((doc: any) => {
                const video = doc.data();
                const voted = video.voted.map((v: any) => v.user);

                return {
                    ...video,
                    voted,
                    id: doc.id,
                    user: filteredUsers[idx],
                };
            });

            return [...acc, ...videos];
        }, []);
    }

    async add(videoData: Video) {
        videoData.createdAt = moment().unix();
        videoData.voted = [];
        videoData.voteCount = 0;
        const docRef = await this.userVideoCollection.add(videoData);
        await docRef.update({ id: docRef.id });

        return docRef.id;
    }

    async vote(videoId: string, userId: string, ip: string) {
        const videoDoc = this.userVideoCollection.doc(videoId);
        const videoSnapshot = await videoDoc.get();

        if (!videoSnapshot.exists) throw Error('Invalid video id');

        const videoData = videoSnapshot.data() as Video;

        if (_.find(videoData.voted.map((v: VoteData) => v.user), votedUser => votedUser == userId))
            throw Error('User already voted');

        if (CURRENT_WEEK_ID(videoData.createdAt) !== CURRENT_WEEK_ID(moment().unix())) {
            throw Error('Vote feature expired');
        }

        await videoDoc.update({
            voted: FieldValue.arrayUnion({
                createdAt: moment().unix(),
                user: userId,
                ip,
            }),
            voteCount: FieldValue.increment(1),
        });

        return videoId;
    }

    async update(videoId: string, updateFields: any) {
        const videoDoc = this.userVideoCollection.doc(videoId);
        await videoDoc.update(updateFields);

        return videoId;
    }

    async getKOLs(populateUsers = false) {
        const querySnapshot = await this.kolVideoCollection.get();
        if (querySnapshot.empty) return [];

        // if (populateUsers) {
        //     const users = await UsersService.getAllUsersAsDict();
        //     console.log(users);
        //     return querySnapshot.docs.map(doc => {
        //         const video = doc.data();
        //         return {
        //             id: doc.id,
        //             ...video,
        //             voted: video.voted.map((id: any) => ({ id, ...users[id] })),
        //         };
        //     });
        // } else {
        return querySnapshot.docs.map(doc => {
            const video = doc.data();
            return {
                id: doc.id,
                ...video,
            };
        });
        // }
    }

    async getVotedKOLsByUser(userId: string) {
        const votedSnapshot = await this.kolVideoVoteCollection.where('user', '==', userId).get();
        if (votedSnapshot.empty) return [];
        return votedSnapshot.docs.map(doc => doc.data().video);
    }

    async getVotedKOLs() {
        const votedSnapshot = await this.kolVideoVoteCollection.orderBy('video').get();
        const users = await UsersService.getAllUsersAsDict();

        if (votedSnapshot.empty) return [];

        return votedSnapshot.docs.map(doc => {
            const data = doc.data() as KOLVideoVote;
            return {
                ...data,
                user: users[data.user],
            };
        });
    }

    async voteKOLs(videoId: string, userId: string) {
        const videoSnapshot = await this.kolVideoCollection.doc(videoId).get();
        if (!videoSnapshot.exists) throw Error('Invalid video id');

        if (CURRENT_WEEK_ID(moment().unix()) === -1) {
            throw Error('Vote feature expired');
        }

        const votedSnapshot = await this.kolVideoVoteCollection
            .where('video', '==', videoId)
            .where('user', '==', userId)
            .get();

        if (!votedSnapshot.empty) throw Error('User already voted');

        const addedDoc = await this.kolVideoVoteCollection.add({
            user: userId,
            video: videoId,
            createdAt: moment().unix(),
        } as KOLVideoVote);

        await UsersService.update(userId, {
            kolVoted: FieldValue.arrayUnion(videoId),
        });

        await this.kolVideoCollection.doc(videoId).update({
            voteCount: FieldValue.increment(1),
        });

        return addedDoc.id;
    }

    // For admin
    // TODO: store counter value in database
    async countUserVideos() {
        const records = await this.userVideoCollection.get();
        if (records.empty) return 0;

        return records.size;
    }
}

const vs = new VideosService();
export { vs as VideosService };
