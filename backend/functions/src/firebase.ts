import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

admin.initializeApp(functions.config().firebase);
export default admin;

export const firestore = admin.firestore();
export type CollectionReference = admin.firestore.CollectionReference;
export type DocumentReference = admin.firestore.DocumentReference;
export const FieldValue = admin.firestore.FieldValue;

export const storage = admin.storage();
export const bucket = storage.bucket();
export type ObjectMetadata = functions.storage.ObjectMetadata;

export const auth = admin.auth();
