import * as functions from 'firebase-functions';
import { Request, Response } from 'express';
import _ from 'lodash';
import requestIp from 'request-ip';

import { HttpResponseHelper, HttpMethod } from '../lib/response-helper';
import { auth } from '../firebase';
import { DEPLOY_REGION, TIMEZONE_DIFF } from '../config';
import { prepareApp, useAuthenticate, allowViewer } from '../lib/express-base';
import { UsersService } from '../services/users';
import { VideosService } from '../services/videos';
import { RewardsService } from '../services/rewards';

// @ts-ignore
import json2xls from 'json2xls';
import moment from 'moment-timezone';

const app = prepareApp();
app.use(json2xls.middleware);
app.use(requestIp.mw());

app.use('*', async (req, res, next) => {
    await UsersService.initialize();
    VideosService.initialize();
    RewardsService.initialize();
    next();
});

app.get('/videos/:id', exportSingleVideo);

app.use('/users', useAuthenticate(), allowViewer, exportUsers);
app.use('/videos', useAuthenticate(), allowViewer, exportVideos);
app.use('/kol', useAuthenticate(), allowViewer, exportKOLs);

app.get('/test', (req: Request, res: Response) => {
    res.send('Current IP: ' + req.clientIp);
});

const TIMEZONE = 'Asia/Ho_Chi_Minh';

async function exportUsers(req: Request, res: Response) {
    try {
        const users = (await UsersService.getAllUsers()).map(u => ({
            id: u.id,
            email: u.email || '',
            name: u.name || '',
            phone: u.phone || '',
            facebookId: u.facebookId || '',
            facebookName: u.facebookName || '',
            facebookEmail: u.facebookEmail || '',
            createdAt: moment
                .unix(u.createdAt)
                .tz(TIMEZONE)
                .format('DD/MM/YYYY HH:m:s'),
        }));
        // @ts-ignore
        res.xls('users-data.xlsx', users);
    } catch (error) {
        console.log(error);
    }
}

async function exportVideos(req: Request, res: Response) {
    try {
        const videos = (await VideosService.get(0, null, false, false)).map((v: any) => ({
            id: v.id,
            title: v.title,
            voteCount: v.voteCount,
            user: v.user.name,
            phone: v.user.phone,
            email: v.user.email,
            headCode: _.get(v, 'headData.headCode', ''),
            headName: _.get(v, 'headData.headName', ''),
            province: _.get(v, 'headData.province', ''),
            isValid: v.isValid ? 'true' : 'false',
            createdAt: moment
                .unix(v.createdAt)
                .tz(TIMEZONE)
                .format('DD/MM/YYYY HH:m:s'),
        }));
        // @ts-ignore
        res.xls('videos-data.xlsx', videos);
    } catch (error) {
        console.log(error);
    }
}

async function exportSingleVideo(req: Request, res: Response) {
    try {
        const { id } = req.params;
        const video = await VideosService.getOne(id, false);

        // @ts-ignore
        res.xls(
            `${id}-voted.xlsx`,
            video.voted.map((v: any) => ({
                voteAt: moment
                    .unix(v.createdAt)
                    .tz(TIMEZONE)
                    .format('DD/MM/YYYY HH:m:s'),
                user: v.user,
                ip: _.get(v, 'ip', ''),
            })),
        );
    } catch (error) {
        console.log(error);
    }
}

async function exportKOLs(req: Request, res: Response) {
    try {
        const votes = (await VideosService.getVotedKOLs()).map((v: any) => ({
            voteAt: moment
                .unix(v.createdAt)
                .tz(TIMEZONE)
                .format('DD/MM/YYYY HH:m:s'),
            video: v.video,
            user: v.user.name,
            phone: v.user.phone,
            email: v.user.email,
        }));
        // @ts-ignore
        res.xls('vote-data.xlsx', votes);
    } catch (err) {
        console.log(err);
    }
}

export default functions.region(DEPLOY_REGION).https.onRequest(app);
