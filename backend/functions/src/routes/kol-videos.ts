import * as functions from 'firebase-functions';
import { Request, Response } from 'express';
import moment from 'moment';

import { HttpResponseHelper, HttpMethod } from '../lib/response-helper';
import { auth, storage } from '../firebase';
import { DEPLOY_REGION } from '../config';
import { prepareApp, useAuthenticate, allowViewer } from '../lib/express-base';
import { VideosService } from '../services/videos';
import { UsersService } from '../services/users';

const app = prepareApp();
app.use('*', async (req, res, next) => {
    await UsersService.initialize();
    VideosService.initialize();
    next();
});

// TODO: only allow admin
app.get('/admins/voted', useAuthenticate(), allowViewer, getAdminVoted);

// For users
app.get('/', getVideos);
app.post('/:videoId/vote', useAuthenticate(), voteVideo);

async function getVideos(req: Request, res: Response) {
    try {
        HttpResponseHelper.success(res, await VideosService.getKOLs());
    } catch (error) {
        HttpResponseHelper.badRequest(res, error);
    }
}

async function getAdminVoted(req: Request, res: Response) {
    try {
        HttpResponseHelper.success(res, await VideosService.getVotedKOLs());
    } catch (error) {
        HttpResponseHelper.badRequest(res, error);
    }
}

async function voteVideo(req: Request, res: Response) {
    const { videoId } = req.params;
    const { user_id: userId } = <any>req.user;

    try {
        const affectedId = await VideosService.voteKOLs(videoId, userId);
        HttpResponseHelper.success(res, affectedId);
    } catch (error) {
        console.log(error);
        HttpResponseHelper.badRequest(res, error.message);
    }
}

export default functions.region(DEPLOY_REGION).https.onRequest(app);
