import * as functions from 'firebase-functions';
import { Request, Response } from 'express';
import { HttpResponseHelper, HttpMethod } from '../lib/response-helper';
import { auth } from '../firebase';
import { DEPLOY_REGION } from '../config';
import { prepareApp, useAuthenticate, allowViewer } from '../lib/express-base';
import { UsersService } from '../services/users';

const app = prepareApp();
app.use('*', async (req, res, next) => {
    await UsersService.initialize();
    next();
});

// TODO: auth for admin
app.get('/', useAuthenticate(), allowViewer, getUsers);
app.get('/count', useAuthenticate(), allowViewer, countUsers);

app.post('/:id', useAuthenticate(), updateUser);

async function getUsers(req: Request, res: Response) {
    const { limit = 6, lastUserId } = req.query;

    try {
        HttpResponseHelper.success(res, await UsersService.getWithPaging(+limit, lastUserId));
    } catch (error) {
        HttpResponseHelper.badRequest(res, error);
    }
}

async function countUsers(req: Request, res: Response) {
    try {
        HttpResponseHelper.success(res, await UsersService.countUsers());
    } catch (error) {
        HttpResponseHelper.badRequest(res, error);
    }
}

//###########################
async function updateUser(req: Request, res: Response) {
    const { user_id } = <any>req.user;
    const userData = req.body;

    try {
        const affectedId = await UsersService.update(user_id, userData);
        HttpResponseHelper.success(res, affectedId);
    } catch (error) {
        console.log(error);
        HttpResponseHelper.badRequest(res, error.message);
    }
}

export default functions.region(DEPLOY_REGION).https.onRequest(app);
