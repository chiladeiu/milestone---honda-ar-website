import * as functions from 'firebase-functions';
import { Request, Response } from 'express';
import moment from 'moment';
import _ from 'lodash';
import requestIp from 'request-ip';

import { HttpResponseHelper, HttpMethod } from '../lib/response-helper';
import { auth, storage } from '../firebase';
import { DEPLOY_REGION } from '../config';
import { prepareApp, useAuthenticate, allowAdmin, allowViewer } from '../lib/express-base';
import { VideosService } from '../services/videos';
import { UsersService } from '../services/users';

const app = prepareApp();
app.use(requestIp.mw());

app.use('*', async (req, res, next) => {
    VideosService.initialize();
    await UsersService.initialize();
    next();
});

// TODO: admin auth required
app.get('/admins', useAuthenticate(), allowViewer, getAdminVideos);
app.get('/admins/count', useAuthenticate(), allowViewer, countUserVideos);
app.delete('/admins/:videoId', useAuthenticate(), allowAdmin, deleteAdminVideo);

app.get('/', getVideos);
app.get('/:id', getVideoById);
app.post('/:videoId/vote', useAuthenticate(), voteVideo);

async function getVideos(req: Request, res: Response) {
    const { limit, lastVideoId, sortBy, week, userId, username } = req.query;

    try {
        if (!_.isUndefined(userId)) HttpResponseHelper.success(res, await VideosService.getVideoByUser(userId, week));
        else if (!_.isUndefined(username))
            HttpResponseHelper.success(res, await VideosService.getVideoByUsername(username));
        else if (sortBy == 'vote') HttpResponseHelper.success(res, await VideosService.getMostVoted(week, +limit));
        else HttpResponseHelper.success(res, await VideosService.get(+limit, lastVideoId));
    } catch (error) {
        console.log(error);
        HttpResponseHelper.badRequest(res, error);
    }
}

async function getVideoById(req: Request, res: Response) {
    const { id } = req.params;

    try {
        HttpResponseHelper.success(res, await VideosService.getOne(id));
    } catch (error) {
        HttpResponseHelper.badRequest(res, error.message);
    }
}

async function voteVideo(req: Request, res: Response) {
    const { videoId } = req.params;
    const { user_id: userId } = <any>req.user;
    const ip = req.clientIp;

    try {
        const affectedId = await VideosService.vote(videoId, userId, ip);
        HttpResponseHelper.success(res, affectedId);
    } catch (error) {
        console.log(error);
        HttpResponseHelper.badRequest(res, error.message);
    }
}

async function getAdminVideos(req: Request, res: Response) {
    const { limit, lastVideoId } = req.query;

    try {
        HttpResponseHelper.success(res, await VideosService.get(+limit, lastVideoId, true, false));
    } catch (error) {
        console.log(error);
        HttpResponseHelper.badRequest(res, error);
    }
}

async function deleteAdminVideo(req: Request, res: Response) {
    const { videoId } = req.params;

    try {
        HttpResponseHelper.success(res, await VideosService.update(videoId, { isValid: false }));
    } catch (error) {
        console.log(error);
        HttpResponseHelper.badRequest(res, error);
    }
}

async function countUserVideos(req: Request, res: Response) {
    try {
        HttpResponseHelper.success(res, await VideosService.countUserVideos());
    } catch (error) {
        HttpResponseHelper.badRequest(res, error);
    }
}

export default functions.region(DEPLOY_REGION).https.onRequest(app);
