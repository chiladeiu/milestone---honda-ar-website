import * as functions from 'firebase-functions';
import { Request, Response } from 'express';
import moment from 'moment';
import _ from 'lodash';

import { HttpResponseHelper, HttpMethod } from '../lib/response-helper';
import { auth, storage } from '../firebase';
import { DEPLOY_REGION } from '../config';
import { prepareApp, useAuthenticate } from '../lib/express-base';
import { UsersService } from '../services/users';
import { VideosService } from '../services/videos';

const app = prepareApp();

app.use('*', async (req, res, next) => {
    await UsersService.initialize();
    VideosService.initialize();
    next();
});

app.get('/profile', useAuthenticate(), getProfile);
app.patch('/profile', useAuthenticate(), updateProfile);

async function getProfile(req: Request, res: Response) {
    const { user_id } = <any>req.user;

    try {
        HttpResponseHelper.success(res, await UsersService.getUser(user_id));
    } catch (error) {
        console.log(error);
        HttpResponseHelper.badRequest(res, error.message);
    }
}

async function updateProfile(req: Request, res: Response) {
    const { user_id } = <any>req.user;
    const userData = req.body;

    try {
        const affectedId = await UsersService.update(user_id, userData);
        HttpResponseHelper.success(res, affectedId);
    } catch (error) {
        console.log(error);
        HttpResponseHelper.badRequest(res, error.message);
    }
}

export default functions.region(DEPLOY_REGION).https.onRequest(app);
