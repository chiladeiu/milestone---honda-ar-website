import * as functions from 'firebase-functions';
import ffmpeg from 'fluent-ffmpeg';
import ffmpegStatic from 'ffmpeg-static';
import path, { basename } from 'path';
import fs from 'fs';
import os from 'os';
import _ from 'lodash';
import moment from 'moment';
// @ts-ignore
import ffprobe from 'ffprobe';
// @ts-ignore
import ffprobeStatic from 'ffprobe-static';
import sharp from 'sharp';

import { DEPLOY_REGION, LOGO_FILE } from '../config';
import { ObjectMetadata, storage, bucket, auth } from '../firebase';
import { VideosService } from '../services/videos';
import { UsersService } from '../services/users';
import { User } from '../data-type';

// Makes an ffmpeg command return a promise.
function promisifyCommand(command: any) {
    return new Promise((resolve, reject) => {
        command
            .on('end', resolve)
            .on('error', reject)
            .run();
    });
}

async function addAudio(videoPath: string, audioPath: string, outputPath: string) {
    const fileName = path.basename(videoPath).split('.')[0];

    const outputVideoName = fileName + '_s1.mp4';
    const outputVideoPath = path.join(outputPath, outputVideoName);

    const noAudioName = fileName + '_an_s1.mp4';
    const noAudioPath = path.join(outputPath, noAudioName);

    const removeAudioCmd = ffmpeg()
        .setFfmpegPath(ffmpegStatic.path)
        .input(videoPath)
        .noAudio()
        .output(noAudioPath);

    await promisifyCommand(removeAudioCmd);
    console.log('Remove audio');

    const addAudioCmd = ffmpeg()
        .setFfmpegPath(ffmpegStatic.path)
        .input(noAudioPath)
        .input(audioPath)
        // .audioCodec('aac')
        .addOptions(['-shortest'])
        .videoCodec('copy')
        .output(outputVideoPath);

    await promisifyCommand(addAudioCmd);
    console.log('Add audio');

    return {
        videoName: outputVideoName,
        videoPath: outputVideoPath,
    };
}

async function addLogo(videoPath: string, logoPath: string, outputPath: string) {
    const fileName = path.basename(videoPath).split('.')[0];

    const outputVideoName = fileName + '_s2.mp4';
    const outputVideoPath = path.join(outputPath, outputVideoName);

    const addAudioCmd = ffmpeg()
        .setFfmpegPath(ffmpegStatic.path)
        .input(videoPath)
        .input(logoPath)
        .complexFilter(
            [
                {
                    filter: 'overlay',
                    options: '0:0',
                },
            ],
            null,
        )
        .output(outputVideoPath);

    await promisifyCommand(addAudioCmd);

    return {
        videoName: outputVideoName,
        videoPath: path.join(outputPath, outputVideoName),
    };
}

async function generateThumbnail(videoPath: string, outputPath: string) {
    console.log('Extract thumbnail:', videoPath);
    const fileName = path.basename(videoPath).split('.')[0];
    const outputThumbnailName = fileName + '_thumbnail.jpg';

    const takeScreenshotCmd = ffmpeg()
        .setFfmpegPath(ffmpegStatic.path)
        .input(videoPath)
        .output(path.join(outputPath, outputThumbnailName))
        .noAudio()
        .seek(1);

    try {
        await promisifyCommand(takeScreenshotCmd);
    } catch (err) {
        // DO NOTHING/ umimportant error
        console.log('#### UNIMPORTANT ####');
        console.log('Error', err);
        console.log('#### UNIMPORTANT ####');
    }

    return {
        thumbnailName: outputThumbnailName,
        thumbnailPath: path.join(outputPath, outputThumbnailName),
    };
}

async function scaleLogo(videoPath: string, logoPath: string, outputPath: string) {
    const info = await ffprobe(videoPath, { path: ffprobeStatic.path });
    const videoMeta = _.find(info.streams, i => i.codec_type == 'video');

    const logoOutName = '_scaled_watermark.png';
    const logoOutPath = path.join(outputPath, logoOutName);

    await sharp(logoPath)
        .resize(videoMeta.width)
        .toFile(logoOutPath);

    return {
        logoName: logoOutName,
        logoPath: logoOutPath,
    };
}

async function handleFileUploaded(fileObject: ObjectMetadata) {
    const startTime = moment().unix();

    VideosService.initialize();
    await UsersService.initialize();

    const fileBucket = fileObject.bucket; // The Storage bucket that contains the file.
    const fileName = fileObject.name; // File path in the bucket.
    const contentType = fileObject.contentType; // File content type.

    console.log('fileBucket', fileBucket);
    console.log('fileName', fileName);
    console.log('contentType', contentType);

    // Exit if this is triggered on a file that is not an audio.
    if (!contentType.startsWith('video/')) {
        console.log('This is not a video.');
        return null;
    }

    // Get the file name.
    // Exit if the audio is already converted. / KOL video
    if (fileName.endsWith('_edited.mp4')) {
        console.log('Already a converted video.');
        return null;
    }

    if (fileName.includes('kol_')) {
        console.log('KOL video -> do nothing');
        return null;
    }

    const [user_id] = fileName.split('_');
    const { videoTitle = '', joinWeeklyContest, headData = {} } = (await UsersService.getUser(user_id)) as User;

    if (!joinWeeklyContest) {
        console.log(`User ${user_id} has reach maximum upload per week. Reject`);
        return null;
    }

    if (_.isEmpty(videoTitle.trim())) {
        console.log(`Empty video title of user ${user_id} `);
        return null;
    }

    await UsersService.update(user_id, { isProcessingVideo: true, processingStep: 1 });

    // Download file from bucket.
    const logoFile = '_watermark.png';
    const audioFile = '_audio1.aac';

    const tmpVideoPath = path.join(os.tmpdir(), fileName);
    const tmpAudioPath = path.join(os.tmpdir(), audioFile);
    const tmpLogoPath = path.join(os.tmpdir(), logoFile);

    console.log('Download video');
    await bucket.file(fileName).download({ destination: tmpVideoPath });
    await bucket.file(audioFile).download({ destination: tmpAudioPath });
    await bucket.file(logoFile).download({ destination: tmpLogoPath });

    await UsersService.update(user_id, { processingStep: 2 });

    console.log('Begin convertion');
    // const s1 = await addAudio(tmpVideoPath, tmpAudioPath, os.tmpdir());
    // console.log('s1', s1);

    const s1 = {
        videoPath: tmpVideoPath,
    };

    await UsersService.update(user_id, { processingStep: 3 });

    const s11 = await scaleLogo(s1.videoPath, tmpLogoPath, os.tmpdir());

    const s2 = await addLogo(s1.videoPath, s11.logoPath, os.tmpdir());
    console.log('s2', s2);
    await UsersService.update(user_id, { processingStep: 4 });

    const s3 = await generateThumbnail(s1.videoPath, os.tmpdir());
    console.log('s3', s3);
    await UsersService.update(user_id, { processingStep: 5 });

    // Re-upload to bucket
    console.log('Re-upload ...');
    const uploadBaseName = path.basename(fileName).split('.')[0];
    const uploadVideoFile = `${uploadBaseName}_edited.mp4`;
    const uploadThumbnailFile = `${uploadBaseName}_thumbnail.jpg`;

    await bucket.upload(s2.videoPath, {
        destination: uploadVideoFile,
        resumable: false,
    });
    await UsersService.update(user_id, { processingStep: 6 });

    await bucket.upload(s3.thumbnailPath, {
        destination: uploadThumbnailFile,
        resumable: false,
    });

    await UsersService.update(user_id, { processingStep: 7 });
    console.log('Reup DONE');

    const videoId = await VideosService.add({
        user: user_id,
        path: uploadVideoFile,
        thumbnail: uploadThumbnailFile,
        title: videoTitle,
        isValid: true,
        headData,
    });

    await UsersService.update(user_id, {
        isProcessingVideo: false,
        processingStep: 0,
        joinWeeklyContest: false,
        mostRecentVideo: videoId,
        lastUploaded: moment().unix(),
        videoTitle: '',
        headData: null,
    });

    // Clean up
    // ...
    // fs.unlinkSync(tmpVideoPath);
    // fs.unlinkSync(thumbnailPath);

    console.log('Total time', moment().unix() - startTime);

    return true;
}

export default functions
    .region(DEPLOY_REGION)
    .storage.object()
    .onFinalize(handleFileUploaded);
