import * as functions from 'firebase-functions';
import { Request, Response } from 'express';
import { HttpResponseHelper, HttpMethod } from '../lib/response-helper';
import { auth } from '../firebase';
import { DEPLOY_REGION } from '../config';
import { prepareApp, useAuthenticate } from '../lib/express-base';
import { UsersService } from '../services/users';
import { VideosService } from '../services/videos';
import { RewardsService } from '../services/rewards';

const app = prepareApp();
app.use('*', async (req, res, next) => {
    await UsersService.initialize();
    VideosService.initialize();
    RewardsService.initialize();
    next();
});

app.use('/', getWeeklyRewards);

async function getWeeklyRewards(req: Request, res: Response) {
    const { type, week = -1 } = req.query;

    try {
        HttpResponseHelper.success(res, await RewardsService.getWeeklyReward(type, +week));
    } catch (error) {
        HttpResponseHelper.badRequest(res, error);
    }
}

export default functions.region(DEPLOY_REGION).https.onRequest(app);
