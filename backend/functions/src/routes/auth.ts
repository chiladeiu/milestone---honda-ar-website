import * as functions from 'firebase-functions';
import { Request, Response } from 'express';
import moment from 'moment';
import _ from 'lodash';

import { HttpResponseHelper, HttpMethod } from '../lib/response-helper';
import { auth, storage } from '../firebase';
import { DEPLOY_REGION } from '../config';
import { prepareApp } from '../lib/express-base';
import { UsersService } from '../services/users';

const app = prepareApp();

app.use('*', async (req, res, next) => {
    await UsersService.initialize();
    next();
});

app.post('/request', requestLoginToken);

async function requestLoginToken(req: Request, res: Response) {
    const { facebookToken, zaloToken } = req.body;

    console.log(req.body);

    try {
        if (facebookToken) {
            console.log('facebookToken', facebookToken);

            const loginToken = await UsersService.prepareFacebookLoginToken(facebookToken);
            HttpResponseHelper.success(res, loginToken);
            return;
        }
        // else if (zaloToken) {
        //     const loginToken = await UsersService.prepareZaloLoginToken(zaloToken);
        //     HttpResponseHelper.success(res, loginToken);
        // }

        HttpResponseHelper.notAllowed(res, 'Login method not allow');
    } catch (error) {
        console.log(error);
        HttpResponseHelper.badRequest(res, error.message);
    }
}

export default functions.region(DEPLOY_REGION).https.onRequest(app);
