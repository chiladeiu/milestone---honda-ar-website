import axios from 'axios';
import { FB_APP_ID, FB_APP_SECRET } from '../config';

const FB_GRAPH_API = 'https://graph.facebook.com/v3.3/';
const FB_OATH_API = 'https://graph.facebook.com/oauth/access_token';
const FB_DEBUG_API = 'https://graph.facebook.com/debug_token';

export const FacebookAPI = {
    requestAccessToken: () =>
        axios.get(FB_OATH_API, {
            params: {
                client_id: FB_APP_ID,
                client_secret: FB_APP_SECRET,
                grant_type: 'client_credentials',
            },
        }),
    verifyToken: (input_token: string, access_token: string) =>
        axios.get(FB_DEBUG_API, {
            params: {
                access_token,
                input_token,
            },
        }),
    queryUserData: (access_token: string) =>
        axios.get(`${FB_GRAPH_API}me`, {
            params: {
                access_token,
                fields: 'id,last_name,first_name,name,email',
            },
        }),
};
