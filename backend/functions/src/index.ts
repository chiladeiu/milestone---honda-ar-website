import userVideos from './routes/user-videos';
import videoProcessing from './routes/video-processing';
import users from './routes/users';
import auth from './routes/auth';
import me from './routes/me';
import kolVideos from './routes/kol-videos';
import rewards from './routes/rewards';
import exporter from './routes/exporter';

export { userVideos, users, videoProcessing, auth, me, kolVideos, rewards, exporter };
