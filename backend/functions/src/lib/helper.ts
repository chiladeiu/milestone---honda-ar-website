import moment from 'moment';
import { WEEK_COUNT, WEEK_DATA, TIMEZONE_DIFF } from '../config';

export function getWeekInTimestamp(weekId = -1) {
    let weekStart,
        weekEnd = 0;

    if (weekId >= 0 && weekId < WEEK_COUNT) {
        weekStart = WEEK_DATA[weekId].start;
        weekEnd = WEEK_DATA[weekId].end;
    } else {
        weekStart =
            moment()
                .startOf('week')
                .unix() + TIMEZONE_DIFF;
        weekEnd =
            moment()
                .endOf('week')
                .unix() + TIMEZONE_DIFF;
    }

    return {
        weekStart,
        weekEnd,
    };
}

export function nonAccentVietnamese(str: string) {
    let outStr = str.toLowerCase();

    outStr = outStr.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    outStr = outStr.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    outStr = outStr.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    outStr = outStr.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    outStr = outStr.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    outStr = outStr.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    outStr = outStr.replace(/đ/g, 'd');

    // Some system encode vietnamese combining accent as individual utf-8 characters
    outStr = outStr.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, ''); // Huyền sắc hỏi ngã nặng
    outStr = outStr.replace(/\u02C6|\u0306|\u031B/g, ''); // Â, Ê, Ă, Ơ, Ư

    return outStr;
}
