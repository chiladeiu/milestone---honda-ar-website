import express, { Request, Response, NextFunction } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import passport from 'passport';
import { Strategy } from 'passport-http-bearer';
import _ from 'lodash';

import { auth } from '../firebase';
import { ADMIN_EMAILS, VIEWER_EMAILS } from '../config';
import { HttpResponseHelper } from './response-helper';

async function verifyToken(idToken: string, done: any) {
    try {
        const decodedIdToken = await auth.verifyIdToken(idToken);
        return done(null, { ...decodedIdToken });
    } catch (error) {
        console.error('Error while verifying Firebase ID token:', error);
        return done(null, false);
    }
}

export function prepareApp() {
    const app = express();
    app.disable('x-powered-by');

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(cors({ origin: true }));

    const strategy = new Strategy(verifyToken);
    passport.use(strategy);

    app.use(passport.initialize());

    return app;
}

export function useAuthenticate() {
    return passport.authenticate('bearer', { session: false });
}

export function allowAdmin(req: Request, res: Response, next: NextFunction) {
    const { email = '' } = <any>req.user;

    if (_.includes(ADMIN_EMAILS, email)) {
        next();
        return;
    }

    HttpResponseHelper.notAllowed(res);
}

export function allowViewer(req: Request, res: Response, next: NextFunction) {
    const { email = '' } = <any>req.user;

    if (_.includes(VIEWER_EMAILS, email)) {
        next();
        return;
    }

    HttpResponseHelper.notAllowed(res);
}
