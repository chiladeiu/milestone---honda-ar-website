import { NextFunction, Response } from 'express';

export enum HttpMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    PATCH = 'PATCH',
    DELETE = 'DELETE',
}

export enum HttpStatusCode {
    SUCCESS = 200,
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    NOT_ALLOWED = 403,
    NOT_FOUND = 404,
}

class HttpResponseHelper {
    httpResponse?: Response;

    json(success: boolean, code: number, message: string, payload: any) {
        if (!this.httpResponse) throw new Error('Undefined response object');

        this.httpResponse.status(code).send({
            success,
            code,
            message,
            payload,
        });
    }

    success(res: Response, payload: any, message: string = '') {
        this.httpResponse = res;
        this.json(true, HttpStatusCode.SUCCESS, message, payload);
    }

    badRequest(res: Response, message: string = 'Parameter not correctly.') {
        this.httpResponse = res;
        this.json(false, HttpStatusCode.BAD_REQUEST, message, {});
    }

    unauthorized(res: Response, message: string = 'Authorization failed.') {
        this.httpResponse = res;
        this.json(false, HttpStatusCode.UNAUTHORIZED, message, {});
    }

    notAllowed(res: Response, message: string = 'Forbidden.') {
        this.httpResponse = res;
        this.json(false, HttpStatusCode.NOT_ALLOWED, message, {});
    }

    notFound(res: Response, message: string = 'Resource not found.') {
        this.httpResponse = res;
        this.json(false, HttpStatusCode.NOT_FOUND, message, {});
    }
}

const httpResponseHelper = new HttpResponseHelper();
export { httpResponseHelper as HttpResponseHelper };
