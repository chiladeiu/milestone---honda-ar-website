export const DEPLOY_REGION = 'asia-east2';

export const LOGO_FILE = '_logo.png';
export const DELAY_INIT = 1000;

export const FB_APP_ID = '479552689258009';
export const FB_APP_SECRET = '95317f246e11b7872e317b7f9e03ccea';
export const FB_APP_TOKEN = '479552689258009|sf_4n26zHHNg3u2WV3w7QwYRsSo';

export const EVENT_BEGIN_DATE = 1568080800; // Tuesday September 10, 2019 00:00:00 (am) in time zone Asia/Ho Chi Minh (+07)
export const WEEK_IN_SECONDS = 604800;
export const WEEK_COUNT = 6;
export const TIMEZONE_DIFF = 2520; // VN at GMT +7

export const WEEK_DATA = [...new Array(6)].map((x, idx) => ({
    start: EVENT_BEGIN_DATE + idx * WEEK_IN_SECONDS,
    end: EVENT_BEGIN_DATE + (idx + 1) * WEEK_IN_SECONDS,
}));
export const CURRENT_WEEK_ID = (currentTimestamp: number) =>
    WEEK_DATA.findIndex(week => week.start <= currentTimestamp && currentTimestamp < week.end);

export const CURRENT_WEEK_DATA = (currentTimestamp: number) =>
    WEEK_DATA.find(week => week.start <= currentTimestamp && currentTimestamp < week.end);

export const VIEWER_EMAILS = ['fractalcompany123@gmail.com'];
export const ADMIN_EMAILS = ['fractalcompany123@gmail.com'];
