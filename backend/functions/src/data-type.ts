export interface VoteData {
    createdAt: number;
    user: string;
    ip?: string;
}

export interface Video {
    path: string;
    thumbnail: string;
    user: string;
    createdAt?: number;
    voted?: VoteData[];
    voteCount?: number;
    title?: string;
    isValid?: boolean;
    headData?: any;
}

export interface KOLVideo {
    path: string;
    thumbnail: string;
    voted?: string[];
    voteCount?: string[];
}

export interface KOLVideoVote {
    user: string;
    video: string;
    createdAt: number;
}

export interface User {
    name?: string;
    phone?: string;
    email?: string;
    isProcessingVideo?: boolean;
    processingStep?: number;
    joinWeeklyContest?: boolean;
    mostRecentVideo?: string;
    videoTitle?: string;
    headData?: any;
}

export enum RewardType {
    USER = 'user',
    KOL = 'kol',
    FINAL = 'final',
}

/*
{
    week
    createAt
    data: [
        { video, place },
        { video, place },
        { video, place },
        { video, place },
        { video, place },
        { video, place },
    ]
}
*/
