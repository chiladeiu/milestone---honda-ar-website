/* eslint-disable */

export const FacebookSDK = {
    initialize: () => {
        window.fbAsyncInit = function() {
            FB.init({
                appId: '479552689258009',
                autoLogAppEvents: true,
                xfbml: true,
                version: 'v3.3',
            });

            FB.getLoginStatus();
        };
    },
    getLoginStatus: () =>
        new Promise(resolve => {
            FB.getLoginStatus(function(response) {
                resolve(response);
            });
        }),
    login: () =>
        new Promise(resolve => {
            FB.login(
                function(response) {
                    resolve(response);
                },
                { scope: 'public_profile, email' },
            );
        }),
    queryData: () =>
        new Promise(resolve => {
            FB.api('/me?fields=name,email', function(response) {
                resolve(response);
            });
        }),
    parseAuthResponse: response => {
        if (response.status === 'connected') {
            return response.authResponse.accessToken;
        }

        return '';
    },
    parseXFBML: () => FB.XFBML.parse(),
    logout: () => new Promise(resolve => FB.logout(response => resolve(response))),
    share: data =>
        new Promise(resolve => {
            const { href = '', hashtag = '' } = data;

            FB.ui(
                {
                    method: 'share',
                    href,
                    hashtag,
                },
                response => resolve(response),
            );
        }),
};
