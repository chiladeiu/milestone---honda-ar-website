/* eslint-disable */
import axios from 'axios';

export const ZaloSDK = {
    initialize: (appId, redirectUrl) => {
        Zalo.init({
            version: '2.0',
            appId,
            redirectUrl,
        });
    },
    // getAccessToken: () =>
    //     new Promise((resolve, reject) => {
    //         Zalo.getAccessToken(
    //             response => {
    //                 console.log('Respn', response);
    //                 resolve(response);
    //             },
    //             error => {
    //                 reject(error);
    //             },
    //         );
    //     }),
    getAccessToken: (appId, appSecret, code) =>
        axios.get('https://oauth.zaloapp.com/v3/access_token', {
            params: {
                app_id: appId,
                app_secret: appSecret,
                code,
                isSDK: true,
            },
        }),
    getLoginStatus: () =>
        new Promise(resolve => {
            Zalo.getLoginStatus(function(response) {
                resolve(response);
            });
        }),
    login: () => Zalo.login('', 'get_profile'),
    queryData: () =>
        new Promise(resolve => {
            Zalo.api(
                '/me',
                'GET',
                {
                    fields: 'id,name',
                },
                function(response) {
                    resolve(response);
                },
            );
        }),
    parseAuthResponse: response => {
        console.log(response);
        if (response.status === 'connected') {
            return response.authResponse.accessToken;
        }

        return '';
    },
};

// ZL_LOG = 'zl-log';
// ZL_REDIRECT_URL = 'https://honda.stdio.vn/login';

// function appendZLLog(text) {
//     ZL_LOG.value = text + '\r\n----\r\n' + ZL_LOG.value;
// }

// function zaloInitialize() {
//     ZL_LOG = document.getElementById(ZL_LOG);

//     console.log('Zalo initialize');

//     Zalo.init({
//         version: '2.0',
//         appId: '2970099256358239421',
//         redirectUrl: ZL_REDIRECT_URL,
//     });
//     Zalo.getLoginStatus(zaloStatusChangeCallback);
// }

// function zaloStatusChangeCallback(response) {
//     if (response.status === 'connected') {
//         appendZLLog(JSON.stringify(response));
//         Zalo.api(
//             '/me',
//             'GET',
//             {
//                 fields: 'id,name',
//             },
//             function(response) {
//                 appendZLLog(JSON.stringify(response));
//             },
//         );
//     } else {
//         appendZLLog('Please login Zalo first');
//     }
// }

// function onZaloLogin() {
//     Zalo.login('', 'get_profile,get_friends,send_message,post_feed');
// }
