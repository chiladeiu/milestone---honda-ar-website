/* eslint-disable indent */
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/storage';

import { FIREBASE_CONFIG } from '../../config';

export const FirebaseSDK = {
    initialize: () => {
        firebase.initializeApp(FIREBASE_CONFIG);
    },
    setAuthChangedCallback: authCallback => {
        firebase.auth().onAuthStateChanged(authCallback);
    },
    login: token => firebase.auth().signInWithCustomToken(token),
    loginWithEmail: (email, password) => firebase.auth().signInWithEmailAndPassword(email, password),
    logout: () => firebase.auth().signOut(),
    upload: (file, fileName, progressChangeCallback) => {
        return new Promise(resolve => {
            const storageRef = firebase.storage().ref();
            const uploadFileRef = storageRef.child(fileName);

            const uploadTask = uploadFileRef.put(file);

            uploadTask.on(
                'state_changed',
                snapshot => {
                    let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                    // console.log('Upload is ' + progress + '% done');
                    switch (snapshot.state) {
                        case firebase.storage.TaskState.PAUSED: // or 'paused'
                            console.log('Upload is paused');
                            break;
                        case firebase.storage.TaskState.RUNNING: // or 'running'
                            // console.log('Upload is running');
                            break;
                    }

                    if (progressChangeCallback) {
                        progressChangeCallback(progress);
                    }
                },
                error => {
                    // Handle unsuccessful uploads
                    console.log('Upload error', error);
                },
                () => {
                    // Handle successful uploads on complete
                    // For instance, get the download URL: https://firebasestorage.googleapis.com/...
                    // uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
                    //     console.log('File available at', downloadURL);
                    // });
                    resolve(true);
                },
            );
        });
    },
};

// // FIREBASE_LOGIN_URL = 'http://localhost:5000/honda-ar-ac35d/asia-east2/auth/';
// // FIREBASE_LOGIN_URL = 'https://honda.stdio.vn/api/auth/';
// FIREBASE_LOGIN_URL = 'https://asia-east2-honda-ar-ac35d.cloudfunctions.net/auth/';

// FIREBASE_LOG = 'fbe-log';

// function pageOnLoad() {
//     FIREBASE_LOG = document.getElementById(FIREBASE_LOG);

//     facebookInitialize();
//     // zaloInitialize();

//     firebaseInitialize();
// }

// function appendFirebaseLog(text) {
//     FIREBASE_LOG.value = text + '\r\n----\r\n' + FIREBASE_LOG.value;
// }

// function firebaseInitialize() {
//     // Your web app's Firebase configuration
//     var firebaseConfig = {
//         apiKey: 'AIzaSyCns45BIAR0xXZ7W3WWWLNZ9p1RaGIfOsM',
//         authDomain: 'honda-ar-ac35d.firebaseapp.com',
//         databaseURL: 'https://honda-ar-ac35d.firebaseio.com',
//         projectId: 'honda-ar-ac35d',
//         storageBucket: 'honda-ar-ac35d.appspot.com',
//         messagingSenderId: '805837778807',
//         appId: '1:805837778807:web:5b7f411b4f30e1d4',
//     };

//     // Initialize Firebase
//     firebase.initializeApp(firebaseConfig);
//     firebase.auth().onAuthStateChanged(firebaseOnAuthStateChanged);
// }

// function firebaseOnAuthStateChanged(user) {
//     console.log('[Firebase] On auth state change');
//     if (user) {
//         user.getIdToken()
//             .then(function(idToken) {
//                 appendFirebaseLog('idToken');
//                 appendFirebaseLog(idToken);
//             })
//             .catch(function(error) {
//                 // Handle error
//                 console.log(error);
//             });

//         console.log('User Data');
//         appendFirebaseLog('Name: ', user.displayName);
//         // User is signed in
//         var email = user.email;
//         // ...
//     } else {
//         appendFirebaseLog('User not signed in');
//     }
// }

// function onFirebaseRequestLogin() {
//     axios
//         .post(FIREBASE_LOGIN_URL + 'request', {
//             facebookToken: FB_TOKEN,
//         })
//         .then(response => {
//             const loginToken = response.data.payload;
//             appendFirebaseLog('Login token');
//             appendFirebaseLog(loginToken);

//             appendFirebaseLog('Begin login');

//             firebase
//                 .auth()
//                 .signInWithCustomToken(loginToken)
//                 .catch(function(error) {
//                     // Handle Errors here.
//                     var errorCode = error.code;
//                     var errorMessage = error.message;
//                     if (errorCode === 'auth/invalid-custom-token') {
//                         alert('The token you provided is not valid.');
//                     } else {
//                         appendFirebaseLog(error);
//                     }
//                 });
//         });
// }
