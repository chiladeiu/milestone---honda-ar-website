import Router from 'next/router';
import nextCookie from 'next-cookies';
import React from 'react';
import cookie from 'js-cookie';
import axios from 'axios';
import { LOGIN_URL, ROUTES, ROLES } from '../config';
import { redirect, serverRedirect } from './helper';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Spin } from 'antd';

import _ from 'lodash';

export const withAuth = compose(
    connect(state => ({
        me: state.me,
    })),
    WrappedComponent =>
        class extends React.Component {
            constructor(props) {
                super(props);

                this.state = {
                    role: '',
                };
            }

            componentDidMount() {}

            componentDidUpdate(prevProps, prevState) {
                const { me } = this.props;

                if (prevProps.me.isInitFirebase !== me.isInitFirebase && me.isInitFirebase) {
                    if (!me.isLoggedIn) {
                        Router.push(ROUTES.CMS_AUTH);
                    }
                }
            }

            render() {
                const { isInitFirebase, isLoggedIn, role } = this.props.me;

                if (!isInitFirebase) {
                    return <Spin />;
                } else if (_.isEmpty(role)) {
                    return <div>Permission Error</div>;
                }

                return <WrappedComponent {...this.props} />;
            }
        },
);
