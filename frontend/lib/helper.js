import Router from 'next/router';
import _ from 'lodash';
import { STORAGE_URL } from '../config';

export function serverRedirect(ctx, location) {
    const { res } = ctx;
    if (res) {
        res.writeHead(302, {
            Location: location,
        });
        res.end();
    } else {
        throw new Error('Invalid context');
    }
}

export function channingActions(currentActions, dispatch, ...actionGenerators) {
    return actionGenerators.reduce((accActions, actionGenerator) => {
        return {
            ...actionGenerator(accActions, dispatch),
        };
    }, currentActions);
}

export function generateOnChangeInput(fieldName, context) {
    return {
        onChange: event => context.setState({ [fieldName]: event.target.value }),
        value: context.state[fieldName],
    };
}

export function generateOnChangeDatePicker(fieldName, context) {
    return date => context.setState({ [fieldName]: date });
}

export function generateOnChangeInputNumber(fieldName, context, isInteger = false) {
    return value => context.setState({ [fieldName]: isInteger ? _.floor(value) : value });
}

export function generateOnChangeSwitch(fieldName, context) {
    return checked => context.setState({ [fieldName]: checked });
}

export function loadClientScript(d, s, id, path) {
    var js;
    var fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = path;
    fjs.parentNode.insertBefore(js, fjs);
}

export function loadScriptAsync(src) {
    return new Promise(resolve => {
        const script = document.createElement('script');
        script.src = src;
        script.type = 'text/javascript';
        script.async = true;
        script.onload = resolve;
        document.body.appendChild(script);
    });
}

export function getMediaURL(path) {
    return `${STORAGE_URL}${encodeURIComponent(path)}?alt=media`;
}

export function clamp(num, min, max) {
    return num <= min ? min : num >= max ? max : num;
}
