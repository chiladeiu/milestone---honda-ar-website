import axios from 'axios';
import _ from 'lodash';
import { auth } from './auth';
import { FIREBASE_TOKEN_KEY } from '../config';

let callback401 = null;

export function set401Callback(cb) {
    callback401 = cb;
}

const axiosInstance = axios.create();

axiosInstance.interceptors.response.use(
    response => response,
    error => {
        const { response } = error;
        if (!_.isEmpty(response) && response.status === 401 && !_.isNull(callback401)) {
            callback401(response.data.error);
        }

        return Promise.reject(error);
    },
);

axiosInstance.interceptors.request.use(
    config => {
        if (process.browser) config.headers.authorization = localStorage.getItem(FIREBASE_TOKEN_KEY);

        return config;
    },
    error => Promise.reject(error),
);

export { axiosInstance as axios };
