class Utils {
    formatNumberToVND (number = '') {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + ' đ';
    }

    formatWithCommas (number = '') {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
}

export default new Utils();
