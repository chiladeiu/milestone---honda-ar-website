
// !! THIS FILE IS GENERATED - DO NOT MODIFY MANUALY !!
// !! RUN 'yarn lang' IF YOU WANT TO UPDATE THE LANGUAGE SET !!

import i18next from 'i18next';

export class LANG {
    static HeaderHome = () => i18next.t('HeaderHome');
    static HeaderContest = () => i18next.t('HeaderContest');
    static HeaderPrize = () => i18next.t('HeaderPrize');
    static HeaderTerms = () => i18next.t('HeaderTerms');
}

let rootApp = null;
export function setRootApp(app) {
    rootApp = app;
}

export function changeLanguage(lng) {
    i18next.changeLanguage(lng).then(() => {
        if (rootApp) {
            rootApp.applyLanguage();
        }
    });
}

export function getLanguage() {
    return i18next.getLanguage();
}
