import React from 'react';
import { Layout } from 'antd';

import AuthHeader from '../components/forms/auth/AuthHeader';
import './AuthLayout.less';

const {
    Content
} = Layout;

class AuthLayout extends React.Component {
    render () {
        return (
            <Layout className="authLayout">
                <AuthHeader />
                <Layout>
                    <Content className="contentLayout">
                        { this.props.children }
                    </Content>
                </Layout>
            </Layout>
        );
    }
};

export default AuthLayout;
