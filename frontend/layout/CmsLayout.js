import React from 'react';
import { Layout, Menu, Icon } from 'antd';

import './CmsLayout.less';
import CmsSider from '../components/forms/cms/CmsSider';
import CmsHeader from '../components/forms/cms/CmsHeader';
import CmsFooter from '../components/forms/cms/CmsFooter';

const { Header, Content, Footer, Sider } = Layout;

class CmsLayout extends React.Component {
    render() {
        return (
            <Layout className="cmsLayout" hasSider>
                <CmsSider />
                <Layout>
                    <CmsHeader title={this.props.title} />
                    <Content className="cmsLayoutContent">
                        {this.props.children}
                        <CmsFooter />
                    </Content>
                </Layout>
            </Layout>
        );
    }
}

export default CmsLayout;
