import * as React from 'react';

import { Layout, BackTop } from 'antd';

import '../assets/global.less';

import './MainLayout.less';
import HondaHeader from '../components/honda/headers/HondaHeader';
import HondaFooter from '../components/honda/footers/HondaFooter';

const { Content } = Layout;

class MainLayout extends React.Component {
    render() {
        const { refs } = this.props;

        return (
            <Layout className="mainLayout">
                <HondaHeader isAlreadyJoinedContest={this.props.isAlreadyJoinedContest} refs={refs} />
                <Layout>
                    <Content className="contentLayout">{this.props.children}</Content>
                </Layout>
                <HondaFooter />
                {/* <BackTop /> */}
            </Layout>
        );
    }
}

export default MainLayout;
