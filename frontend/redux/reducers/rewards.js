import { handleActions } from 'redux-actions';
import { FETCH_KOL_REWARDS_SUCCEEDED, FETCH_USER_REWARDS_SUCCEEDED } from '../action-types';

const initialState = {
    user: { data: [] },
    kol: { data: [] },
};

export default handleActions(
    {
        [FETCH_KOL_REWARDS_SUCCEEDED]: (state, action) => ({ ...state, kol: action.payload }),
        [FETCH_USER_REWARDS_SUCCEEDED]: (state, action) => ({ ...state, user: action.payload }),
    },
    initialState,
);
