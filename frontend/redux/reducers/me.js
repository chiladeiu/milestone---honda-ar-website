import { handleActions } from 'redux-actions';
import {
    FIREBASE_AUTH_CHANGE_SUCCEEDED,
    LOGIN_WITH_FACEBOOK_SUCCEEDED,
    GET_PROFILE_SUCCEEDED,
    UPLOAD_VIDEO_PROGRESS_CHANGE,
    LOGOUT,
    LOGOUT_SUCCEEDED,
    LOGIN_WITH_ZALO_SUCCEEDED,
} from '../action-types';

const initialState = {
    data: {},
    isLoggedIn: false,
    isInitFirebase: false,
    role: '',
};

export default handleActions(
    {
        [LOGOUT_SUCCEEDED]: (state, action) => ({ ...initialState, isInitFirebase: true }),
        [FIREBASE_AUTH_CHANGE_SUCCEEDED]: (state, action) => ({ ...state, ...action.payload, isInitFirebase: true }),
        [LOGIN_WITH_FACEBOOK_SUCCEEDED]: (state, action) => ({ ...state, ...action.payload }),
        [LOGIN_WITH_ZALO_SUCCEEDED]: (state, action) => ({ ...state, ...action.payload }),
        [GET_PROFILE_SUCCEEDED]: (state, action) => ({ ...state, data: action.payload }),
        [UPLOAD_VIDEO_PROGRESS_CHANGE]: (state, action) => ({ ...state, uploadProgress: action.payload }),
    },
    initialState,
);
