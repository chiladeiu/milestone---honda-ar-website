import { combineReducers } from 'redux';

import me from './me';
import kolVideos from './kol-videos';
import userVideos from './user-videos';
import rewards from './rewards';

export default function createRootReducer() {
    return combineReducers({
        me,
        kolVideos,
        userVideos,
        rewards,
    });
}
