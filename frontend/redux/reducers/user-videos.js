import { handleActions } from 'redux-actions';
import {
    USER_FETCH_VIDEOS_SUCCEEDED,
    USER_FETCH_CURRENT_SUCCEEDED,
    USER_RESET_FETCHED_VIDEOS,
    USER_VOTE_VIDEO,
    USER_VOTE_VIDEO_SUCCEEDED,
} from '../action-types';
import _ from 'lodash';

const initialState = {
    data: [],
    current: {},
};

export default handleActions(
    {
        [USER_RESET_FETCHED_VIDEOS]: (state, action) => ({ ...initialState }),
        [USER_FETCH_VIDEOS_SUCCEEDED]: (state, action) => ({ ...state, data: [...state.data, ...action.payload] }),
        [USER_FETCH_CURRENT_SUCCEEDED]: (state, action) => ({ ...state, current: action.payload }),
        [USER_VOTE_VIDEO_SUCCEEDED]: (state, action) => {
            const { videoId, userId } = action.payload;
            const videoData = state.data;
            const idx = _.findIndex(videoData, v => v.id === videoId);

            if (idx >= 0) {
                videoData[idx].voted.push(userId);
                videoData[idx].voteCount += 1;
            }

            return {
                ...state,
                data: [...videoData],
            };
        },
    },
    initialState,
);
