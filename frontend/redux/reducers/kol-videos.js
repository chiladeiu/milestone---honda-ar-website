import { handleActions } from 'redux-actions';
import { KOL_FETCH_VIDEOS_SUCCEEDED } from '../action-types';

const initialState = {
    data: [],
};

export default handleActions(
    {
        [KOL_FETCH_VIDEOS_SUCCEEDED]: (state, action) => ({ ...state, data: action.payload }),
    },
    initialState,
);
