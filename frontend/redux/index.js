import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';

import createRootReducer from './reducers';
import { IS_DEV } from '../config';

const logger = createLogger({
    level: 'info',
    collapsed: true,
});
// => remove redux logger for production

export function initializeStore(initialState) {
    return IS_DEV
        ? createStore(createRootReducer(), initialState, composeWithDevTools(applyMiddleware(thunk, logger)))
        : createStore(createRootReducer(), initialState, composeWithDevTools(applyMiddleware(thunk)));
}
