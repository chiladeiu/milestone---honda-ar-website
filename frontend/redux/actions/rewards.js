import { bindActionCreators } from 'redux';
import { createAction } from 'redux-actions';
import { createActionThunk } from 'redux-thunk-actions';
import _ from 'lodash';
import moment from 'moment';
import { FETCH_USER_REWARDS, FETCH_KOL_REWARDS } from '../action-types';
import { RewardsAPI } from '../../api/rewards';

export const fetchUserRewards = createActionThunk(FETCH_USER_REWARDS, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [week] = extras;
        const response = await RewardsAPI.fetch('user', week);
        resolve(response.data.payload);
    });
});

export const fetchKOLRewards = createActionThunk(FETCH_KOL_REWARDS, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [week] = extras;
        const response = await RewardsAPI.fetch('kol', week);
        resolve(response.data.payload);
    });
});

export function bindRewardsActions(currentActions, dispatch) {
    return {
        ...currentActions,
        rewardsActions: bindActionCreators(
            {
                fetchUserRewards,
                fetchKOLRewards,
            },
            dispatch,
        ),
    };
}
