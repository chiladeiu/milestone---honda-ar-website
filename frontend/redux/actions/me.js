import { bindActionCreators } from 'redux';
import { createAction } from 'redux-actions';
import { createActionThunk } from 'redux-thunk-actions';
import _ from 'lodash';
import moment from 'moment';

import { MeAPI } from '../../api/me';

import {
    SET_TOKEN,
    GET_INFORMATION,
    FIREBASE_AUTH_CHANGE,
    LOGIN_WITH_FACEBOOK,
    LOGIN_WITH_FIREBASE,
    GET_PROFILE,
    UPDATE_PROFILE,
    UPLOAD_VIDEO,
    UPLOAD_VIDEO_PROGRESS_CHANGE,
    LOGOUT,
    LOGIN_WITH_ZALO,
    RESET_USER_DATA,
    RESET_PROFILE,
} from '../action-types';
import { FIREBASE_TOKEN_KEY, FB_APPID, ZALO_CONFIG, CMSRole } from '../../config';
import { FacebookSDK } from '../../lib/wrapper/facebook';
import { FirebaseSDK } from '../../lib/wrapper/firebase';
import { ZaloSDK } from '../../lib/wrapper/zalo';

export const resetProfile = createAction(RESET_PROFILE);

export const updateFirebaseAuthState = createActionThunk(FIREBASE_AUTH_CHANGE, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [user] = extras;

        // console.log('[Firebase] On auth state change');
        // console.log('User', user);

        if (!_.isEmpty(user)) {
            const token = await user.getIdToken();
            localStorage.setItem(FIREBASE_TOKEN_KEY, `Bearer ${token}`);
            let role = '';
            if (user.uid.slice(0, 2) === 'fb') {
                dispatch(getProfile());
            } else if (user.email === 'fractalcompany123@gmail.com') {
                role = CMSRole.Admin;
            } else {
                role = CMSRole.Viewer;
            }

            resolve({
                token,
                uid: user.uid,
                isLoggedIn: true,
                role,
            });
        } else {
            resolve({});
        }
    });
});

export const getProfile = createActionThunk(GET_PROFILE, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const response = await MeAPI.getProfile();

        resolve({
            ...response.data.payload,
        });
    });
});

export const updateProfile = createActionThunk(UPDATE_PROFILE, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [profile] = extras;

        const response = await MeAPI.updateProfile(profile);
        dispatch(getProfile());

        resolve({
            ...response.data.payload,
        });
    });
});

export const loginWithFacebook = createActionThunk(LOGIN_WITH_FACEBOOK, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const fbStatusResponse = await FacebookSDK.getLoginStatus();

        const { dispatch, getState } = extras.pop();
        let fbToken = '';

        fbToken = FacebookSDK.parseAuthResponse(fbStatusResponse);

        if (_.isEmpty(fbToken)) {
            console.log('[ME FB force login]');
            fbToken = FacebookSDK.parseAuthResponse(await FacebookSDK.login());
        }

        if (_.isEmpty(fbToken)) {
            reject('Invalid token');
        }

        resolve({
            facebookToken: fbToken,
            zaloToken: '',
        });
    });
});

export const loginWithZalo = createActionThunk(LOGIN_WITH_ZALO, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [code] = extras;

        const zlOathResponse = await ZaloSDK.getAccessToken(ZALO_CONFIG.appId, ZALO_CONFIG.appSecret, code);
        const { access_token: zlToken } = zlOathResponse.data;

        console.log('Zalo token', zlToken);

        if (_.isEmpty(zlToken)) {
            reject('Zalo login error');
        }

        resolve({
            zaloToken: zlToken,
            facebookToken: '',
        });
    });
});

export const loginFirebase = createActionThunk(LOGIN_WITH_FIREBASE, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const { facebookToken, zaloToken } = getState().me;

        // console.log('Begin Firebase login');

        try {
            const response = await MeAPI.requestLoginToken({ facebookToken, zaloToken });
            const loginToken = response.data.payload;

            // console.log('Login Token', loginToken);

            const firebaseResponse = await FirebaseSDK.login(loginToken);
            // console.log('Login success');

            resolve(true);
        } catch (error) {
            console.log(error);
        }
    });
});

export const loginAdmin = createActionThunk(LOGIN_WITH_FIREBASE, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [email, password] = extras;

        try {
            const firebaseResponse = await FirebaseSDK.loginWithEmail(email, password);

            resolve(true);
        } catch (error) {
            console.log(error);
        }
    });
});

export const uploadVideoProgressChange = createAction(UPLOAD_VIDEO_PROGRESS_CHANGE, progress => progress);

export const uploadVideo = createActionThunk(UPLOAD_VIDEO, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [file] = extras;

        const { uid } = getState().me;

        try {
            // const requestVideoUploadPath = await MeAPI.requestVideoUpload();
            // const uploadPath = requestVideoUploadPath.data.payload;
            // console.log('uploadPath', uploadPath);

            // const uploadPath =
            //     'https://storage.googleapis.com/honda-ar-ac35d.appspot.com/fb1155162908026398_video_1566984061?GoogleAccessId=honda-ar-ac35d%40appspot.gserviceaccount.com&Expires=1566984361&Signature=TToJDY8SKX%2BaL9ShlH0c%2BQR5Dvbwta%2BaAemO1tKMUO2VfI6Zgt%2Be7LuCpFwRr09nNcitvOa6jiGH5UerB%2FaL88TmpLQvd0zy5RNv7GgpG7ltcsVGwRgI1o0qjqGb1oYxkj03hhXvMOynQjE5DT0y8ZBuO87gnClqaLMPVsTXJqd0byhb8G9ZL91ki2epSW84AeHBimuw2W3G68EdTBSWPfDkPKsJbvFoa2qwY5HkJz0R9BcksAvrMh%2FUCjV%2BQSGo7L%2FcFTvootOzXeDdnG702iu68Ijy%2BRySGaEOSmpgFfmIBvRckEcePYX%2BP6tDKZG3P3HBa0KS08WdI%2BpnXCM1bg%3D%3D';

            // // const uploadResponse = await MeAPI.uploadVideo(file, uploadPath);
            // // console.log('uploadResponse', uploadResponse);

            // const req = new XMLHttpRequest();

            // const formData = new FormData();
            // formData.append('file', file, file.name);

            // req.open('PUT', uploadPath);
            // req.setRequestHeader('Origin', 'http://localhost:3030');
            // req.send(formData);

            const res = await FirebaseSDK.upload(file, `${uid}_${moment().unix()}_video.mp4`, progress => {
                dispatch(uploadVideoProgressChange(progress));
            });
            resolve();
        } catch (error) {
            console.log(error);
        }
    });
});

export const logout = createActionThunk(LOGOUT, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();

        const { uid } = getState().me;
        const type = uid.slice(0, 2);

        await FirebaseSDK.logout();
        try {
            if (type === 'fb') {
                const response = await FacebookSDK.logout();
                // console.log('[FB Logout response]', response);
            }
        } catch (error) {
            console.log(error);
        }

        resolve(true);
    });
});

export function bindMeActions(currentActions, dispatch) {
    return {
        ...currentActions,
        meActions: bindActionCreators(
            {
                // updateInformation,
                loginWithFacebook,
                loginWithZalo,
                loginFirebase,
                updateProfile,
                uploadVideo,
                uploadVideoProgressChange,
                logout,
                getProfile,
                loginAdmin,
            },
            dispatch,
        ),
    };
}
