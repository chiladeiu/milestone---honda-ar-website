import { bindActionCreators } from 'redux';
import { createAction } from 'redux-actions';
import { createActionThunk } from 'redux-thunk-actions';
import _ from 'lodash';
import moment from 'moment';

import { MeAPI } from '../../api/me';
import { USER_FETCH_VIDEOS, USER_VOTE_VIDEO, USER_FETCH_CURRENT, USER_RESET_FETCHED_VIDEOS } from '../action-types';
import { UserVideosAPI } from '../../api/user-videos';
import { DEFAULT_USER_FETCH_LIMIT, MOST_VOTED_FETCH_LIMIT } from '../../config';

export const resetFetchedVideos = createAction(USER_RESET_FETCHED_VIDEOS);

export const fetchVideos = createActionThunk(USER_FETCH_VIDEOS, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [lastVideoId] = extras;

        if (_.isUndefined(lastVideoId)) {
            dispatch(resetFetchedVideos());
        }

        const response = await UserVideosAPI.fetch(DEFAULT_USER_FETCH_LIMIT, lastVideoId);

        resolve(response.data.payload);
    });
});

export const searchByName = createActionThunk(USER_FETCH_VIDEOS, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [username] = extras;

        console.log('Search name', username);

        dispatch(resetFetchedVideos());

        const response = await UserVideosAPI.search(username);
        resolve(response.data.payload);
    });
});

export const fetchMostVotedVideos = createActionThunk(USER_FETCH_VIDEOS, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [weekId] = extras;

        const response = await UserVideosAPI.fetchMostVoted(weekId, MOST_VOTED_FETCH_LIMIT);
        dispatch(resetFetchedVideos());

        resolve(response.data.payload);
    });
});

export const fetchOne = createActionThunk(USER_FETCH_CURRENT, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [videoId] = extras;

        try {
            const response = await UserVideosAPI.fetchOne(videoId);
            resolve(response.data.payload);
        } catch (error) {
            reject(error);
        }
    });
});

export const voteVideo = createActionThunk(USER_VOTE_VIDEO, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [videoId] = extras;

        const response = await UserVideosAPI.vote(videoId);
        resolve({ videoId, userId: getState().me.uid });
    });
});

export function bindUserVideosActions(currentActions, dispatch) {
    return {
        ...currentActions,
        userVideosActions: bindActionCreators(
            {
                fetchVideos,
                fetchMostVotedVideos,
                voteVideo,
                fetchOne,
                resetFetchedVideos,
                searchByName,
            },
            dispatch,
        ),
    };
}
