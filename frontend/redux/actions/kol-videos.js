import { bindActionCreators } from 'redux';
import { createAction } from 'redux-actions';
import { createActionThunk } from 'redux-thunk-actions';
import _ from 'lodash';
import moment from 'moment';

import { MeAPI } from '../../api/me';

import { KOL_FETCH_VIDEOS, KOL_VOTE_VIDEO } from '../action-types';
import { KOLVideosAPI } from '../../api/kol-videos';

export const fetchVideos = createActionThunk(KOL_FETCH_VIDEOS, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const response = await KOLVideosAPI.fetch();

        resolve(response.data.payload);
    });
});

export const voteVideo = createActionThunk(KOL_VOTE_VIDEO, (...extras) => {
    return new Promise(async (resolve, reject) => {
        const { dispatch, getState } = extras.pop();
        const [videoId] = extras;

        const response = await KOLVideosAPI.vote(videoId);
        dispatch(fetchVideos());
        resolve();
    });
});

export function bindKOLVideosActions(currentActions, dispatch) {
    return {
        ...currentActions,
        kolVideosActions: bindActionCreators(
            {
                fetchVideos,
                voteVideo,
            },
            dispatch,
        ),
    };
}
