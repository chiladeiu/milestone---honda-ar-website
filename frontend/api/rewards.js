import { axios } from '../lib/custom-axios';
import { API_URL, TEST_ID } from '../config';

export const RewardsAPI = {
    fetch: (type, week) => {
        return axios.get(`${API_URL}rewards`, {
            params: {
                type,
                week,
            },
        });
    },
};
