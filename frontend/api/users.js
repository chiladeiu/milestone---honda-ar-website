import { axios } from '../lib/custom-axios';
import { API_URL } from '../config';

export const UsersAPI = {
    get: (limit, lastUserId) => {
        return axios.get(`${API_URL}users`, {
            params: {
                limit,
                lastUserId,
            },
        });
    },
    count: () => {
        return axios.get(`${API_URL}users/count`);
    },
    export: () => {
        axios
            .get(`${API_URL}exporter/users`, {
                responseType: 'blob',
            })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'users-data.xlsx');
                document.body.appendChild(link);
                link.click();
            });
    },
};
