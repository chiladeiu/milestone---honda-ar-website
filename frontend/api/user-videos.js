import { axios } from '../lib/custom-axios';
import { API_URL, TEST_ID } from '../config';

export const UserVideosAPI = {
    fetch: (limit = 6, lastVideoId = null) => {
        return axios.get(`${API_URL}userVideos`, {
            params: {
                limit,
                lastVideoId,
            },
        });
    },
    fetchMostVoted: (week = null, limit = 6) => {
        return axios.get(`${API_URL}userVideos`, {
            params: {
                week,
                limit,
                sortBy: 'vote',
            },
        });
    },
    search: username => {
        return axios.get(`${API_URL}userVideos`, {
            params: {
                username,
            },
        });
    },
    fetchOne: videoId => {
        return axios.get(`${API_URL}userVideos/${videoId}`);
    },
    vote: videoId => {
        return axios.post(`${API_URL}userVideos/${videoId}/vote`);
    },
    adminFetch: (limit, lastVideoId) => {
        return axios.get(`${API_URL}userVideos/admins`, {
            params: {
                limit,
                lastVideoId,
            },
        });
    },
    adminDelete: videoId => {
        return axios.delete(`${API_URL}userVideos/admins/${videoId}`);
    },
    adminCountVideos: () => {
        return axios.get(`${API_URL}userVideos/admins/count`);
    },
    export: () => {
        axios
            .get(`${API_URL}exporter/videos`, {
                responseType: 'blob',
            })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'videos-data.xlsx');
                document.body.appendChild(link);
                link.click();
            });
    },
};
