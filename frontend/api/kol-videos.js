import { axios } from '../lib/custom-axios';
import { API_URL, TEST_ID } from '../config';

export const KOLVideosAPI = {
    fetch: () => {
        return axios.get(`${API_URL}kolVideos`);
    },
    vote: videoId => {
        return axios.post(`${API_URL}kolVideos/${videoId}/vote`);
    },
    adminGetVoted: () => {
        return axios.get(`${API_URL}kolVideos/admins/voted`);
    },
    export: () => {
        axios
            .get(`${API_URL}exporter/kol`, {
                responseType: 'blob',
            })
            .then(response => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', 'kol-vote-data.xlsx');
                document.body.appendChild(link);
                link.click();
            });
    },
};
