import { axios } from '../lib/custom-axios';
import { API_URL } from '../config';

export const MeAPI = {
    getProfile: () => {
        return axios.get(`${API_URL}me/profile`);
    },
    requestLoginToken: tokenPack => {
        return axios.post(`${API_URL}auth/request`, tokenPack);
    },
    updateProfile: data => {
        return axios.patch(`${API_URL}me/profile`, data);
    },
    // requestVideoUpload: () => {
    //     return axios.post(`${API_URL}videos/request`);
    // },
    // uploadVideo: (file, uploadPath) => {
    //     return axios.put(uploadPath, file, {
    //         headers: {
    //             'Content-Type': 'video/mp4',
    //             Origin: 'https://localhost:3030/',
    //         },
    //     });
    // },
};
