const fs = require('fs-extra');
const path = require('path');
const _ = require('lodash');

const data = fs.readJsonSync(path.join(__dirname, '../I18n/languages/en.json'));

let interfaceData = [];
let i18nData = [];

console.log('1. Parse data');
for (var key in data) {
    if (data.hasOwnProperty(key)) {
        if (!key.match(/^[A-Za-z0-9]+$/g)) {
            console.log('[ERROR] Key must be contain letter / number only. Key:', key);
            process.exit(1);
        }
        i18nData.push(`    static ${key} = () => i18next.t('${key}');`);
    }
}

let outputFileContent = `
// !! THIS FILE IS GENERATED - DO NOT MODIFY MANUALY !!
// !! RUN 'yarn lang' IF YOU WANT TO UPDATE THE LANGUAGE SET !!

import i18next from 'i18next';

export class LANG {
${i18nData.join(`\n`)}
}

let rootApp = null;
export function setRootApp(app) {
    rootApp = app;
}

export function changeLanguage(lng) {
    i18next.changeLanguage(lng).then(() => {
        if (rootApp) {
            rootApp.applyLanguage();
        }
    });
}

export function getLanguage() {
    return i18next.getLanguage();
}
`;

console.log('2. Write file');
fs.writeFileSync(path.join(__dirname, '../lang.js'), outputFileContent);

console.log('=> DONE');
