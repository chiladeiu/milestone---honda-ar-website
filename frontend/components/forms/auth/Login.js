import React from 'react';
import Link from 'next/link';
import Router from 'next/router';

import '../../../layout/AuthTemplate.less';
import './Login.less';

import { Icon, Col, Row, Input, Button, Form, Divider } from 'antd';

class Login extends React.Component {
    render() {
        const { getFieldDecorator } = this.props.form;

        return (
            <div className="authTemplate-body">
                <div className="authTemplate-bodyBox">
                    <Form layout="vertical" hideRequiredMark>
                        <Row>
                            <Col className="title">ĐĂNG NHẬP</Col>
                            <Col>
                                <Form.Item label="Email">
                                    {getFieldDecorator('email', {
                                        rules: [{ required: true, message: 'Không được để trống' }],
                                    })(<Input />)}
                                </Form.Item>
                                <Form.Item label="Password">
                                    {getFieldDecorator('password', {
                                        rules: [{ required: true, message: 'Không được để trống' }],
                                    })(<Input type="password" />)}
                                </Form.Item>
                            </Col>
                            {/* <Col className="textAlignRight">
                                <Link href={`${pathname}/recover-password`}><a>Khôi phục mật khẩu? <Icon type="arrow-right"/></a></Link>
                            </Col> */}
                        </Row>
                        <Row>
                            <Col span={10}>
                                <Button className="logIn" onClick={this.props.onLogin} loading={this.props.isLogingin}>
                                    Đăng nhập
                                </Button>
                            </Col>
                        </Row>
                        {/* <Row>
                            <Col span={24}>
                                <span>Chưa có tài khoản?</span> <Link href={`${pathname}/signup`}><a>Đăng ký</a></Link>
                            </Col>
                        </Row>
                        <Row gutter={20}>
                            <Col span={24}>
                                <Divider orientation="left">Hoặc đăng nhập bằng</Divider>
                            </Col>
                            <Col span={12}>
                                <Button className="facebook">
                                    <Icon type="facebook" />
                                    Facebook
                                </Button>
                            </Col>
                            <Col span={12}>
                                <Button className="google">
                                    <Icon type="google" />
                                    Google
                                </Button>
                            </Col>
                        </Row> */}
                    </Form>
                </div>
            </div>
        );
    }
}

export default Login;
