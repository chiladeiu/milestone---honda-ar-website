import React from 'react';
import Link from 'next/link';
import Router from 'next/router';

import '../../../layout/AuthTemplate.less';
import './SignUp.less';

import {
    Col, Row, Input, Button, Form
} from 'antd';

class SignUp extends React.Component {
    constructor (props) {
        super(props);
    }
    
    render () {
        const { getFieldDecorator } = this.props.form;
        const { pathname } = Router;
        
        return (
            <div className="authTemplate-body">
                <div className="authTemplate-bodyBox">
                    <Form layout="vertical" hideRequiredMark>
                        <Row gutter={20}>
                            <Col className="title">ĐĂNG KÝ</Col>
                            <Col span={12}>
                                <Form.Item label="Tên">
                                    {getFieldDecorator('lastName', {
                                        rules: [{ required: true, message: 'Không được để trống' }]
                                    })(<Input />)}
                                </Form.Item>
                            </Col>
                            <Col span={12}>
                                <Form.Item label="Họ">
                                    {getFieldDecorator('firstName', {
                                        rules: [{ required: true, message: 'Không được để trống' }]
                                    })(<Input />)}
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item label="Email">
                                    {getFieldDecorator('email', {
                                        rules: [{ required: true, message: 'Không được để trống' }]
                                    })(<Input />)}
                                </Form.Item>
                                <Form.Item label="Mật khẩu">
                                    {getFieldDecorator('password', {
                                        rules: [{ required: true, message: 'Không được để trống' }]
                                    })(<Input />)}
                                </Form.Item>
                            </Col>
                        </Row>
                        {/* <Row>
                            <Col span={24}>
                                Khi nhấn vào đăng ký, tôi đã đọc và đồng ý các <Link href="/"><a>điều khoản sử dụng</a></Link>.
                            </Col>
                        </Row> */}
                        <Row>
                            <Col span={10}>
                                <Button className="signUp">Đăng ký</Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24}>
                                <span>Đã có tài khoản?</span> <Link href={`${pathname}/login`}><a>Đăng nhập</a></Link>
                            </Col>
                        </Row>
                    </Form>
                </div>
            </div>
        );
    }
}

export default SignUp;