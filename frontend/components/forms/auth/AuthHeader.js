import React from 'react';
import Link from 'next/link';

import '../../../layout/AuthTemplate.less';
import './AuthHeader.less';

import { ROUTES, SITE_NAME } from '../../../config';

class AuthHeader extends React.Component {
    render () {
        return (
            <div className="authHeader">
                <div className="authHeaderBox">
                    <div className="logo">
                        <Link prefetch href={ROUTES.HOME}>
                            <a><img src={'../static/shared/logo.svg'} alt={SITE_NAME}/></a>
                        </Link>
                    </div>
                    <div className="siteName">
                        {SITE_NAME}
                    </div>
                </div>
            </div>
        );
    }
}

export default AuthHeader;
