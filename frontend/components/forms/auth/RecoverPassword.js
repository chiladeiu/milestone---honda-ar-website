import React from 'react';
import Link from 'next/link';
import Router from 'next/router';

import '../../../layout/AuthTemplate.less';
import './RecoverPassword.less';

import {
    Icon, Col, Row, Input, Button, Form
} from 'antd';

class RecoverPassword extends React.Component {
    static async getInitialProps (ctx) {
        return {
            code: ctx.query.code
        };
    }

    constructor (props) {
        super(props);
    }

    render () {
        const { getFieldDecorator } = this.props.form;
        const { pathname } = Router;

        return (
            <div className="authTemplate-body">
                <div className="authTemplate-bodyBox">

                    { this.props.code === undefined &&
                    <Form layout="vertical" hideRequiredMark>
                        <Row>
                            <Col className="title">KHÔI PHỤC MẬT KHẨU</Col>
                            <Col>
                                <Form.Item label="Email">
                                    {getFieldDecorator('email', {
                                        rules: [{ required: true, message: 'Không được để trống' }]
                                    })(<Input />)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={10}>
                                <Button className="recoverPassword">Khôi phục</Button>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={24} className="textAlignLeft">
                                <Link href={`${pathname}/login`}><a><Icon type="arrow-left"/> Đăng nhập</a></Link>
                            </Col>
                        </Row>
                    </Form>
                    }

                    { this.props.code !== undefined &&
                    <Form layout="vertical" hideRequiredMark>
                        <Row>
                            <Col className="title">NHẬP MẬT KHẨU MỚI</Col>
                            <Col>
                                <Form.Item label="Mật khẩu">
                                    {getFieldDecorator('password', {
                                        rules: [{ required: true, message: 'Không được để trống' }]
                                    })(<Input />)}
                                </Form.Item>
                                <Form.Item label="Nhắc lại mật khẩu">
                                    {getFieldDecorator('passwordRepeat', {
                                        rules: [{ required: true, message: 'Không được để trống' }]
                                    })(<Input />)}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={10}>
                                <Button className="recoverPassword">Đặt lại mật khẩu</Button>
                        </Col>
                        </Row>
                    </Form>
                    }

                </div>
            </div>
        );
    }
}

export default RecoverPassword;
