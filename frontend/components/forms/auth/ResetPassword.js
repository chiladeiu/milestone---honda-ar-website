import React from 'react';
import Link from 'next/link';
import Router from 'next/router';
import _ from 'lodash';

import '../../../layout/AuthTemplate.less';
import './ResetPassword.less';

import {
    Layout, Icon, Col, Row, Input, Button, Form, Divider
} from 'antd';

import { ROUTES, SITE_NAME } from '../../../config';
import { serverRedirect } from '../../../lib/helper';

class ResetPassword extends React.Component {
    checkConfirmPassword = (rule, value, callback) => {
        const newPassword = this.props.form.getFieldValue('password');
        if (value === newPassword) {
            callback();
            return;
        }
        // eslint-disable-next-line standard/no-callback-literal
        callback('Xác nhận mật khẩu mới không trùng khớp');
    }

    render () {
        const { getFieldDecorator } = this.props.form;
        const { pathname } = Router;

        return (
            <div className="authTemplate-body">
                <div className="authTemplate-bodyBox">
                    { this.props.code === undefined &&
                    <Form layout="vertical" hideRequiredMark>
                        <Row>
                            <Col className="title">NHẬP MẬT KHẨU MỚI</Col>
                            <Col>
                                <Form.Item label="Mật khẩu mới">
                                    {getFieldDecorator('password', {
                                        rules: [{ required: true, message: 'Không được để trống' }]
                                    })(<Input.Password />)}
                                </Form.Item>
                                <Form.Item label="Nhắc lại mật khẩu">
                                    {getFieldDecorator('confirmNewPassword', {
                                        rules: [{
                                            validator: this.checkConfirmPassword
                                        }]
                                    })(
                                        <Input.Password/>
                                    )}
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row>
                            <Col span={10}>
                                <Button onClick={this.props.onClickReset} className="ResetPassword">Đặt lại mật khẩu</Button>
                            </Col>
                        </Row>
                    </Form>
                    }

                </div>
            </div>
        );
    }
}

export default ResetPassword;
