import React from 'react';
import { withRouter } from 'next/router';
import Link from 'next/link';
import _ from 'lodash';
import classnames from 'classnames';

import './BasicMenu.less';
import { Drawer, Button, Icon } from 'antd';

class BasicMenu extends React.Component {
    getRoutePath () {
        const { asPath: routePath } = this.props.router;
        return routePath;
    }

    constructor (props) {
        super(props);

        this.state = {
            isMobileMenuDrawerVisible: false
        };
    }

    componentDidMount () {
        this.hideMobileMenuDrawer();
        window.addEventListener('resize', this.hideMobileMenuDrawer);
    }

    componentWillUnmount () {
        window.removeEventListener('resize', this.hideMobileMenuDrawer);
    }

    showMobileMenuDrawer = () => {
        this.setState({
            isMobileMenuDrawerVisible: true
        });
    };

    hideMobileMenuDrawer = () => {
        this.setState({
            isMobileMenuDrawerVisible: false
        });
    };

    renderDesktopMenu = menu => {
        return (
            <div className="desktopMenu">
                {menu.map(item => (
                    <div className={classnames('item', { 'active': item.link === this.getRoutePath() })}>
                        <Link href={item.link}>
                            <a>
                                {/* <Icon type={item.icon}/>  */}
                                <span>{item.text}</span>
                            </a>
                        </Link>
                    </div>
                ))}
            </div>
        );
    };

    renderMobileMenu = menu => {
        return (
            <>
                <Button onClick={this.showMobileMenuDrawer} className="mobileMenuButton">
                    <Icon className="mobileMenuButtonIcon" type="menu-fold" />
                </Button>

                <Drawer
                    className="mobileMenuDrawer"
                    placement="right"
                    closable={true}
                    onClose={this.hideMobileMenuDrawer}
                    visible={this.state.isMobileMenuDrawerVisible}
                    width="300"
                >
                    <div className="mobileMenu">
                        {menu.map(item => (
                            <div className={classnames('item', { 'active': item.link === this.getRoutePath() })}>
                                <Link href={item.link}>
                                    <a>
                                        {/* <Icon type={item.icon}/>  */}
                                        <span>{item.text}</span>
                                    </a>
                                </Link>
                            </div>
                        ))}
                    </div>
                </Drawer>
            </>
        );
    };

    render () {
        const { menu } = this.props;

        return (
            <div className="basicMenu">
                <div className="basicMenuBox">
                    {this.renderDesktopMenu(menu)}
                    {this.renderMobileMenu(menu)}
                </div>
            </div>
        );
    }
}

export default withRouter(BasicMenu);
