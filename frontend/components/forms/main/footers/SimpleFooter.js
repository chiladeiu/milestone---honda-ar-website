import React from 'react';
import Link from 'next/link';

import './SimpleFooter.less';
import { Layout, Col, Row, Divider } from 'antd';
import { SITE_NAME } from '../../../../config';

import { LANG } from '../../../../lang';

const { Footer } = Layout;

class SimpleFooter extends React.Component {
    render() {
        return (
            <Footer className="simpleFooter">
                <div className="simpleFooterBox">
                    <Row className="simpleFooterInfo" type="flex">
                        <Col className="logo" xs={12} sm={12} md={6} lg={6} xl={4} xxl={4}>
                            <img src={'/static/shared/logo.svg'} alt={SITE_NAME} />
                        </Col>

                        <Col className="detail" xs={12} sm={12} md={18} lg={18} xl={20} xxl={20}>
                            <Col className="menu" xs={24} sm={24} md={12} lg={8} xl={16} xxl={16}>
                                <Row>
                                    <Col  xs={24} sm={24} md={12} lg={8} xl={6} xxl={6}>
                                        <h3>Title</h3>
                                    </Col>
                                    <Col  xs={24} sm={24} md={12} lg={8} xl={6} xxl={6}>
                                        <h3>Title</h3>
                                    </Col>
                                    <Col  xs={24} sm={24} md={12} lg={8} xl={6} xxl={6}>
                                        <h3>Title</h3>
                                    </Col>
                                    <Col  xs={24} sm={24} md={12} lg={8} xl={6} xxl={6}>
                                        <h3>Title</h3>
                                    </Col>
                                </Row>
                            </Col>

                            <Col className="qrCode" xs={24} sm={24} md={12} lg={8} xl={6} xxl={6}>
                                <h3>QR Code</h3>
                            </Col>
                        </Col>
                    </Row>
                </div>
            </Footer>
        );
    }
}

export default SimpleFooter;
