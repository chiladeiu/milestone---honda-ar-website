import React from 'react';
import Link from 'next/link';

import './MainFooter.less';
import { Layout, Icon, Col, Row } from 'antd';
import { ROUTES, SITE_NAME } from '../../../../config';

import { LANG, changeLanguage } from '../../../../lang';
import i18next from 'i18next';
import { lang } from 'moment';

const { Footer } = Layout;

class MainFooter extends React.Component {
    render () {
        return (
            <Footer className="mainFooter">
                <div className="mainFooterBox">
                    <Row>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8} className="info">
                            <h3>{LANG.FooterFindUs()}</h3>
                            <a href="https://goo.gl/maps/q1aLYjAvkbvHnUqSA" target="_blank">
                                <img src={'/static/main/map.png'} alt="map" />
                            </a>
                            <p>{LANG.FooterAddress()}</p>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8} className="info">
                            <h3>{LANG.FooterAskForHelp()}</h3>
                            <p>
                                <Link href="/contact">
                                    <a>{LANG.FooterContactButtonText()}</a>
                                </Link>
                            </p>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8} xl={8} className="info">
                            <h3>{LANG.FooterAboutTitle()}</h3>
                            <p>{LANG.FooterBrief()}</p>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24} className="info">
                            <img className="logo" src={'/static/shared/logo.svg'} alt={SITE_NAME} />
                        </Col>
                        <Col span={24} className="info">
                            <h3>{LANG.FooterSlogan()}</h3>
                            <p>
                                ©{SITE_NAME}, 2017-{new Date().getFullYear()}
                            </p>
                        </Col>
                    </Row>
                </div>
            </Footer>
        );
    }
}

export default MainFooter;
