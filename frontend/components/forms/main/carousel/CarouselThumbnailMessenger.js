import React from 'react';
import Link from 'next/link';

import './CarouselThumbnailMessenger.less';

import {
    Layout, Icon, Col, Row, Carousel
} from 'antd';
import DescriptionMessenger from '../messengers/DescriptionMessenger';

class CarouselThumbnailMessenger extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { contents, background, color } = this.props;
        return (
            <div className="carouselThumbnailMessenger" style={{ background }}>
                <div className="carouselThumbnailMessengerBox">
                    { this.props.contents.length > 1 &&
                        <a className="prev" onClick={() => { this.carousel.prev(); }}>
                            <Icon className="icon" type="caret-left"/>
                        </a>
                    }
                    <Carousel
                        autoplay
                        className="contents"
                        ref={me => { this.carousel = me; }}
                    >
                        { this.props.contents.map(content => {
                            return (
                                <Row className="content">
                                    <Col xs={24} sm={24} md={12} lg={12} xl={12} className="thumbnail">
                                        <img src={content.thumbnail} alt={content.title} />
                                    </Col>
                                    <Col xs={24} sm={24} md={12} lg={12} xl={12} className="meta">
                                        <DescriptionMessenger
                                            color={color}
                                            background={background}
                                            intro={content.intro}
                                            title={content.title}
                                            extra={content.extra}
                                            description={content.description}
                                        />
                                    </Col>
                                </Row>
                            );
                        })}
                    </Carousel>
                    { this.props.contents.length > 1 &&
                    <a className="next" onClick={() => { this.carousel.next(); }}>
                        <Icon className="icon" type="caret-right"/>
                    </a>
                    }
                </div>
            </div>
        );
    }
}

export default CarouselThumbnailMessenger;
