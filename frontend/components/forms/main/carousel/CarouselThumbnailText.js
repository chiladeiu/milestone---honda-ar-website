import React from 'react';

import './CarouselThumbnailText.less';

import {
    Icon, Col, Row, Carousel
} from 'antd';

class CarouselThumbnailText extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { contents, background, color, padding } = this.props;
        return (
            <div className="carouselThumbnailText" style={{ background, padding }}>
                <div className="carouselThumbnailTextBox">
                    { this.props.contents.length > 1 &&
                        <a className="prev" onClick={() => { this.carousel.prev(); }}>
                            <Icon className="icon" type="caret-left"/>
                        </a>
                    }
                    <Carousel
                        autoplay
                        className="contents"
                        ref={me => { this.carousel = me; }}
                    >
                        { contents.map(content => {
                            return (
                                <Row className="content">
                                    <Col xs={24} sm={24} md={12} lg={12} xl={12} className="meta">
                                        <h3>{content.title}</h3>
                                        <p>{content.category}</p>
                                        <p>{content.description}</p>
                                    </Col>
                                    <Col xs={24} sm={24} md={12} lg={12} xl={12} className="thumbnail">
                                        <img src={content.thumbnail} alt={content.title} />
                                    </Col>
                                </Row>
                            );
                        })}
                    </Carousel>
                    { this.props.contents.length > 1 &&
                    <a className="next" onClick={() => { this.carousel.next(); }}>
                        <Icon className="icon" type="caret-right"/>
                    </a>
                    }
                </div>
            </div>
        );
    }
}

export default CarouselThumbnailText;
