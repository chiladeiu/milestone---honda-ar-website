import React from 'react';
import Link from 'next/link';

import './CarouselText.less';

import { Layout, Icon, Col, Row, Carousel } from 'antd';

class CarouselText extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { background = '#fff', contents, color = '#000' } = this.props;
        return (
            <div className="carouselText" style={{ background: background }}>
                <div className="carouselTextBox">
                    {contents.length > 1 && (
                        <a
                            className="prev"
                            onClick={() => {
                                this.carousel.prev();
                            }}
                        >
                            <Icon className="icon" type="caret-left" />
                        </a>
                    )}
                    <Carousel
                        autoplay={false}
                        className="contents"
                        ref={me => {
                            this.carousel = me;
                        }}
                    >
                        {contents.map(content => {
                            return (
                                <Row className="content">
                                    <Col span={24} className="meta">
                                        <h3 style={{ color: color }}>{content.title}</h3>
                                        <p style={{ color: color, borderColor: color }}>{content.description}</p>
                                    </Col>
                                </Row>
                            );
                        })}
                    </Carousel>
                    {contents.length > 1 && (
                        <a
                            className="next"
                            onClick={() => {
                                this.carousel.next();
                            }}
                        >
                            <Icon className="icon" type="caret-right" />
                        </a>
                    )}
                </div>
            </div>
        );
    }
}

export default CarouselText;
