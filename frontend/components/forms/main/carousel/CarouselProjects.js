import React from 'react';
import Slider from 'react-slick';
import ContentItem from '../../../honda/contents/ContentItem';

import './CarouselProjects.less';

import { Icon, Col, Row, Carousel } from 'antd';

class CarouselProjects extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { contents } = this.props;

        let settings = {
            dots: false,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 3,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                    }
                },
                {
                    breakpoint: 900,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                    }
                },
                // {
                //     breakpoint: 600,
                //     settings: {
                //         slidesToShow: 1,
                //         slidesToScroll: 1,
                //     }
                // }
            ]
        };

        return (
            <div className="carouselProjects" style={{ background: this.props.background }}>
                <div className="carouselProjectsBox">
                    {this.props.contents.length > 1 && (
                        <a
                            className="prev"
                            onClick={() => {
                                this.carousel.prev();
                            }}
                        >
                            <Icon className="icon" type="left" />
                        </a>
                    )}
                    <Carousel
                        {...settings}
                        className="carouselProjectsContents"
                        ref={me => {
                            this.carousel = me;
                        }}
                    >
                        {contents.map(content => {
                            return (
                                <div className="carouselProjectsContent">
                                    <ContentItem
                                        thumbnail={content.thumbnail}
                                        name={content.author}
                                        createdAt={content.createdAt}
                                        link={content.link}
                                    />
                                </div>
                            );
                        })}
                    </Carousel>
                    {contents.length > 1 && (
                        <a
                            className="next"
                            onClick={() => {
                                this.carousel.next();
                            }}
                        >
                            <Icon className="icon" type="right" />
                        </a>
                    )}
                </div>
            </div>
        );
    }
}

export default CarouselProjects;
