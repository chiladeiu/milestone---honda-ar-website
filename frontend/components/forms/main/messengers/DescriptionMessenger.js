import React from 'react';
import Link from 'next/link';

import './DescriptionMessenger.less';

import {
    Layout, Icon, Col, Row
} from 'antd';
import { ROUTES, SITE_NAME } from '../../../../config';

class DescriptionMessenger extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { intro, title, description, extra, background, color } = this.props;
        return (
            <div className="descriptionMessenger" style={{ background }}>
                <div className="descriptionMessengerBox">
                    <p style={{ color }}>{intro}</p>
                    <h3 style={{ color }}>{title}</h3>
                    <p style={{ color }}>{extra}</p>
                    <p style={{ color }}>{description}</p>
                </div>
            </div>
        );
    }
}

export default DescriptionMessenger;
