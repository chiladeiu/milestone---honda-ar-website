import React from 'react';
import Link from 'next/link';

import './BasicMessenger.less';

import {
    Layout, Icon, Col, Row
} from 'antd';
import { ROUTES, SITE_NAME } from '../../../config';

class BasicMessenger extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { title, description, link, background, color } = this.props;
        return (
            <div className="basicMessenger" style={{ background }}>
                <div className="basicMessengerBox">
                    <h3 style={{ color }}>{title}</h3>
                    <p style={{ color }}>{description}</p>
                    { link !== undefined &&
                        <p><Link href={link.url}><a>{link.text}</a></Link></p>
                    }
                </div>
            </div>
        );
    }
}

export default BasicMessenger;
