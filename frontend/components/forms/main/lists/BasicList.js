import React from 'react';

import './BasicList.less';

import {
    Col, Row
} from 'antd';

class BasicList extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { contents, background, color } = this.props;
        return (
            <div className="basicList" style={{ background }}>
                <div className="basicListBox">
                    <Row gutter={50} type="flex">
                        { contents.map(content => {
                            return (
                                <Col xs={24} sm={24} md={12} lg={8} xl={8} className="item">
                                    <img src={content.thumbnail} alt={content.title} />
                                    <h3>{content.title}</h3>
                                    <p>{content.description}</p>
                                </Col>
                            );
                        })}
                    </Row>
                </div>
            </div>
        );
    }
}

export default BasicList;
