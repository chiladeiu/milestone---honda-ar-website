import React from 'react';
import Link from 'next/link';

import './BannerCenterFocus.less';

class BannerCenterFocus extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { intro, title, description, link, background, color } = this.props;
        return (
            <div className="bannerCenterFocus" style={{ background }}>
                <div className="bannerCenterFocusBox">
                    <p style={{ color }}>{intro}</p>
                    <h2 style={{ color }}>{title}</h2>
                    <p style={{ color }}>{description}</p>
                    <p>
                        <Link href={link.href}>
                            <a>{link.text}</a>
                        </Link>
                    </p>
                </div>
            </div>
        );
    }
}

export default BannerCenterFocus;
