import React from 'react';

import './BannerCenterCenter.less';

class BannerCenterCenter extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { intro, title, description, background, color } = this.props;
        return (
            <div className="bannerCenterCenter" style={{ background }}>
                <div className="bannerCenterCenterBox">
                    <p style={{ color }}>{intro}</p>
                    <h2 style={{ color }}>{title}</h2>
                    <p style={{ color }}>{description}</p>
                    {/* <p><a>DISCOVER MORE</a></p> */}
                </div>
            </div>
        );
    }
}

export default BannerCenterCenter;
