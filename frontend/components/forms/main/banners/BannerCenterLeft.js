import React from 'react';
import Link from 'next/link';

import './BannerCenterLeft.less';

class BannerCenterLeft extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { intro, title, description, background, color } = this.props;
        return (
            <div className="bannerCenterLeft" style={{ background }}>
                <div className="bannerCenterLeftBox">
                    <p style={{ color }}>{intro}</p>
                    <h2 style={{ color }}>{title}</h2>
                    <p style={{ color }}>{description}</p>
                    {/* <p><a>DISCOVER MORE</a></p> */}
                </div>
            </div>
        );
    }
}

export default BannerCenterLeft;
