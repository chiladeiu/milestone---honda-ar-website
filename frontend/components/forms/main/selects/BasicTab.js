import React from 'react';

import './BasicTab.less';

import { Button, Icon, Radio } from 'antd';

class BasicTab extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        let { size, tabs, onChange, value } = this.props;
        return (
            <div className="basicTab">
                <Radio.Group size={size} onChange={e => onChange(e.target.value)} value={value}>
                    {tabs &&
                        tabs.map(tab => {
                            return <Radio.Button value={tab.value}> {tab.text} </Radio.Button>;
                        })}
                </Radio.Group>
            </div>
        );
    }
}

export default BasicTab;
