import React from 'react';

import './BasicSelect.less';

import { Button, Icon, Radio, Dropdown, Menu } from 'antd';

function handleChange(value) {
    console.log(`selected ${value}`);
}

class BasicSelect extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentOption: 0,
        };
    }

    onClick(index) {
        console.log(index);
        const { onSelect = {} } = this.props;
        onSelect(index);
    }

    render() {
        const options = [
            '09/09/2019 - 16/09/2019',
            '16/09/2019 - 23/09/2019',
            '23/09/2019 - 30/09/2019',
            '30/09/2019 - 07/10/2019',
            '07/10/2019 - 14/10/2019',
            '14/10/2019 - 21/09/2019',
        ];

        const menu = () => (
            <Menu>
                {options.map((option, index) => (
                    <Menu.Item key={index}>
                        <span onClick={index => this.onClick(index)}>
                            <b>TUẦN {index + 1}:</b> {option}
                        </span>
                    </Menu.Item>
                ))}
            </Menu>
        );

        return (
            <div className="basicSelect">
                <Dropdown placement="bottomRight" overlay={menu} trigger={['click']}>
                    {/* <div className="basicSelectButton" href="#"> */}
                        <Button>Tuần</Button>
                    {/* </div> */}
                </Dropdown>
            </div>
        );
    }
}

export default BasicSelect;
