import React from 'react';
import Link from 'next/link';

import './BasicCollapse.less';

import {
    Layout, Icon, Col, Row, Collapse
} from 'antd';
import { ROUTES, SITE_NAME } from '../../../config';

class BasicCollapse extends React.Component {
    constructor (props) {
        super(props);

        this.state = {

        };
    }

    render () {
        const { contents, background, color } = this.props;
        return (
            <div className="basicCollapse" style={{ background }}>
                <div className="basicCollapseBox">
                    <Collapse
                        accordion
                        bordered={false}
                        expandIcon={({ isActive }) => isActive ? (<Icon type="minus"/>) : (<Icon type="plus"/>)}
                        expandIconPosition="right"
                    >
                        { contents.map((content, index) => {
                            return (
                                <Collapse.Panel header={content.title} key={index}>
                                    <p>{content.description}</p>
                                </Collapse.Panel>
                            );
                        })}
                    </Collapse>
                </div>
            </div>
        );
    }
}

export default BasicCollapse;
