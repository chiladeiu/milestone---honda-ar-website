import React from 'react';

import './BasicCopy.less';

import {
    Button, Icon, Input
} from 'antd';

class BasicCopy extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { value } = this.props;
        return (
            <div className="basicCopy">
                <div className="basicCopyBox">
                    <Input
                        disabled
                        value={value}
                        prefix={<Icon type="global" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    />
                    <Button icon="copy"/>
                </div>
            </div>
        );
    }
}

export default BasicCopy;


