import React from 'react';
import Link from 'next/link';

import './BasicInput.less';

import { Input } from 'antd';

class BasicInput extends React.Component {
    render() {
        return (
            <div className="basicInput">
                <div className="basicInputBox">
                    <Input {...this.props} className="input" allowClear />
                </div>
            </div>
        );
    }
}

export default BasicInput;
