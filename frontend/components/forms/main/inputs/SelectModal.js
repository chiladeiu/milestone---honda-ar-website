import React from 'react';
import Link from 'next/link';

import './BasicModal.less';

import { Input, Modal, Row, Col, Select } from 'antd';
import { ROUTES, SITE_NAME } from '../../../../config';

class SelectModal extends React.Component {
    render() {
        const { title, visible, handleOk, handleCancel, handleChange, data, defaultValue } = this.props;
        return (
            <div className="selectModal">
                <div className="selectModalBox">
                    <Modal
                        title={'Edit ' + title}
                        visible={visible}
                        onOk={handleOk}
                        onCancel={handleCancel}
                        width="400px"
                    >
                        <Select defaultValue={defaultValue} style={{ width: 120 }} onChange={handleChange}>
                            {data &&
                                data.map((item, index) => {
                                    return <Option value={item.value}> {item.text} </Option>;
                                })}
                        </Select>
                    </Modal>
                </div>
            </div>
        );
    }
}

export default SelectModal;
