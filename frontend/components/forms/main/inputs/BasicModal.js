import React from 'react';
import Link from 'next/link';

import './BasicModal.less';

import {
    Input, Modal, Row, Col
} from 'antd';
import { ROUTES, SITE_NAME } from '../../../../config';

class BasicModal extends React.Component {

    render () {
        const { title, visible, handleOk = {}, handleCancel = {}, data } = this.props;
        return (
            <div className="basicModal">
                <div className="basicModalBox">
                    <Modal
                        title={'Edit ' + title}
                        visible={visible}
                        onOk={handleOk}
                        onCancel={handleCancel}
                        width='400px'
                    >
                        <Row>
                        {data && data.map(item => {
                            console.log(visible);
                            return (
                                
                                <Col style={{margin: '1rem auto'}}>
                                    <h4> {item.title} </h4>
                                    <Input value={item.value} />
                                </Col>
                            );
                        })}
                        </Row>
                    </Modal>
                </div>
            </div>
        );
    }
}

export default BasicModal;
