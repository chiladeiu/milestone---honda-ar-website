import React from 'react';
import Link from 'next/link';

import './BasicSearch.less';

import {
    Input
} from 'antd';
import { ROUTES, SITE_NAME } from '../../../../config';

class BasicSearch extends React.Component {
    constructor (props) {
        super(props);

        this.state = {
        };
    }

    render () {
        const { onSearch = {} } = this.props;
        return (
            <div className="basicSearch">
                <div className="basicSearchBox">
                    <Input.Search
                        className="search"
                        placeholder="Search"
                        onSearch={onSearch}
                        allowClear
                    />
                </div>
            </div>
        );
    }
}

export default BasicSearch;
