/* eslint-disable no-unused-expressions */
import React from 'react';
import { Input, Tooltip } from 'antd';
// import './Bug.less';

class NumericInput extends React.Component {
    onChange = event => {
        if (event.target === undefined) return;
        const value = event.target.value;

        if (value.length < 5 && !isNaN(value)) {
            this.props.onChange(value);
        }
    };

    // '.' at the end or only '-' in the input box.
    onBlur = () => {
        const { value, onBlur, onChange } = this.props;

        if (value === undefined || value === '') return;

        if (value.charAt(value.length - 1) === '.' || value === '-') {
            onChange(value.slice(0, -1));
        }

        if (onBlur) {
            onBlur();
        }
    };

    render() {
        return <Input {...this.props} onChange={event => this.onChange(event)} />;
    }
}

export default NumericInput;
