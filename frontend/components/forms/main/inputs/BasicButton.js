import React from 'react';

import './BasicButton.less';

import { Button, Icon } from 'antd';

class BasicButton extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { iconType, text, onClick = {} } = this.props;
        return (
            <Button className="basicButton" onClick={onClick}>
                {iconType !== undefined && <Icon type={iconType} />}
                {text !== undefined && <span>{text}</span>}
            </Button>
        );
    }
}

export default BasicButton;
