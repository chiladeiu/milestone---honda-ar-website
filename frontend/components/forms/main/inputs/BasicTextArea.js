import React from 'react';

import './BasicTextArea.less';

import { Input } from 'antd';

const { TextArea } = Input;

class BasicTextArea extends React.Component {

    render() {
        const { fontFamily = '' } = this.props;
        return (
            <div className="basicTextArea">
                <div className="basicTextAreaBox">
                    <TextArea {...this.props} className={`input ${this.props.fontFamily}`} />
                </div>
            </div>
        );
    }
}

export default BasicTextArea;
