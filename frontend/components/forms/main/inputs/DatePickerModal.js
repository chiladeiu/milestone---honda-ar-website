import React from 'react';
import Link from 'next/link';

import './BasicModal.less';

import {
    Input, Modal, Row, Col, DatePicker
} from 'antd';
import { ROUTES, SITE_NAME } from '../../../../config';

function onChange(date, dateString) {
    console.log(date, dateString);
}

class DatePickerModal extends React.Component {

    render () {
        const { title, visible, handleOk = {}, handleCancel = {}, onChange } = this.props;
        return (
            <div className="datePickerModal">
                <div className="datePickerModalBox">
                    <Modal
                        title={'Edit ' + title}
                        visible={visible}
                        onOk={handleOk}
                        onCancel={handleCancel}
                        width='400px'
                    >
                        <Row>
                            <Col>
                                <DatePicker onChange={onChange} />
                            </Col>
                        </Row>
                    </Modal>
                </div>
            </div>
        );
    }
}

export default DatePickerModal;
