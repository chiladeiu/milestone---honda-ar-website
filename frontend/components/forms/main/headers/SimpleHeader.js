import React from 'react';
import { withRouter } from 'next/router';
import Link from 'next/link';
import _ from 'lodash';

import './SimpleHeader.less';
import { Layout } from 'antd';

import { ROUTES, SITE_NAME } from '../../../../config';

import { LANG } from '../../../../lang';
import BasicMenu from '../menus/BasicMenu';

const { Header } = Layout;

class SimpleHeader extends React.Component {
    highlightSelected () {
        const { asPath: routePath } = this.props.router;

        // Special case
        // if (_.includes(routePath, ROUTES.BLOG)) { return ROUTES.BLOG; }
        return routePath;
    }

    constructor (props) {
        super(props);

        this.state = {
        };
    }

    componentDidMount () {
    }

    componentWillUnmount () {
    }

    render () {
        const MENU = [
            { text: LANG.HeaderHome(), icon: 'mail', link: ROUTES.HOME },
            { text: LANG.HeaderProfile(), icon: 'info', link: ROUTES.PROFILE },
            { text: LANG.HeaderLogIn(), icon: 'mail', link: ROUTES.AUTH_LOGIN },
            // { text: LANG.HeaderOurStory(), icon: 'home', link: ROUTES.OUR_STORY },
            // { text: LANG.HeaderWhatWeDo(), icon: 'info', link: ROUTES.WHAT_WE_DO }
        ];

        return (
            <div className="simpleHeader">
                <div className="headerMargin" />
                <Header className="simpleHeaderBox">
                    <div className="logo">
                        <Link prefetch href={ROUTES.HOME}>
                            <a>
                                <img src={'/static/shared/logo.svg'} alt={SITE_NAME} />
                            </a>
                        </Link>
                    </div>

                    <div className="menu">
                        <BasicMenu
                            menu={MENU}
                        />
                    </div>
                </Header>
            </div>
        );
    }
}

export default withRouter(SimpleHeader);
