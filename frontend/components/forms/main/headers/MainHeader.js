import React from 'react';
import { withRouter } from 'next/router';
import Link from 'next/link';
import _ from 'lodash';

import './MainHeader.less';
import { Layout, Drawer, Button, Icon, Avatar } from 'antd';

import { ROUTES, SITE_NAME } from '../../../../config';

import { LANG, changeLanguage } from '../../../../lang';
import BasicMenu from '../menus/BasicMenu';

const { Header } = Layout;

class MainHeader extends React.Component {
    highlightSelected() {
        const { asPath: routePath } = this.props.router;

        // Special case
        // if (_.includes(routePath, ROUTES.BLOG)) { return ROUTES.BLOG; }
        return routePath;
    }

    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {}

    componentWillUnmount() {}

    render() {
        const MENU = [
            { text: LANG.HeaderHome(), icon: 'home', link: ROUTES.HOME },
            { text: LANG.HeaderLogIn(), icon: 'info', link: ROUTES.AUTH_LOGIN },
            { text: 'FOLLOWING', icon: 'mail', link: ROUTES.FOLLOWING },
            { text: 'PROFILE', icon: 'user', link: ROUTES.PROFILE },
        ];

        return (
            <div className="mainHeader">
                <div className="headerMargin" />
                <Header className="mainHeaderBox">
                    <div className="logo">
                        <Link prefetch href={ROUTES.HOME}>
                            <a>
                                <img src={'/static/shared/logo.svg'} alt={SITE_NAME} />
                            </a>
                        </Link>
                    </div>

                    <div className="menu">
                        <BasicMenu menu={MENU} />
                    </div>
                </Header>
            </div>
        );
    }
}

export default withRouter(MainHeader);
