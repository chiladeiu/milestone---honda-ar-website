import React from 'react';

import Link from 'next/link';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';

import { Layout, Menu, PageHeader, Icon, Divider } from 'antd';

import './CmsSider.less';
import { ROUTES } from '../../../config';

const { Header, Sider } = Layout;

class CmsSider extends React.Component {
    _renderMenuItem = (link, icon, text) => (
        <Menu.Item key={link}>
            <Link href={link}>
                <a>
                    <Icon type={icon} />
                    <span className="nav-text">{text}</span>
                </a>
            </Link>
        </Menu.Item>
    );

    _renderAdminSider = pathname => (
        <Menu
            theme="dark"
            mode="inline"
            selectedKeys={[pathname]}
            defaultOpenKeys={['account']}
            openKeys={['collaborator']}
        >
            {this._renderMenuItem(ROUTES.CMS_ADMINS_CONTENTS, 'video-camera', 'Quản Lý Nội Dung')}
            {this._renderMenuItem(ROUTES.CMS_ADMINS_USERS, 'user', 'Quản Lý Người Dùng')}
            {this._renderMenuItem(ROUTES.CMS_ADMINS_KOLS, 'star', 'Bình Chọn KOLS')}
            {/* {this._renderMenuItem(ROUTES.CMS_ADMINS_CONTENTS_RANKS, 'container', 'Xếp Hạng Nội Dung')}
            {this._renderMenuItem(ROUTES.CMS_ADMINS_KOLS_RANKS, 'gift', 'Xếp Hạng KOLS')} */}
        </Menu>
    );

    render() {
        const { asPath: pathname } = this.props.router;

        const { role } = this.props;

        return (
            <Sider
                className="cmsSider"
                breakpoint="lg"
                collapsible={true}
                onBreakpoint={broken => {
                    console.log(broken);
                }}
                onCollapse={(collapsed, type) => {
                    console.log(collapsed, type);
                }}
            >
                <div className="logo">
                    <Link href={ROUTES.CMS_MAIN}>
                        <a>
                            <img src="/static/shared/logo.svg" />
                        </a>
                    </Link>
                </div>
                {this._renderAdminSider(pathname)}
            </Sider>
        );
    }
}
export default withRouter(
    connect(state => {
        const { role } = state.me;
        return {
            role,
        };
    })(CmsSider),
);
