import React from 'react';

import './CmsFooter.less';

import {
    Layout, Icon, Col, Row, Divider
} from 'antd';
import { SITE_NAME } from '../../../config';

const { Footer } = Layout;

class CmsFooter extends React.Component {
    render() {
        return (
            <Footer className="cmsFooter">
                {SITE_NAME} ©{new Date().getFullYear()}
            </Footer>
        );
    }
}

export default CmsFooter;
