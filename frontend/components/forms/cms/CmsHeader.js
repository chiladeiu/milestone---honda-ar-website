import React from 'react';
import Link from 'next/link';

import { Layout, Menu, PageHeader, Icon, Divider, Button } from 'antd';
import './CmsHeader.less';
import { SITE_NAME, ROUTES } from '../../../config';
// import { logout } from '../../../lib/auth';
import { connect } from 'react-redux';

const { Header } = Layout;

class CmsHeader extends React.Component {
    render() {
        const { title, role } = this.props;

        const ROLE_NAME = {
            superadmin: 'SUPER ADMIN',
            admin: 'ADMIN',
            collaborator: 'CỘNG TÁC VIÊN',
        };

        return (
            <Header className="cmsHeader">
                <PageHeader
                    onBack={() => window.history.back()}
                    title={title}
                    extra={[
                        <>
                            <Icon type="audit" /> <span>{ROLE_NAME[role]}</span>
                        </>,
                        <Divider key="1" type="vertical" />,
                        // <a key="2" onClick={logout}><Icon type="export" /> THOÁT</a>
                    ]}
                />
            </Header>
        );
    }
}

export default connect(state => {
    const { role } = state.me;
    return {
        role,
    };
})(CmsHeader);
