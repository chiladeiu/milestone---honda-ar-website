import React from 'react';
import Link from 'next/link';
import { ROUTES } from '../../config';

export const BlogLink = ({ slug, children }) => (
    <Link as={`${ROUTES.BLOG}/${slug}`} href={`${ROUTES.BLOG_POST}?slug=${slug}`}>
        <a>{ children }</a>
    </Link>
);

export const UtilityLink = ({ id, children }) => (
    <Link as={`${ROUTES.UTILITIES}/${id}`} href={`${ROUTES.UTILITY_DETAILS}?id=${id}`}>
        <a>{ children }</a>
    </Link>
);
