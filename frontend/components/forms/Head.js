import React from 'react';
import NextHead from 'next/head';
import PropTypes from 'prop-types';
import { SITE_NAME, FB_APPID, MAIN_METAIMAGE } from '../../config';
import { getMediaURL } from '../../lib/helper';

const Head = ({ title, image, url, description }) => {
    const updatedTitle = 'Honda Wave RSX';
    return (
        <NextHead>
            <meta charSet="utf-8" />
            <link rel="shortcut icon" href="/static/main/shared/favicon.ico" />
            <title>{updatedTitle}</title>

            {/* <meta name="google-site-verification" content="" /> */}

            <meta property="og:title" content={updatedTitle} />
            <meta property="og:type" content="website" />
            <meta property="og:image" content={image} />
            <meta property="og:url" content={url} />
            <meta property="og:site_name" content={SITE_NAME} />
            <meta property="og:description" content={'Trải nghiệm xe Wave RSX cùng công nghệ thực tế ảo AR'} />
            <meta property="fb:app_id" content={FB_APPID} />
            <meta name="theme-color" content="#000000" />
            <meta httpEquiv="Content-Type" content="text/html;charset=utf-8" />
            <meta httpEquiv="content-language" content="vi" />
            <meta name="robots" content="index,follow" />

            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-147634386-1"></script>
            <script
                dangerouslySetInnerHTML={{
                    __html: `
                    window.dataLayer = window.dataLayer || [];
                    function gtag() {
                        dataLayer.push(arguments);
                    }
                    gtag('js', new Date());
                    gtag('config', 'UA-147634386-1');`,
                }}
            />
            <script
                dangerouslySetInnerHTML={{
                    __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                    })(window,document,'script','dataLayer','GTM-PHPCWK5');`,
                }}
            />
            <noscript
                dangerouslySetInnerHTML={{
                    __html: `
                    <iframe
                        src="https://www.googletagmanager.com/ns.html?id=GTM-PHPCWK5"
                        height="0"
                        width="0"
                        style="display:none;visibility:hidden"
                    ></iframe>`,
                }}
            ></noscript>
        </NextHead>
    );
};

Head.propTypes = {
    title: PropTypes.string,
    image: PropTypes.string,
    url: PropTypes.string,
    description: PropTypes.string,
};

export default Head;
