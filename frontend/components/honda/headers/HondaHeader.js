import React from 'react';
import { withRouter } from 'next/router';
import Link from 'next/link';
import _ from 'lodash';

import './HondaHeader.less';
import { Layout, Icon, Button } from 'antd';

import { ROUTES, SITE_NAME } from '../../../config';

import { LANG } from '../../../lang';
import HondaMenu from '../menus/HondaMenu';
import BasicRoundButton from '../inputs/BasicRoundButton';

const { Header } = Layout;

class HondaHeader extends React.Component {
    highlightSelected() {
        const { asPath: routePath } = this.props.router;
        return routePath;
    }

    constructor(props) {
        super(props);

        this.state = {
            scrollStyle: '',
        };
    }

    /*
    componentDidMount() {
        this.updateHeaderStyle();
        window.addEventListener('scroll', this.updateHeaderStyle, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.updateHeaderStyle);
    }

    updateHeaderStyle = () => {
        if (window.pageYOffset > 10 && this.state.scrollStyle !== 'scrollHeaderEnterStyle')
            this.setState({ scrollStyle: 'scrollHeaderEnterStyle' });
        else if (window.pageYOffset <= 10 && this.state.scrollStyle !== 'scrollHeaderExitStyle')
            this.setState({ scrollStyle: 'scrollHeaderExitStyle' });
    };
    */

    render() {
        const { refs } = this.props;

        return (
            // <div className={`hondaHeader ${this.state.scrollStyle}`}>
            <div className={`hondaHeader scrollHeaderEnterStyle`}>
                <div className="headerMargin" />
                <div className="hondaHeaderBox">
                    <div className="logo">
                        <Link prefetch href={ROUTES.HOME}>
                            <a>
                                <img src={'/static/shared/logo.svg'} alt={SITE_NAME} />
                            </a>
                        </Link>
                    </div>

                    <div className="menu">
                        <HondaMenu isAlreadyJoinedContest={this.props.isAlreadyJoinedContest} refs={refs} />
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(HondaHeader);
