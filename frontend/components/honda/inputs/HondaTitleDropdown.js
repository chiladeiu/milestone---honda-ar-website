import React from 'react';
import HondaDropdown from './HondaDropdown';
import Link from 'next/link';
import moment from 'moment';
import './HondaTitleDropdown.less';

import { Button, Icon, Menu, Dropdown } from 'antd';
import { WEEK_DATA, CURRENT_WEEK_ID, WEEK_COUNT } from '../../../config';
import { clamp } from '../../../lib/helper';

class HondaTitleDropdown extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentWeekId: 0,
        };
    }

    async componentDidMount() {
        const { onFilterChange, skipCurrentWeek = true } = this.props;
        // const weekId = CURRENT_WEEK_ID(moment().unix());
        const weekId = 5;

        this.setState(
            {
                weekData: skipCurrentWeek ? WEEK_DATA.slice(0, weekId) : WEEK_DATA,
                currentWeekId: skipCurrentWeek ? clamp(weekId - 1, 0, WEEK_COUNT) : weekId,
            },
            () => {
                const { currentWeekId } = this.state;
                if (onFilterChange) {
                    onFilterChange(currentWeekId);
                }
            },
        );
    }

    onChangeWeek(currentWeekId) {
        this.setState({ currentWeekId });
        this.props.onFilterChange(currentWeekId);
    }

    render() {
        // if (CURRENT_WEEK_ID(moment().unix()) === -1) return null;

        const { text, textExtra = '' } = this.props;
        const { currentWeekId, weekData } = this.state;

        if (!weekData) return null;

        const menu = () => (
            <Menu>
                {weekData.map((week, index) => (
                    <Menu.Item key={index}>
                        <span onClick={() => this.onChangeWeek(index)}>
                            <b>TUẦN {index + 1}:</b> {moment.unix(week.start).format('DD/MM/YYYY')} -{' '}
                            {moment.unix(week.end).format('DD/MM/YYYY')}
                        </span>
                    </Menu.Item>
                ))}
            </Menu>
        );

        return (
            <div className="hondaTitleDropdown">
                <div className="hondaTitleDropdownBox">
                    <div className="titleDropdownTitle">
                        <h3>
                            <span>{text}</span> {textExtra !== '' && <span style={{ color: '#fff' }}>{textExtra}</span>}
                        </h3>
                        <p>
                            Tuần {currentWeekId + 1}: {moment.unix(WEEK_DATA[currentWeekId].start).format('DD/MM/YYYY')}{' '}
                            - {moment.unix(WEEK_DATA[currentWeekId].end).format('DD/MM/YYYY')}
                        </p>
                    </div>
                    <div className="titleDropdownButtons">
                        <div className="titleDropdownButton">
                            <Dropdown placement="bottomRight" overlay={menu} trigger={['click']}>
                                <div className="hondaDropdownButton" href="#">
                                    <img src="/static/main/shared/icon-filters.svg" />
                                </div>
                            </Dropdown>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default HondaTitleDropdown;
