import React from 'react';

import './HondaDropdown.less';

import { Button, Icon, Menu, Dropdown } from 'antd';
import CarouselThumbnailMessenger from '../../forms/main/carousel/CarouselThumbnailMessenger';

class HondaDropdown extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentOption: 0,
        };
    }

    onClick(index) {
        console.log(index);
        const { onSelect = {} } = this.props;
        onSelect(index);
    }

    render() {
        const options = [
            '09/09/2019 - 16/09/2019',
            '16/09/2019 - 23/09/2019',
            '23/09/2019 - 30/09/2019',
            '30/09/2019 - 07/10/2019',
            '07/10/2019 - 14/10/2019',
            '14/10/2019 - 21/09/2019',
        ];

        const menu = () => (
            <Menu>
                {options.map((option, index) => (
                    <Menu.Item key={index}>
                        <span onClick={index => this.onClick(index)}>
                            <b>TUẦN {index + 1}:</b> {option}
                        </span>
                    </Menu.Item>
                ))}
            </Menu>
        );

        return (
            <div className="hondaDropdown">
                <Dropdown placement="bottomRight" overlay={menu} trigger={['click']}>
                    <div className="hondaDropdownButton" href="#">
                        <img src="/static/main/shared/icon-filters.svg" />
                    </div>
                </Dropdown>
            </div>
        );
    }
}

export default HondaDropdown;
