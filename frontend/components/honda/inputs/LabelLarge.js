import React from 'react';

import './LabelLarge.less';

import { Button, Icon } from 'antd';

class LabelLarge extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { prizeRank, name } = this.props;
        return (
            <div className="labelLarge">
                <div className="rect">
                    <p>{name}</p>
                </div>

                <div className="circle">
                    <p>
                        Giải
                        <br />
                        <b>{prizeRank}</b>
                    </p>
                </div>
            </div>
        );
    }
}

export default LabelLarge;
