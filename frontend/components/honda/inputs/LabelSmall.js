import React from 'react';

import './LabelSmall.less';

import { Button, Icon } from 'antd';

class LabelSmall extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { prizeRank, name, textExtra = '' } = this.props;
        return (
            <div className="labelSmall">
                <div className="rect">
                    <p>{name}</p>
                </div>
                {textExtra !== '' && (
                    <div className="rectExtra">
                        <p>{textExtra}</p>
                    </div>
                )}
                <div className="circle">
                    <p>
                        Giải
                        <br />
                        <b>{prizeRank}</b>
                    </p>
                </div>
            </div>
        );
    }
}

export default LabelSmall;
