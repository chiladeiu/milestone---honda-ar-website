import React from 'react';
import Link from 'next/link';

import './PrizeTextItem.less';

import { Button, Icon } from 'antd';

class PrizeTextItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { minorText = "", majorText = "" } = this.props;

        return (
            <div className="prizeTextItem">
                <div className="artItem" />
                <div className="minorText">
                    {minorText}
                </div>
                <div className="majorText">
                    {majorText}
                </div>
            </div>
        );
    }
}

export default PrizeTextItem;
