import React from 'react';
import Link from 'next/link';

import './PrizeIntroItem.less';

import { Icon, Menu, Dropdown } from 'antd';

class PrizeIntroItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { thumbnail = "" } = this.props;
        return <div style={{ backgroundImage: `url(${thumbnail})` }} className="prizeIntroItem" />;
    }
}

export default PrizeIntroItem;
