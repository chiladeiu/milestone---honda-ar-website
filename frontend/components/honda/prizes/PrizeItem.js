import React from 'react';
import Link from 'next/link';

import { getMediaURL } from '../../../lib/helper';

import './PrizeItem.less';

import { Button, Icon } from 'antd';
import PrizeTextItem from './PrizeTextItem';
import LabelLarge from '../inputs/LabelLarge';
import LabelSmall from '../inputs/LabelSmall';
import MisthyModal from '../modal/MisthyModal';
import { SITE_URL } from '../../../config';

class PrizeItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            visibleVideo: false,
        };
    }

    showVideo = () => {
        this.setState({ visibleVideo: true });
    };

    closeVideo = () => {
        this.setState({ visibleVideo: false });
    };

    render() {
        const {
            thumbnail = '',
            link = '',
            majorText = '',
            minorText = '',
            rank = {},
            voteCount = {},
            voted = false,
        } = this.props;

        return (
            <div className="prizeItem">
                <div className="prizeItemBox">
                    <div className="thumbnail">
                        <div className="thumbnailBox">
                            <img src={thumbnail} />
                        </div>
                        {/* </Link> */}
                        <div className={voted ? 'likeLabel active' : 'likeLabel'}>
                            <Icon type="like" />
                        </div>
                        <span className="voteCount">{voteCount}</span>
                        <a target="_blank" className="playButton" onClick={this.showVideo}>
                            <Icon type="play-circle" />
                        </a>
                        <img src="/static/main/shared/logo-content-honda.png" className="logoContentHonda" />
                        <img src="/static/main/shared/logo-content-rsx.png" className="logoContentRsx" />
                    </div>
                    {/* <PrizeTextItem majorText={majorText} minorText={minorText} /> */}
                    {rank === 3 ? (
                        <LabelSmall prizeRank={minorText} name={majorText} />
                    ) : (
                        <LabelLarge prizeRank={minorText} name={majorText} />
                    )}
                </div>

                <MisthyModal
                    visible={this.state.visibleVideo}
                    onOk={this.closeVideo}
                    onCancel={this.closeVideo}
                    source={link}
                />
            </div>
        );
    }
}

export default PrizeItem;
