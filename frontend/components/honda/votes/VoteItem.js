import React from 'react';
import Link from 'next/link';

import './VoteItem.less';

import { Icon, Menu, Dropdown } from 'antd';

class VoteItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { item } = this.props;
        return (
            <div className="voteItem">
                <div style={{ backgroundImage: `url(${item})` }} className="voteItemBox" />
            </div>
        );
    }
}

export default VoteItem;
