import React from 'react';

import './VerticalTab.less';

import { Button, Icon, Radio } from 'antd';

class VerticalTab extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        let { value, size, tabs, onChange } = this.props;
        return (
            <div className="verticalTab">
                <Radio.Group value={value} size={size} onChange={e => onChange(e.target.value)}>
                    {tabs &&
                        tabs.map(tab => {
                            return (
                                <Radio.Button value={tab.value}>
                                    <Icon type={tab.icon} /> <span className="text">{tab.text}</span>
                                </Radio.Button>
                            );
                        })}
                </Radio.Group>
            </div>
        );
    }
}

export default VerticalTab;
