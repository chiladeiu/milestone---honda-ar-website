import React from 'react';
import Link from 'next/link';
import _ from 'lodash';
import moment from 'moment';

import './ContentItem.less';

import { Button, Icon, Row, Col } from 'antd';
import { ROUTES } from '../../../config';
import { withRouter } from 'next/router';
import MisthyModal from '../modal/MisthyModal';
import { getMediaURL } from '../../../lib/helper';
import { FacebookSDK } from '../../../lib/wrapper/facebook';

class ContentItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            visibleVideo: false,
        };
    }

    showVideo = () => {
        this.setState({ visibleVideo: true });
    };

    closeVideo = () => {
        this.setState({ visibleVideo: false });
    };

    render() {
        const { index, voted = false, onVote, video } = this.props;
        if (_.isEmpty(video)) return null;

        return (
            <>
                <div className="contentItem">
                    <div className="contentItemBox">
                        <div className="thumbnail">
                            <div className="thumbnailBox">
                                <img
                                    src={
                                        video.thumbnail
                                            ? getMediaURL(video.thumbnail)
                                            : '/static/main/shared/thumbnail-default.png'
                                    }
                                />
                            </div>
                            {index && <div className="index">#{index}</div>}
                            <a target="_blank" className="playButton" onClick={this.showVideo}>
                                <Icon type="play-circle" />
                            </a>
                            <div
                                className={voted ? 'voteButton active' : 'voteButton'}
                                onClick={() => !_.isUndefined(onVote) && onVote()}
                            >
                                <div className="voteIcon">
                                    <Icon type="like" />
                                </div>
                                <div className="voteCount">{video.voteCount}</div>
                            </div>
                            {/* {<a className="voteButton easeIn" onClick={() => !_.isUndefined(onVote) && onVote()} />} */}
                            <img src="/static/main/shared/logo-content-honda.png" className="logoContentHonda" />
                            <img src="/static/main/shared/logo-content-rsx.png" className="logoContentRsx" />
                        </div>

                        <div className="meta">
                            <Row style={{ width: '100%' }}>
                                <Col xs={12} sm={14} md={15} lg={16} xl={17}>
                                    <p className="contentItemTitle">
                                        <Link href={`${ROUTES.CONTENT}?videoId=${video.id}`}>
                                            <a>{video.title}</a>
                                        </Link>
                                    </p>
                                    <p className="contentItemName">{video.user.name}</p>
                                </Col>
                                <Col xs={12} sm={10} md={9} lg={8} xl={7}>
                                    <p className="contentItemShare">
                                        <div className="facebookShare">
                                            <Icon style={{ color: '#fff' }} theme="filled" type="facebook" />
                                            <div
                                                className="text"
                                                onClick={() => {
                                                    let shareLink = `https://www.honda-rsx.com.vn${ROUTES.CONTENT}?videoId=${video.id}`;
                                                    // shareLink = encodeURIComponent(shareLink);
                                                    console.log(shareLink);
                                                    FacebookSDK.share({
                                                        // href: `${ROUTES.CONTENT}?videoId=${video.id}`,
                                                        href: shareLink,
                                                        hashtag: '#HondaWaveRSX',
                                                    });
                                                }}
                                            >
                                                Chia sẻ
                                            </div>
                                        </div>
                                    </p>
                                    <p className="contentItemDate">
                                        <Icon type="clock-circle" /> {moment.unix(video.createdAt).format('DD/MM/YYYY')}
                                    </p>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </div>
                <MisthyModal
                    visible={this.state.visibleVideo}
                    onOk={this.closeVideo}
                    onCancel={this.closeVideo}
                    source={getMediaURL(video.path)}
                />
            </>
        );
    }
}

export default withRouter(ContentItem);
