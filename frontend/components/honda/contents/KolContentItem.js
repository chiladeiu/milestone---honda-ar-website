import React from 'react';
import Link from 'next/link';
import { FacebookSDK } from '../../../lib/wrapper/facebook';

import './KolContentItem.less';

import DenVauModal from '../../honda/modal/DenVauModal';

import { Button, Icon } from 'antd';

class KolContentItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
        };
    }

    showVideo = () => {
        this.setState({ visible: true });
    };

    closeVideo = () => {
        this.setState({ visible: false });
    };

    render() {
        const { thumbnail = '', rank = 1, link = '', voteCount = 0, voted = false, onVote = null } = this.props;
        return (
            <div className="kolContentItem">
                <div className="kolContentItemBox">
                    <div className="thumbnail">
                        <a onClick={this.showVideo}>
                            <img src={thumbnail} />
                        </a>
                        <div className="rank">#{rank}</div>
                        <a onClick={this.showVideo} className="playButton">
                            <Icon type="play-circle" />
                        </a>
                        <div className={voted ? 'voteButton active' : 'voteButton'} onClick={() => !voted && onVote()}>
                            <div className="voteIcon">
                                <Icon type="like" />
                            </div>
                            <div className="voteCount">{voteCount}</div>
                        </div>
                    </div>
                    {/* <div className="kolContentItemShare">
                        <div className="facebookShare">
                            <Icon style={{ color: '#fff' }} theme="filled" type="facebook" />
                            <div
                                className="text"
                                onClick={() => {
                                    let shareLink = `https://www.honda-rsx.com.vn`;
                                    console.log(shareLink);
                                    FacebookSDK.share({
                                        href: shareLink,
                                        hashtag: '#HondaWaveRSX',
                                    });
                                }}
                            >
                                Chia sẻ
                            </div>
                        </div>
                    </div> */}
                </div>

                <DenVauModal
                    visible={this.state.visible}
                    onOk={this.closeVideo}
                    onCancel={this.closeVideo}
                    source={link}
                />
            </div>
        );
    }
}

export default KolContentItem;
