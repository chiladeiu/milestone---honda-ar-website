import React from 'react';
import Link from 'next/link';

import './HondaTitle.less';

import { Button, Icon } from 'antd';

class HondaTitle extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { size = 1, title = '' } = this.props;

        const sizes = 'size' + size + 'x';

        return (
            <div className={`hondaTitle ${sizes}`}>
                <div className="hondaTitleBox">
                    {size == 1 && <img src="/static/main/shared/title1x.svg" />}
                    {size == 2 && <img src="/static/main/shared/title2x.svg" />}
                    {size == 3 && <img src="/static/main/shared/title3x.svg" />}
                    <h3>{title}</h3>
                </div>
            </div>
        );
    }
}

export default HondaTitle;
