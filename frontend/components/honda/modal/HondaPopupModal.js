import React from 'react';

import { Layout, Icon, Col, Row, Divider, Drawer, Form, Input, Button, Modal } from 'antd';

import './HondaPopupModal.less';

class HondaPopupModal extends React.Component {
    render() {
        const {
            title = '',
            visible,
            onOk,
            onCancel,
            content,
            classname = '',
            confirmLoading = {}
        } = this.props;

        return (
            <div>
                <Modal
                    width={500}
                    visible={this.props.visible}
                    onOk={onOk}
                    onCancel={onCancel}
                    footer={null}
                    className={`hondaPopupModal ${classname}`}
                    title={title}
                    centered
                    confirmLoading={confirmLoading}
                >
                    {content}
                </Modal>
            </div>
        );
    }
}

export default HondaPopupModal;
