import React from 'react';

import { Layout, Icon, Col, Row, Divider, Drawer, Form, Input, Button, Modal } from 'antd';

class UpdateUserInfoModal extends React.Component {
    render() {
        const {
            form: { getFieldDecorator },
            visible,
            onOk,
            onCancel,
            loading,
        } = this.props;

        return (
            <div>
                <Modal
                    width={500}
                    visible={this.props.visible}
                    title={'CẬP NHẬT THÔNG TIN TÀI KHOẢN'}
                    onOk={onOk}
                    onCancel={onCancel}
                    footer={[
                        <Button key="submit" type="primary" loading={loading} onClick={onOk}>
                            GỬI
                        </Button>,
                    ]}
                >
                    <Form layout="vertical" hideRequiredMark>
                        <Row gutter={30}>
                            <Col span={24}>
                                <Form.Item label="Tên hiển thị">
                                    {getFieldDecorator('displayName', {
                                        rules: [{ required: true, message: 'Không được để trống' }],
                                    })(<Input style={{ width: 'calc(100% - 100px - 15px)' }} />)}
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item label="Email">
                                    {getFieldDecorator('email', {
                                        rules: [{ required: true, message: 'Không được để trống' }],
                                    })(<Input />)}
                                </Form.Item>
                            </Col>
                            <Col span={24}>
                                <Form.Item label="Số điện thoại">
                                    {getFieldDecorator('phoneNumber', {
                                        rules: [{ required: true, message: 'Không được để trống' }],
                                    })(<Input />)}
                                </Form.Item>
                            </Col>
                        </Row>
                    </Form>
                </Modal>
            </div>
        );
    }
}

export default UpdateUserInfoModal;
