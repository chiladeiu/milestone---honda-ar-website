import React from 'react';

import { Layout, Icon, Col, Row, Divider, Drawer, Form, Input, Button, Modal } from 'antd';

import './DenVauModal.less';

class DenVauModal extends React.Component {
    componentDidMount() {
        this.videoRef = React.createRef();
    }

    render() {
        const {
            thumbnail = '',
            rank = 1,
            title = '',
            visible,
            onOk,
            onCancel,
            content,
            confirmLoading = {},
            source,
            voteCount = 0,
            voted = false,
            onVote = null,
        } = this.props;

        return (
            <div>
                <Modal
                    visible={this.props.visible}
                    onOk={onOk}
                    onCancel={() => {
                        this.videoRef.pause();
                        onCancel();
                    }}
                    footer={null}
                    className="denVauModal"
                    title={title}
                    centered={true}
                    confirmLoading={confirmLoading}
                    // width={300}
                >
                    <video ref={ref => (this.videoRef = ref)} preload="auto" autoPlay controls>
                        <source src={source} type="video/mp4" />
                    </video>
                </Modal>
            </div>
        );
    }
}

export default DenVauModal;
