import React from 'react';

import { Layout, Icon, Col, Row, Divider, Drawer, Form, Input, Button, Modal } from 'antd';

import './MisthyModal.less';

class MisthyModal extends React.Component {
    constructor(props) {
        super(props);

        this.videoRef = React.createRef();
        this.modalRef = React.createRef();
    }

    componentDidMount() {}

    render() {
        const { title = '', visible, onOk, onCancel, content, confirmLoading = {}, source } = this.props;

        return (
            <div>
                <Modal
                    visible={this.props.visible}
                    onOk={onOk}
                    onCancel={() => {
                        this.videoRef.pause();
                        onCancel();
                    }}
                    footer={null}
                    wrapClassName="misthyModal"
                    title={title}
                    centered={true}
                    confirmLoading={confirmLoading}
                    bodyStyle={{ padding: '10px' }}
                    ref={ref => (this.modalRef = ref)}
                >
                    <video ref={ref => (this.videoRef = ref)} preload="auto" autoPlay controls>
                        <source onLoad={() => console.log('DONE')} src={source} type="video/mp4" />
                    </video>
                </Modal>
            </div>
        );
    }
}

export default MisthyModal;
