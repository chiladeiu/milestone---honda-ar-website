import React from 'react';
import Link from 'next/link';

import './CustomTitle.less';

import { Button, Icon } from 'antd';

class CustomTitle extends React.Component {
    constructor(props) {
        super(props);

        this.textRef = React.createRef();
        this.titleRef = React.createRef();

        this.state = {
            fontSize: 1.55,
            backgroundImage: '/static/main/shared/title1x.png',
            backgroundImageBottom: '-10px',
            backgroundImageLeft: '-40px',
        };
    }

    componentDidMount() {
        this.recomputeComponent();
        window.addEventListener('resize', this.recomputeComponent);

        this.intervalCounter = 0;

        this.updateInterval = setInterval(() => {
            const { intervalCounter } = this.state;
            this.recomputeComponent();

            if (intervalCounter === 5) {
                clearInterval(this.updateInterval);
            }

            this.setState({ intervalCounter: intervalCounter + 1 });
        }, 1000);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.recomputeComponent);
    }

    recomputeComponent = () => {
        if (this.titleRef === null || this.textRef === null) return;

        const titleRect = this.titleRef.getBoundingClientRect();
        const textRect = this.textRef.getBoundingClientRect();

        const { text } = this.props;
        const BG_IMAGES = {
            title1x: '/static/main/shared/title1x.png',
            title2x: '/static/main/shared/title2x.png',
            title3x: '/static/main/shared/title3x.png',
            title4x: '/static/main/shared/title4x.png',
        };

        let fontSize = 1.55;

        // if (titleClientWidth <= 320)
        //     fontSize = 0.8;
        // else if (titleClientWidth <= 575)
        //     fontSize = 1.0;
        // else
        //     fontSize = 1.55;

        let backgroundImage = BG_IMAGES.title1x;
        let backgroundImageBottom = 0 - 10;
        let backgroundImageLeft = 0 - 40;

        let test = '';

        if (textRect.width >= 700) {
            backgroundImage = BG_IMAGES.title4x;
            // console.log(titleRect.width + ' ' + textRect.width + ' ' + 'title4x' + ' ' + text);
        } else if (textRect.width >= 500 && textRect.width < 700) {
            backgroundImageBottom = 0 - 20;
            backgroundImageLeft = 0 - 35;
            backgroundImage = BG_IMAGES.title3x;
            // console.log(titleRect.width + ' ' + textRect.width + ' ' + 'title3x' + ' ' + text);
        } else if (textRect.width > 360 && textRect.width < 500) {
            backgroundImageBottom = 0 - 25;
            backgroundImageLeft = 0 - 40;
            backgroundImage = BG_IMAGES.title2x;
            // console.log(titleRect.width + ' ' + textRect.width + ' ' + 'title2x' + ' ' + text);
        } else if (textRect.width <= 360) {
            backgroundImageBottom = 0 - 15;
            backgroundImageLeft = 0 - 35;
            backgroundImage = BG_IMAGES.title1x;
            // console.log(titleRect.width + ' ' + textRect.width + ' ' + 'title1x' + ' ' + text);
        }

        this.setState({
            backgroundImage: backgroundImage,
            backgroundImageBottom: `${backgroundImageBottom}px`,
            backgroundImageLeft: `${backgroundImageLeft}px`,
        });
    };

    render() {
        const { text } = this.props;
        const { backgroundImage, backgroundImageBottom, backgroundImageLeft } = this.state;

        return (
            <div
                className="customTitle"
                ref={ref => {
                    this.titleRef = ref;
                }}
            >
                <div
                    className="customText"
                    ref={ref => {
                        this.textRef = ref;
                    }}
                >
                    <span>{text}</span>
                    <img src={backgroundImage} style={{ bottom: backgroundImageBottom, left: backgroundImageLeft }} />
                </div>
            </div>
        );
    }
}

export default CustomTitle;
