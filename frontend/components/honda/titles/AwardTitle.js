import React from 'react';
import Link from 'next/link';
import Media from 'react-media';

import './AwardTitle.less';

import { Button, Icon } from 'antd';

class AwardTitle extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { text } = this.props;

        return (
            <div className="awardTitle">
                <Media query="(max-width: 767.98px)">
                    {matches =>
                        matches ? (
                            <span>Bình chọn & <br/> Trúng thưởng hàng tuần</span>
                        ) : (
                            <span>Bình chọn & Trúng thưởng hàng tuần</span>
                        )
                    }
                </Media>
            </div>
        );
    }
}

export default AwardTitle;
