import React from "react";
import Link from "next/link";

import "./MediumTitle.less";

import { Button, Icon } from "antd";

class MediumTitle extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { text } = this.props;

    return (
      <div className="mediumTitle">
        <span>{text}</span>
      </div>
    );
  }
}

export default MediumTitle;
