import React from "react";
import Link from "next/link";

import "./LoginTitle.less";

import { Button, Icon } from "antd";

class LoginTitle extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { text } = this.props;

    return (
      <div className="loginTitle">
        <span>{text}</span>
      </div>
    );
  }
}

export default LoginTitle;
