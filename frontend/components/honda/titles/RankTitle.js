import React from 'react';
import Link from 'next/link';

import './RankTitle.less';

import { Button, Icon } from 'antd';

class RankTitle extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { rank } = this.props;

        return (
            <div className="rankTitle">
                {rank === 1 && <span>01 giải nhất</span>}
                {rank === 2 && <span>01 giải nhì</span>}
                {rank === 3 && <span>03 giải ba</span>}
            </div>
        );
    }
}

export default RankTitle;
