import React from 'react';
import { withRouter } from 'next/router';
import Link from 'next/link';
import _ from 'lodash';

import './HondaFooter.less';
import { Layout, Icon, Button } from 'antd';

import { ROUTES, SITE_NAME } from '../../../config';

import { LANG } from '../../../lang';
import HondaMenu from '../menus/HondaMenu';
import BasicRoundButton from '../inputs/BasicRoundButton';

const { Header } = Layout;

class HondaFooter extends React.Component {
    highlightSelected() {
        const { asPath: routePath } = this.props.router;

        // Special case
        // if (_.includes(routePath, ROUTES.BLOG)) { return ROUTES.BLOG; }
        return routePath;
    }

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
        };
    }

    componentDidMount() {}

    componentWillUnmount() {}

    render() {
        return (
            <div className="hondaFooter">
                <div className="hondaFooterBox">
                    <Icon type="copyright" style={{ fontSize: '0.9rem', marginRight: '0.4rem' }} /> Bản quyền thuộc về
                    Công ty Honda Việt Nam
                </div>
            </div>
        );
    }
}

export default withRouter(HondaFooter);
