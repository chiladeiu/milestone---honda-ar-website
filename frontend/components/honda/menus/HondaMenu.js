import React from 'react';
import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import Link from 'next/link';
import _ from 'lodash';
import classnames from 'classnames';

import './HondaMenu.less';
import { Drawer, Button, Icon, message, Avatar, Divider } from 'antd';
import { ROUTES } from '../../../config';
import { LANG } from '../../../lang';

import scrollToComponent from 'react-scroll-to-component-ssr';
import { dispatch } from 'rxjs/internal/observable/range';
import { bindMeActions } from '../../../redux/actions/me';

class HondaMenu extends React.Component {
    getRoutePath() {
        const { asPath: routePath } = this.props.router;
        return routePath;
    }

    constructor(props) {
        super(props);

        this.state = {
            isMobileMenuDrawerVisible: false,
            scrollStyle: '',
        };
    }

    componentDidMount() {
        this.hideMobileMenuDrawer();
        // this.checkOnFirstRender();
        window.addEventListener('resize', this.hideMobileMenuDrawer);

        // window.addEventListener('scroll', this.updateMenuStyle, true);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.hideMobileMenuDrawer);

        // window.removeEventListener('scroll', this.updateMenuStyle);
    }

    // checkOnFirstRender = () => {
    //     if (window.pageYOffset > 10 && this.state.scrollStyle !== 'scrollMenuEnterStyle')
    //         this.setState({ scrollStyle: 'scrollMenuEnterStyle' });
    //     else if (window.pageYOffset <= 10 && this.state.scrollStyle !== 'scrollMenuExitStyle')
    //         this.setState({ scrollStyle: 'scrollMenuExitStyle' });
    // };

    // updateMenuStyle = () => {
    //     if (window.pageYOffset > 10 && this.state.scrollStyle !== 'scrollMenuEnterStyle')
    //         this.setState({ scrollStyle: 'scrollMenuEnterStyle' });
    //     else if (window.pageYOffset <= 10 && this.state.scrollStyle !== 'scrollMenuExitStyle')
    //         this.setState({ scrollStyle: 'scrollMenuExitStyle' });
    // };

    showMobileMenuDrawer = () => {
        this.setState({
            isMobileMenuDrawerVisible: true,
        });
    };

    hideMobileMenuDrawer = () => {
        this.setState({
            isMobileMenuDrawerVisible: false,
        });
    };

    onClickJoinNowButton = () => {
        const { menu, refs } = this.props;
        if (this.props.isAlreadyJoinedContest) {
            message.warning('Bạn đã tham gia tuần này.');
        } else {
            this.props.router.pathname === '/' && scrollToComponent(refs.loginRef, { offset: 0, align: 'top' });
        }
    };

    renderDesktopMenu = () => {
        const { menu, refs, me, meActions } = this.props;

        return (
            <div className="desktopMenu">
                {me.isLoggedIn && (
                    <div className="desktopProfile">
                        <div className="profileUsername">
                            {me.role === 'admin' ? (
                                <>
                                    <Avatar size="small" icon="user" /> Hi, ADMIN!{' | '}
                                    <span style={{ cursor: 'pointer' }} onClick={meActions.logout}>
                                        Đăng xuất
                                    </span>
                                </>
                            ) : (
                                <>
                                    {me.isLoggedIn && (
                                        <>
                                            <Avatar size="small" icon="user" /> Hi, {me.data.facebookFirstName}!{' | '}
                                            <span style={{ cursor: 'pointer' }} onClick={meActions.logout}>
                                                Đăng xuất
                                            </span>
                                        </>
                                    )}
                                </>
                            )}
                        </div>
                    </div>
                )}
                <div className={classnames('item', { active: ROUTES.HOME === this.getRoutePath() })}>
                    <Link href={ROUTES.HOME}>
                        <a className="line">
                            <span>Trang chủ</span>
                        </a>
                    </Link>
                </div>

                {this.props.router.pathname === '/' ? (
                    <>
                        <div
                            onClick={() => scrollToComponent(refs.prizesRef, { offset: 0 })}
                            className={classnames('item', {})}
                        >
                            <a>
                                <span>Giải thưởng</span>
                            </a>
                        </div>
                        <div
                            onClick={() => scrollToComponent(refs.processRef, { align: 'top' })}
                            className={classnames('item', {})}
                        >
                            <a>
                                <span>Cuộc thi sáng tạo video</span>
                            </a>
                        </div>
                    </>
                ) : (
                    <>
                        <div className={classnames('item', { active: ROUTES.HOME === this.getRoutePath() })}>
                            <Link href={ROUTES.HOME}>
                                <a className="line">
                                    <span>Giải thưởng</span>
                                </a>
                            </Link>
                        </div>
                        <div className={classnames('item', { active: ROUTES.HOME === this.getRoutePath() })}>
                            <Link href={ROUTES.HOME}>
                                <a className="line">
                                    <span>Cuộc thi sáng tạo video</span>
                                </a>
                            </Link>
                        </div>
                    </>
                )}

                <div className={classnames('item', { active: ROUTES.CONTENTS === this.getRoutePath() })}>
                    <Link href={ROUTES.CONTENTS}>
                        <a className="line">
                            <span>Video dự thi</span>
                        </a>
                    </Link>
                </div>

                <div className={classnames('item', { active: ROUTES.TERMS === this.getRoutePath() })}>
                    <Link href={ROUTES.TERMS}>
                        <a className="line">
                            <span>Thể lệ</span>
                        </a>
                    </Link>
                </div>

                {this.props.router.pathname === '/' && (
                    <div onClick={this.onClickJoinNowButton} className={classnames('item', {})}>
                        <a>
                            <img
                                className={this.props.isAlreadyJoinedContest ? 'grayscale' : 'easeIn'}
                                width="200px"
                                src="/static/main/shared/button-tham-gia-ngay.svg"
                            />
                        </a>
                    </div>
                )}
            </div>
        );
    };

    renderMobileMenu = () => {
        const { menu, refs, me, meActions } = this.props;

        return (
            <>
                <div className="headerButtons">
                    {me.isLoggedIn && (
                        <div className="mobileProfile">
                            <div className="profileUsername">
                                {me.role === 'admin' ? (
                                    <>
                                        <Avatar size="small" icon="user" /> Hi, ADMIN!
                                    </>
                                ) : (
                                    <>
                                        {me.data.facebookFirstName && (
                                            <>
                                                <Avatar size="small" icon="user" /> Hi, {me.data.facebookFirstName}!
                                            </>
                                        )}
                                    </>
                                )}
                            </div>
                        </div>
                    )}
                    <div className="joinNowButton">
                        {this.props.router.pathname === '/' && (
                            <div onClick={this.onClickJoinNowButton} className={classnames('item', {})}>
                                <a>
                                    <img
                                        className={this.props.isAlreadyJoinedContest ? 'grayscale' : 'easeIn'}
                                        src="/static/main/shared/button-tham-gia-ngay.svg"
                                    />
                                </a>
                            </div>
                        )}
                    </div>

                    <button onClick={this.showMobileMenuDrawer} className="mobileMenuButton">
                        <Icon className="mobileMenuButtonIcon" type="menu-fold" />
                    </button>
                </div>

                <Drawer
                    className="mobileMenuDrawer"
                    placement="right"
                    closable={true}
                    onClose={this.hideMobileMenuDrawer}
                    visible={this.state.isMobileMenuDrawerVisible}
                    width="300"
                >
                    <div className="mobileMenu">
                        <div className={classnames('item', { active: ROUTES.HOME === this.getRoutePath() })}>
                            <Link href={ROUTES.HOME}>
                                <a className="line">
                                    <span>Trang chủ</span>
                                </a>
                            </Link>
                        </div>

                        {this.props.router.pathname === '/' ? (
                            <>
                                <div
                                    onClick={() => {
                                        this.hideMobileMenuDrawer();
                                        scrollToComponent(refs.prizesRef, { offset: 0, align: 'top' });
                                    }}
                                    className={classnames('item', {})}
                                >
                                    <a>
                                        <span>Giải thưởng</span>
                                    </a>
                                </div>
                                <div
                                    onClick={() => {
                                        this.hideMobileMenuDrawer();
                                        scrollToComponent(refs.processRef, { offset: 0, align: 'top' });
                                    }}
                                    className={classnames('item', {})}
                                >
                                    <a>
                                        <span>Cuộc thi sáng tạo video</span>
                                    </a>
                                </div>
                            </>
                        ) : (
                            <>
                                <div className={classnames('item', { active: ROUTES.HOME === this.getRoutePath() })}>
                                    <Link href={ROUTES.HOME}>
                                        <a className="line">
                                            <span>Giải thưởng</span>
                                        </a>
                                    </Link>
                                </div>
                                <div className={classnames('item', { active: ROUTES.HOME === this.getRoutePath() })}>
                                    <Link href={ROUTES.HOME}>
                                        <a className="line">
                                            <span>Cuộc thi sáng tạo video</span>
                                        </a>
                                    </Link>
                                </div>
                            </>
                        )}

                        <div className={classnames('item', { active: ROUTES.CONTENTS === this.getRoutePath() })}>
                            <Link href={ROUTES.CONTENTS}>
                                <a className="line">
                                    <span>Video dự thi</span>
                                </a>
                            </Link>
                        </div>

                        <div className={classnames('item', { active: ROUTES.TERMS === this.getRoutePath() })}>
                            <Link href={ROUTES.TERMS}>
                                <a className="line">
                                    <span>Thể lệ</span>
                                </a>
                            </Link>
                        </div>

                        {me.isLoggedIn && (
                            <div onClick={meActions.logout} className={classnames('item', {})}>
                                <a>
                                    <span>Đăng xuất</span>
                                </a>
                            </div>
                        )}
                    </div>
                </Drawer>
            </>
        );
    };

    render() {
        return (
            // <div className={`hondaMenu ${this.state.scrollStyle}`}>
            <div className={`hondaMenu scrollMenuEnterStyle`}>
                <div className="hondaMenuBox">
                    {this.renderDesktopMenu()}
                    {this.renderMobileMenu()}
                </div>
            </div>
        );
    }
}

export default withRouter(
    connect(
        state => ({
            me: state.me,
        }),
        dispatch => bindMeActions({}, dispatch),
    )(HondaMenu),
);
