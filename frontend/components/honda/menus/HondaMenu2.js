import React from 'react';
import { withRouter } from 'next/router';
import Link from 'next/link';
import _ from 'lodash';
import classnames from 'classnames';

import './HondaMenu2.less';
import { Drawer, Button, Icon, Row, Col } from 'antd';
import { ROUTES } from '../../../config';
import { LANG } from '../../../lang';

import scrollToComponent from 'react-scroll-to-component-ssr';

class HondaMenu2 extends React.Component {
    getRoutePath() {
        const { asPath: routePath } = this.props.router;
        return routePath;
    }

    constructor(props) {
        super(props);

        this.state = {
            isMobileMenuDrawerVisible: false,
            scrollStyle: '',
        };
    }

    componentDidMount() {
        this.hideMobileMenuDrawer();
        this.checkOnFirstRender();
        window.addEventListener('resize', this.hideMobileMenuDrawer);

        window.addEventListener('scroll', this.updateMenuStyle, true);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.hideMobileMenuDrawer);

        window.removeEventListener('scroll', this.updateMenuStyle);
    }

    checkOnFirstRender = () => {
        if (window.pageYOffset > 10 && this.state.scrollStyle !== 'scrollMenuEnterStyle')
            this.setState({ scrollStyle: 'scrollMenuEnterStyle' });
        else if (window.pageYOffset <= 10 && this.state.scrollStyle !== 'scrollMenuExitStyle')
            this.setState({ scrollStyle: 'scrollMenuExitStyle' });
    };

    updateMenuStyle = () => {
        if (window.pageYOffset > 10 && this.state.scrollStyle !== 'scrollMenuEnterStyle')
            this.setState({ scrollStyle: 'scrollMenuEnterStyle' });
        else if (window.pageYOffset <= 10 && this.state.scrollStyle !== 'scrollMenuExitStyle')
            this.setState({ scrollStyle: 'scrollMenuExitStyle' });
    };

    showMobileMenuDrawer = () => {
        this.setState({
            isMobileMenuDrawerVisible: true,
        });
    };

    hideMobileMenuDrawer = () => {
        this.setState({
            isMobileMenuDrawerVisible: false,
        });
    };

    renderDesktopMenu = () => {
        const { menu, refs } = this.props;

        return (
            <div className="desktopMenu">
                <Row>
                    <Col xxl={18} xl={18} lg={18} md={18} sm={12} xs={18}>
                        <div className={classnames('item', { active: ROUTES.HOME === this.getRoutePath() })}>
                            <Link href={ROUTES.HOME}>
                                <a className="line">
                                    <span>Trang chủ</span>
                                </a>
                            </Link>
                        </div>

                        {this.props.router.pathname === '/' && (
                            <>
                                <div
                                    onClick={() => scrollToComponent(refs.prizesRef, { offset: 0 })}
                                    className={classnames('item', {})}
                                >
                                    <a>
                                        <span>Giải thưởng</span>
                                    </a>
                                </div>
                                <div
                                    onClick={() => scrollToComponent(refs.processRef, { align: 'top' })}
                                    className={classnames('item', {})}
                                >
                                    <a>
                                        <span>Cuộc thi sáng tạo video</span>
                                    </a>
                                </div>
                            </>
                        )}

                        <div className={classnames('item', { active: ROUTES.CONTENTS === this.getRoutePath() })}>
                            <Link href={ROUTES.CONTENTS}>
                                <a className="line">
                                    <span>Video dự thi</span>
                                </a>
                            </Link>
                        </div>

                        <div className={classnames('item', { active: ROUTES.TERMS === this.getRoutePath() })}>
                            <Link href={ROUTES.TERMS}>
                                <a className="line">
                                    <span>Thể lệ</span>
                                </a>
                            </Link>
                        </div>
                    </Col>
                    <Col xxl={6} xl={6} lg={6} md={6} sm={12} xs={6}>
                        <div
                            onClick={() => scrollToComponent(refs.loginRef, { offset: 0, align: 'top' })}
                            className={classnames('item', {})}
                        >
                            <a>
                                <img src="/static/main/shared/button-tham-gia-ngay.svg" />
                            </a>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    };

    renderMobileMenu = () => {
        const { menu, refs } = this.props;

        return (
            <>
                <button onClick={this.showMobileMenuDrawer} className="mobileMenuButton">
                    <Icon className="mobileMenuButtonIcon" type="menu-fold" />
                </button>

                <Drawer
                    className="mobileMenuDrawer"
                    placement="right"
                    closable={true}
                    onClose={this.hideMobileMenuDrawer}
                    visible={this.state.isMobileMenuDrawerVisible}
                    width="300"
                >
                    <div className="mobileMenu">
                        <div className={classnames('item', { active: ROUTES.HOME === this.getRoutePath() })}>
                            <Link href={ROUTES.HOME}>
                                <a className="line">
                                    <span>Trang chủ</span>
                                </a>
                            </Link>
                        </div>

                        {this.props.router.pathname === '/' && (
                            <>
                                <div
                                    onClick={() => {
                                        this.hideMobileMenuDrawer();
                                        scrollToComponent(refs.prizesRef, { offset: 0, align: 'top' });
                                    }}
                                    className={classnames('item', {})}
                                >
                                    <a>
                                        <span>Giải thưởng</span>
                                    </a>
                                </div>
                                <div
                                    onClick={() => {
                                        this.hideMobileMenuDrawer();
                                        scrollToComponent(refs.processRef, { offset: 0, align: 'top' });
                                    }}
                                    className={classnames('item', {})}
                                >
                                    <a>
                                        <span>Cuộc thi sáng tạo video</span>
                                    </a>
                                </div>
                            </>
                        )}

                        <div className={classnames('item', { active: ROUTES.CONTENTS === this.getRoutePath() })}>
                            <Link href={ROUTES.CONTENTS}>
                                <a className="line">
                                    <span>Video dự thi</span>
                                </a>
                            </Link>
                        </div>

                        <div className={classnames('item', { active: ROUTES.TERMS === this.getRoutePath() })}>
                            <Link href={ROUTES.TERMS}>
                                <a className="line">
                                    <span>Thể lệ</span>
                                </a>
                            </Link>
                        </div>
                    </div>
                </Drawer>
            </>
        );
    };

    render() {
        return (
            <div className={`hondaMenu2 ${this.state.scrollStyle}`}>
                <div className="hondaMenu2Box">
                    {this.renderDesktopMenu()}
                    {this.renderMobileMenu()}
                </div>
            </div>
        );
    }
}

export default withRouter(HondaMenu2);
