import React from 'react';
import Link from 'next/link';

import './ProcessItemReverse.less';

import { Icon, Menu, Dropdown } from 'antd';

class ProcessItemReverse extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <div className="processItem">
                <div className="processItemBox">
                    <div className="text" />
                    <div className="image">
                        <div className="image">
                            <img src="../../static/background-2.jpg" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default ProcessItemReverse;
