import React from 'react';
import Link from 'next/link';

import './ProcessItem.less';

import { Icon, Menu, Dropdown } from 'antd';

class ProcessItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <div className="processItem">
                <div className="processItemBox">
                    <div className="image">
                        <img src="../../static/background-2.jpg" /> 
                    </div>

                    <div className="text" />
                </div>
            </div>
        );
    }
}

export default ProcessItem;
