module.exports = {
    env: {
        browser: true,
        es6: true,
        node: true,
    },
    extends: 'standard',
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parser: 'babel-eslint',
    parserOptions: {
        ecmaFeatures: {
            jsx: true,
            legacyDecorators: true,
        },
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: ['react'],
    rules: {
        // Indent with 4 spaces
        indent: ['error', 4],
        // Indent JSX with 4 spaces
        'react/jsx-indent': ['error', 4],
        // Indent props with 4 spaces
        'react/jsx-indent-props': ['error', 4],
        semi: [2, 'always'],
        'no-unused-vars': 'off',
        quotes: ['error', 'single', { allowTemplateLiterals: true }],
        'prefer-promise-reject-errors': 'off',
        'space-before-function-paren': 'off',
        'comma-dangle': 'off',
    },
};
