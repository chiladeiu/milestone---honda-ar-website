import React from 'react';
import { connect } from 'react-redux';
import { bindMeActions } from '../redux/actions/me';
import { channingActions } from '../lib/helper';
import { Button, Spin, Modal, Progress } from 'antd';
import { ZaloSDK } from '../lib/wrapper/zalo';
import { withRouter } from 'next/router';
import { ROUTES } from '../config';

class ZLCB extends React.Component {
    static async getInitialProps(ctx) {
        const { uid, code } = ctx.query;
        return { uid, code };
    }

    constructor(props) {
        super(props);

        this.state = {
            progress: 0,
        };
    }

    async componentDidMount() {
        if (!process.browser) return;
        const { meActions, me, router, code } = this.props;
        this.setState({ progress: 10 });

        try {
            const data = await meActions.loginWithZalo(code);
            this.setState({ progress: 60 });

            console.log('ZL login', data);
            await meActions.loginFirebase();
            this.setState({ progress: 100 });
        } catch (error) {
            console.log('[Index]', error);
            Modal.error({ title: 'Login Error', content: error });
        }

        router.replace(ROUTES.HOME);
    }

    render() {
        const { me, meActions } = this.props;
        const { progress } = this.state;

        return (
            <div
                style={{
                    // position: 'absolute',
                    // top: 0,
                    // left: 0,
                    height: '100vh',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    justifyItems: 'center',
                }}
            >
                <Progress type="circle" percent={progress} />
            </div>
        );
    }
}

export default withRouter(
    connect(
        state => ({
            me: state.me,
        }),
        dispatch => channingActions({}, dispatch, bindMeActions),
    )(ZLCB),
);
