import React from 'react';
import { connect } from 'react-redux';
import { channingActions, getMediaURL, serverRedirect, loadScriptAsync } from '../lib/helper';
import { Button, Icon, Row, Col, Carousel, Input, Modal, Tabs, Radio, Affix, Spin, Tag, message } from 'antd';
import Link from 'next/link';
import { withRouter, Router } from 'next/router';
import _ from 'lodash';
import moment, { duration } from 'moment';

import { bindMeActions } from '../redux/actions/me';
import './content.less';

import MainLayout from '../layout/MainLayout';
import Head from '../components/forms/Head';
import BasicSearch from '../components/forms/main/inputs/BasicSearch';
import BasicTab from '../components/forms/main/selects/BasicTab';
import PrizeItem from '../components/honda/prizes/PrizeItem';
import VoteItem from '../components/honda/votes/VoteItem';
import ProcessItem from '../components/honda/process/ProcessItem';
import ProcessItemReverse from '../components/honda/process/ProcessItemReverse';
import SimpleFooter from '../components/forms/main/footers/SimpleFooter';

import { SITE_NAME, SITE_URL, ROUTES, MAIN_METAIMAGE, CURRENT_WEEK_ID } from '../config';

import ContentItem from '../components/honda/contents/ContentItem';
import HondaTitle from '../components/honda/contents/HondaTitle';
import CustomTitle from '../components/honda/titles/CustomTitle';
import { bindUserVideosActions } from '../redux/actions/user-videos';
import { FacebookSDK } from '../lib/wrapper/facebook';
// import { ZaloSDK } from '../lib/wrapper/zalo';
import { UserVideosAPI } from '../api/user-videos';
import HondaPopupModal from '../components/honda/modal/HondaPopupModal';

const { TextArea, Search } = Input;
const { TabPane } = Tabs;

class Content extends React.Component {
    static async getInitialProps(ctx) {
        const { videoId } = ctx.query;
        const originalUrl = `${SITE_URL}content?videoId=${videoId}`;

        try {
            const videoData = (await UserVideosAPI.fetchOne(videoId)).data.payload;

            return {
                videoId,
                videoData,
                originalUrl,
            };
        } catch (err) {
            if (process.browser) Router.push(ROUTES.HOME);
            else serverRedirect(ctx, ROUTES.HOME);
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            selectedTab: 'feed',
            video: {},
            voteCount: 0,
            voted: [],
            visibleLoginModal: false,
            locking: false,
            loggedIn: false,
        };

        this.onLoginFacebook = this.onLoginFacebook.bind(this);
    }

    async onLoginFacebook() {
        const { meActions } = this.props;

        try {
            const data = await meActions.loginWithFacebook();
            // console.log('FB login', data);

            this.setState({ locking: true });

            await meActions.loginFirebase();
            this.setState({ locking: false, visibleLoginModal: false });
        } catch (error) {
            console.log('[Index]', error);
            this.setState({ locking: false, visibleLoginModal: false });
        }
    }

    async componentDidMount() {
        const { userVideosActions, videoId, router, videoData } = this.props;

        if (process.browser) {
            router.push(router.asPath, router.asPath, {
                shallow: true,
            });
        }

        console.log('Video id:', this.props.videoId);
        console.log('Current url', window.location.href);
        // eslint-disable-next-line handle-callback-err
        try {
            await userVideosActions.fetchOne(videoId);
            console.log('videoData.voteCount', videoData.voteCount);
            this.setState({
                video: videoData,
                voteCount: videoData.voteCount,
                voted: videoData.voted,
            });
            FacebookSDK.parseXFBML();
            // await loadScriptAsync('https://sp.zalo.me/plugins/sdk.js'); // Reload script to trigger share button
        } catch (error) {
            router.push(ROUTES.HOME);
        }

        message.config({
            maxCount: 1,
        });
    }

    showLoginModal = () => {
        this.setState({
            visibleLoginModal: true,
        });
    };

    closeLoginModal = () => {
        this.setState({
            visibleLoginModal: false,
        });
    };

    componentDidUpdate(prevProps, prevState) {
        if (this.state.voteCount !== this.props.video.voteCount) {
            this.setState({ voteCount: this.props.video.voteCount, voted: this.props.video.voted });
        }
    }

    handleUserVote = () => {
        const { userVideosActions, videoId, me, video } = this.props;

        if (me.isLoggedIn === false) {
            message.info('Bạn chưa đăng nhập');
            return;
        }

        if (_.includes(video.voted, me.uid)) {
            // TODO: hiện đã vote
            message.info('Bạn đã bình chọn');
            return;
        }

        if (CURRENT_WEEK_ID(moment().unix()) !== CURRENT_WEEK_ID(video.createdAt)) {
            // TODO: hiện đã vote
            message.info('Video đã quá hạn bình chọn');
            return;
        }

        userVideosActions.voteVideo(videoId).then(async () => {
            await userVideosActions.fetchOne(videoId);
        });
    };

    render() {
        const { me, originalUrl } = this.props;
        const { isFirstLoad, voted } = this.state;

        const video = process.browser ? this.props.video : this.props.videoData;

        return (
            <div style={{ overflowX: 'hidden' }}>
                <Head
                    title="Honda Wave RSX"
                    image={getMediaURL(video.thumbnail)}
                    url={originalUrl}
                    description="Trải nghiệm xe Wave RSX cùng công nghệ thực tế ảo AR"
                />
                {/* <Head title="RSX" image={getMediaURL(video.thumbnail)} url={originalUrl} description="RSX" /> */}
                <MainLayout>
                    <div className="content">
                        <div className="contentBox">
                            <div className="title">
                                {!_.isEmpty(video) && (
                                    <CustomTitle
                                        text={_.isEqual(me.data.id, video.user.id) ? 'Video của bạn ' : 'Xem video'}
                                    />
                                )}
                            </div>
                            <div className="videoBox">
                                <ContentItem
                                    voted={me.isLoggedIn && _.includes(voted, me.uid)}
                                    onVote={me.isLoggedIn ? () => this.handleUserVote() : () => this.showLoginModal()}
                                    video={video}
                                />
                                <div className="shareButton">
                                    {/* <div
                                        className="fb-share-button"
                                        data-href={window.location.href}
                                        data-layout="button"
                                        data-size="large"
                                    /> */}

                                    {/* <div className="facebookShare">
                                        <Icon style={{ color: '#fff' }} theme="filled" type="facebook" />
                                        <div
                                            className="text"
                                            onClick={() =>
                                                FacebookSDK.share({ href: originalUrl, hashtag: '#HondaWaveRSX' })
                                            }
                                        >
                                            Chia sẻ
                                        </div>
                                    </div> */}

                                    {/* <div
                                        dangerouslySetInnerHTML={{
                                            __html: `<div
                                                        class="zalo-share-button"
                                                        data-href={window.location.href}
                                                        data-oaid="579745863508352884"
                                                        data-layout="1"
                                                        data-color="blue"
                                                        data-customize="false"
                                                    ></div>`,
                                        }}
                                    /> */}
                                </div>
                                {/* <div className="loginBox">
                                    Đăng nhập
                                </div> */}
                            </div>
                        </div>
                    </div>
                </MainLayout>

                {this.state.locking && (
                    <div className="locking">
                        <p>
                            Đang đăng nhập...
                            <br />
                            <Icon type="loading" />
                        </p>
                    </div>
                )}

                <HondaPopupModal
                    visible={this.state.visibleLoginModal}
                    onOk={this.closeLoginModal}
                    onCancel={this.closeLoginModal}
                    title="Đăng nhập"
                    classname="loginBox"
                    content={
                        <>
                            <Row type="flex" justify="center">
                                <Col span={24}>
                                    <p>Bạn phải đăng nhập trước khi bình chọn.</p>
                                </Col>
                                <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
                                    <div className="loginButton facebook" onClick={this.onLoginFacebook}>
                                        <div className="icon">
                                            <img src="/static/main/shared/fb-icon.png" />
                                        </div>
                                        <div className="text">
                                            Đăng nhập bằng <b>Facebook</b>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </>
                    }
                />
            </div>
        );
    }
}

export default withRouter(
    connect(
        state => ({
            video: state.userVideos.current,
            me: state.me,
        }),
        dispatch => channingActions({}, dispatch, bindUserVideosActions, bindMeActions),
    )(Content),
);
