import React from 'react';
import { connect } from 'react-redux';
import { channingActions, getMediaURL } from '../lib/helper';
import { Button, Icon, Row, Col, Carousel, Input, Modal, Tabs, Radio, Affix } from 'antd';
import Link from 'next/link';

import './terms.less';

import MainLayout from '../layout/MainLayout';
import Head from '../components/forms/Head';
import BasicSearch from '../components/forms/main/inputs/BasicSearch';
import BasicTab from '../components/forms/main/selects/BasicTab';
import PrizeItem from '../components/honda/prizes/PrizeItem';
import VoteItem from '../components/honda/votes/VoteItem';
import ProcessItem from '../components/honda/process/ProcessItem';
import ProcessItemReverse from '../components/honda/process/ProcessItemReverse';
import SimpleFooter from '../components/forms/main/footers/SimpleFooter';

import { SITE_NAME, SITE_URL, ROUTES, MAIN_METAIMAGE } from '../config';

import ContentItem from '../components/honda/contents/ContentItem';
import CustomTitle from '../components/honda/titles/CustomTitle';

const { TextArea, Search } = Input;
const { TabPane } = Tabs;

class Terms extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTab: 'feed',
    };
  }

  componentDidMount() { }

  render() {
    const { bundles } = this.props;

    return (
      <div style={{ overflowX: 'hidden' }}>
        <Head
                    title="Honda Wave RSX"
                    image={MAIN_METAIMAGE}
                    url={SITE_URL}
                    description="Trải nghiệm xe RSX cùng công nghệ thực tế ảo AR"
                />
        <MainLayout>
          <div className="terms">
            <div className="termsBox">
              <Row className="title">
                <CustomTitle text="Điều lệ cuộc thi" />
              </Row>
              <Row className="content">
                <Col>
                  <div>
        <p><b><span><span>1. GIỚI THIỆU CHUNG:</span></span></b></p>
        <p><span>Honda Wave RSX mới với thiết kế trẻ trung và cá tính hơn, bổ sung nhiều tiện ích sẽ không chỉ là phương tiện đi lại, mà còn là một người bạn đồng hành cùng cá tính của bạn. Chất riêng của bạn là gì? Hãy để Honda Wave RSX giúp bạn thể hiện chất riêng trọn vẹn nhất.</span></p>
        <p><span>Từ ngày 10/9/2019 - 22/10/2019, Honda Việt Nam hân hạnh mang đến cho bạn cuộc thi </span><b><span>Sáng tạo video </span></b><b><span>cùng xe Honda Wave RSX</span></b><b><span> - </span></b><b><span>Rực</span></b><b><span> Sức Trẻ, Xứng Đam Mê.</span></b></p>
        <p><span>Cách tham gia rất đơn giản, truy cập website https://www.honda-rsx.com.vn, sáng tạo video của bạn với ứng dụng Camera tích hợp công nghệ AR (Thực tế ảo tương tác) đăng tải trên website và kêu gọi bạn bè cùng tham gia bình chọn để đạt các giải thưởng giá trị.</span></p>
        <p><b><span><span>2. ĐỐI TƯỢNG THAM GIA:</span></span></b></p>
        <p><span>Tất cả các công dân Việt Nam từ 18 tuổi (tính đến ngày đăng ký dự thi) trở lên và đang cư trú trên phạm vi lãnh thổ Việt Nam.</span></p>
        <p><span>Nhân viên công ty Honda Việt Nam, nhân viên HEAD và thành viên Ban Tổ Chức không được tham gia.</span></p>
        <p><b><span><span>3. CÁCH THỨC THAM GIA:</span></span></b></p>
        <p><span>Cuộc thi gồm 02 hạng mục:</span></p>
        <p><b><span><span>A. SÁNG TẠO VIDEO CÙNG HONDA</span></span></b></p>
        <p><b><span>Bước 1:</span></b><span> Truy cập website https://www.honda-rsx.com.vn</span></p>
        <p><b><span>Bước 2:</span></b><span> Đăng ký tài khoản và đăng nhập.</span></p>
        <p><b><span>Bước 3:</span></b><span> Sử dụng tính năng Camera AR (thực tế ảo tương tác) tích hợp trên website để lựa chọn mô hình xe Honda Wave RSX theo màu xe tùy thích, tự do sáng tạo video có sự tham gia của người dự thi và mô hình xe Honda Wave RSX.</span></p>
        <p><b><span>Bước 4:</span></b><span> </span><span><span>Tải</span></span><span><span> video lên website https://www.honda-rsx.com.vn,</span></span><span> </span><span><span>sau đó chia sẻ (share) link</span></span><span><span> bài dự thi từ </span></span><span><span>website về Facebook</span></span><span><span> cá nhân</span></span><span> ở chế độ công khai kèm hashtag #HondaWaveRSX #RucsuctreXungdamme, #RiSingXpression và kêu gọi bình chọn trên website của cuộc thi.</span></p>
        <p><i><span>Lưu ý, yêu cầu thiết bị hỗ trợ tương thích AR: iOS phiên bản 12.0 trở lên, Android phiên bản 7.0 trở lên.</span></i></p>
        <p><b><span><span>b. BÌNH CHỌN VIDEO "BẠN YÊU THÍCH PHONG CÁCH NÀO NHẤT" </span></span></b></p>
        <p><b><span>Bước 1:</span></b><span> Truy cập website https://www.honda-rsx.com.vn</span></p>
        <p><b><span>Bước 2:</span></b><span> Đăng ký tài khoản và đăng đăng nhập.</span></p>
        <p><b><span>Bước 3:</span></b><span> Xem và bình chọn cho video phong cách Rực Sức Trẻ - Xứng Đam Mê từ những người nổi tiếng.</span></p>
        <p><b><span>Bước 4:</span></b><span> Chờ công bố kết quả hàng tuần. 04 người bình chọn may mắn sẽ nhận được phần quà từ Ban Tổ Chức bằng hình thức bốc thăm may mắn.</span></p>
        <p><b><span><span>4</span></span></b><b><span><span>. CƠ</span></span></b><b><span><span> CẤU GIẢI THƯỞNG</span></span></b><b><span><span>:</span></span></b></p>
        <p><b><span><span>A. SÁNG TẠO VIDEO</span></span></b></p>
        <p><b><span>Giải</span></b><b><span> thưởng Tuần:</span></b></p>
        <p><span>Thời gian diễn ra cuộc thi là 06 tuần.<b><span> </span></b>Kết thúc mỗi tuần, 05 video có lượt bình chọn cao nhất từ cao xuống thấp sẽ nhận được giải thưởng của chương trình gồm:</span></p>
        <ul>
          <li><span>01 giải Nhất: 01 máy ảnh Sony DSC - HX90V</span></li>
          <li><span>01 giải Nhì: 01 tai nghe Bluetooth Sony </span><span><span>WI-SP500</span></span></li>
          <li><span>03 giải Ba: mỗi giải 01 thẻ cào điện thoại trị giá 500.000 VNĐ</span></li>
        </ul>
        <p><i><span>Trong trường hợp 02 video có cùng số lượt bình chọn thì video dự thi sớm hơn sẽ được ưu tiên (tính theo giờ đăng tải thành công trên website).</span></i></p>
        <p><b><span>Giải Chung cuộc:</span></b></p>
        <p><span>Thời gian diễn ra cuộc thi là 06 tuần, </span><span><span>Top </span></span><span><span>10</span></span><span><span> mỗi tuần</span></span><span><span> (tổng cộng 60 người)</span></span><span> sẽ được tham gia quay số ngẫu nhiên bằng phần mềm máy tính, chọn ra người may mắn nhất nhận giải thưởng Chung cuộc:</span></p>
        <ul>
          <li><span>01 xe Honda Wave RSX mới nhất</span></li>
        </ul>
        <p></p>
        <p><b><span><span>b. BÌNH CHỌN VIDEO "BẠN YÊU THÍCH PHONG CÁCH NÀO NHẤT" </span></span></b></p>
        <p><span>Mỗi tuần BTC sẽ chọn ra 04 người may mắn nhất tham gia bình chọn mỗi tuần để trao giải thưởng thẻ cào điện thoại trị giá 200.000 VNĐ. Mỗi người tham gia bình chọn chỉ được nhận quà may mắn duy nhất một lần trong cả cuộc thi.</span></p>
        <p><b><span><span>5</span></span></b><b><span><span>. HÌNH</span></span></b><b><span><span> THỨC TRAO GIẢI</span></span></b></p>
        <ul>
          <li><span>Đối với quà tặng là Xe Honda Wave RSX, trao giải tại Cửa hàng Bán xe và Dịch vụ do Honda ủy nhiệm (HEAD) gần nhất.</span></li>
          <li><span>Đối với quà tặng là Máy ảnh, Tai nghe bluetooth, BTC chuyển qua đường bưu điện tới địa chỉ người chơi đăng ký trên Website.</span></li>
          <li><span>Đối với thẻ điện thoại: BTC gửi mã thẻ điện thoại vào email đăng ký của người chơi.</span></li>
        </ul>
        <p><span>*Quà tặng sẽ gửi cho các bạn trong vòng 20 ngày kể từ khi bạn thực hiện đầy đủ theo yêu cầu của BTC.</span></p>
        <p><b><span><span>6. THỜI GIAN THAM GIA</span></span></b></p>
        <ul>
          <li><span>Tuần 1: Từ 09:00 sáng 10/09/2019 đến 08:59 sáng 17/09/2019</span></li>
          <li><span>Tuần 2: Từ 09:00 sáng 17/09/2019 đến 08:59 sáng 24/09/2019</span></li>
          <li><span>Tuần 3: Từ 09:00 sáng 24/09/2019 đến 08:59 sáng 01/10/2019</span></li>
          <li><span>Tuần 4: Từ 09:00 sáng 01/10/2019 đến 08:59 sáng 08/10/2019</span></li>
          <li><span>Tuần 5: Từ 09:00 sáng 08/10/2019 đến 08:59 sáng 15/10/2019</span></li>
          <li><span>Tuần 6: Từ 09:00 sáng 15/10/2019 đến 08:59 sáng 22/10/2019</span></li>
        </ul>
        <p><b><span><span>7. THỜI GIAN CÔNG</span></span></b><b><span><span> BỐ</span></span></b></p>
        <p><b><span><span>A. </span></span></b><b><span><span>GIẢI THƯỞNG TUẦN</span></span></b></p>
        <ul>
          <li><span>Số lượng bình chọn sẽ được chốt ngay sau khi kết thúc thời gian dự thi của tuần.</span></li>
          <li><span>BTC sẽ tổng hợp và công bố danh sách trúng thưởng sau 3 ngày kể từ ngày kết thúc nhận bài của tuần đó trên website chương trình.</span></li>
        </ul>
        <p></p>
        <p><b><span><span>B. </span></span></b><b><span><span>GIẢI CHUNG CUỘC</span></span></b></p>
        <ul>
          <li><span>BTC sẽ tổng kết danh sách </span><span><span>10</span></span><span><span> bài dự</span></span><span> thi có tổng số lượt bình chọn từ Website cao nhất mỗi tuần sau 03 ngày kể từ ngày kết thúc thời gian dự thi của tuần cuối cùng.</span></li>
          <li><span>BTC thực hiện quay số ngẫu nhiên bằng phần mềm máy tính và công bố kết quả trong 03 ngày kể từ ngày kết thúc việc thực hiện quay số, kết quả được công bố trên website.</span></li>
        </ul>
        <p></p>
        <p><b><span><span>C. </span></span></b><b><span><span>GIẢI</span></span></b><b><span><span> BÌNH CHỌN VIDEO PHONG CÁCH NHẤT</span></span></b><b><span> </span></b></p>
        <p><span>BTC sẽ tổng kết danh sách tham gia bình chọn hàng tuần và tiến hành quay số ngẫu nhiên bằng phần mềm máy tính chọn ra 04 người chơi may mắn nhất và công bố trên website trong vòng 03 ngày kể từ ngày kết thúc thời gian dự thi của tuần.</span></p>
        <p><b>8. TIÊU CHÍ VIDEO DỰ THI</b></p>
        <ul>
          <li><span>Video dự thi phải có người tham dự trong khung hình và xe thực tế ảo Honda Wave RSX. Người dự thi phải xuất hiện rõ mặt không sử dụng hiệu ứng để che mặt. Ban tổ chức sẽ kiểm tra và loại các video không đúng qui định.</span></li>
          <li><span><span>Video dự thi phải được chia sẻ từ website chương trình lên Facebook</span></span><span> </span><span><span>ở chế độ công khai, kèm hashtag </span><i><span>#HondaWaveRSX</span></i><span> </span><i><span>#RucSucTreXungDamMe</span></i><span>, </span><i><span>#RiSing</span></i></span><i><span><span>X</span></span></i><i><span><span>pression</span></span></i></li>
          <li><span>BTC có toàn quyền quyết định về việc video clip dự thi có đạt tiêu chuẩn và phù hợp với quy định của cuộc thi hay không. Quyết định của BTC là quyết định cuối cùng.</span></li>
        </ul>
        <p></p>
        <p><b><span><span>9. QUYỀN VÀ TRÁCH NHIỆM CỦA NGƯỜI THAM GIA</span></span></b></p>
        <ul>
          <li><span>Người tham dự phải điền đủ thông tin cá nhân. Thông tin cá nhân đăng ký sẽ là thông tin đối chiếu khi trao các giải thưởng may mắn, tuần và chung cuộc; người chơi phải chịu trách nhiệm về thông tin đăng ký; trong trường hợp có sự sai lệch thông tin thì BTC không giải quyết trao giải.</span></li>
          <li><span>Người tham gia dự thi có trách nhiệm cung cấp CMND hoặc Hộ chiếu hoặc sổ hộ khẩu để BTC đối chiếu khi nhận giải.</span></li>
          <li><span>Người tham gia dự thi có trách nhiệm ký vào biên bản nhận giải của BTC.</span></li>
          <li><span>Trong thời gian 07 ngày kể từ lúc BTC thông báo danh sách trúng thưởng trên trang Website, BTC sẽ liên hệ theo thông tin người chơi cung cấp trên trong khi nộp bài để trao giải. Trong mọi trường hợp thông tin cung cấp sai hay được gửi sau thời gian quy định sẽ bị hủy giải thưởng.</span></li>
          <li><span>Người trúng giải có giá trị trên 10.000.000 VND trở lên phải tự nộp thuế thu nhập cá nhân theo quy định của nhà nước Việt Nam.</span></li>
        </ul>
        <p></p>
        <p><b><span><span>10. QUYỀN VÀ TRÁCH NHIỆM CỦA BTC</span></span></b></p>
        <ul>
          <li><span>Những video dự thi không đạt tiêu chí dự thi nêu tại điều 8, hình ảnh không phù hợp với thuần phong mỹ tục Việt Nam, hoặc chứa nội dung đã được đăng ký bản quyền, logo của các thương hiệu thì bài dự thi đó coi như không hợp lệ. BTC có toàn quyền gỡ bỏ những tác phẩm này.</span></li>
          <li><span>BTC có toàn quyền sử dụng hình ảnh, tên tuổi của người chơi cũng như tác phẩm dự thi (nội dung và hình ảnh) mà không phải trả bất kì khoản phí nào liên quan đến quyền tác giả cho người chơi.</span></li>
          <li><span>BTC có toàn quyền loại bỏ người chơi tham gia ra khỏi cuộc thi mà không cần thông báo trước nếu BTC phát hiện người chơi đó thực hiện bất cứ hành động nào gây ảnh hưởng đến kết quả trung thực cuối cùng của cuộc thi.</span></li>
          <li><span>Nếu có bất kỳ thay đổi nào về thể lệ của chương trình, BTC sẽ thông báo trên website của cuộc thi.</span></li>
          <li><span>Trong vòng 20 ngày kể từ ngày BTC công bố danh sách nhận thưởng, người tham gia có tên trong danh sách trúng thưởng sẽ nhận được giải thưởng từ chương trình.</span></li>
          <li><span>BTC có trách nhiệm bảo mật thông tin cá nhân cho người chơi tham gia cuộc thi này, không chia sẻ cho bên thứ ba và chỉ sử dụng cho mục đích trao đổi thông tin giữa công ty và người chơi.</span></li>
        </ul>
        <p></p>
        <p><b><span><span>11. QUY ĐỊNH CHUNG</span></span></b></p>
        <ul>
          <li><span>Người chơi tham gia cần phải đọc và hiểu rõ thể lệ của cuộc thi, đồng ý và tuân theo quy định bản thể lệ này. Người tham gia phải đảm bảo mình có toàn quyền sử dụng và sở hữu hợp pháp tác phẩm dự thi, và miễn trừ trách nhiệm cho Honda Việt Nam nếu xảy ra bất kỳ tranh chấp nào liên quan đến bản quyền của tác phẩm dự thi.</span></li>
          <li><span>Lượt bình chọn hợp lệ của cuộc thi được tính bằng tổng số lượt thích trên website.</span></li>
          <li><span>Mỗi tài khoản chỉ được tham gia dự thi một lần trong một tuần. Các video của mỗi cá nhân phải khác nhau mỗi tuần.</span></li>
          <li><span>Mỗi tài khoản đăng nhập Website chỉ được bình chọn một lần và được chia sẻ không giới hạn cho 01 bài dự thi.</span></li>
          <li><span>Người dự thi lưu ý không được gian lận số lượt bình chọn dưới mọi hình thức. Nếu BTC phát hiện có gian lận trong số lượt bình chọn thì bài dự thi không đủ điều kiện để nhận giải thưởng.</span></li>
          <li><span>Mỗi người chơi chỉ được thắng tối đa 01 giải tuần và 01 giải chung cuộc trong toàn cuộc thi.</span></li>
          <li><span>BTC sẽ đối chiếu với CMND hoặc Hộ chiếu hoặc sổ hộ khẩu khi người chơi đoạt giải và nhận giải tuần và giải chung cuộc.</span></li>
          <li><span>Trong trường hợp phát sinh tranh chấp, khiếu nại liên quan đến cuộc thi, BTC sẽ trực tiếp giải quyết và quyết định của BTC là kết quả cuối cùng.</span></li>
          <li><span>Mọi thắc mắc về chương trình, người dự thi có thể liên hệ với BTC cuộc thi để được giải đáp:</span></li>
        </ul>
        <ul>
          <li><span>Địa chỉ e-mail: rucsuctrexungdamme@gmail.com</span></li>
          <li><span>Trang web cuộc thi: https://honda-rsx.com.vn</span></li>
          <li><span><span>Hotline</span></span><span><span>: 0909298235</span></span></li>
        </ul>
        <p></p>
      </div>
                </Col>
              </Row>
            </div>
          </div>
        </MainLayout>
      </div>
    );
  }
}

export default connect(
  state => ({}),
  dispatch => channingActions({}, dispatch),
)(Terms);
