import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { Input, Row, Col, Table, Card, Pagination, Icon, Select, Button, Form, Tag, Modal } from 'antd';

import Head from 'next/head';
import _ from 'lodash';

import '../../../layout/CmsTemplate.less';
import './contents-ranks.less';

import CmsLayout from '../../../layout/CmsLayout';
import Utils from '../../../lib/Utils';

// import { bindAdminActions } from '../../../redux/actions/admin';
// import { bindContentActions } from '../../../redux/actions/content';
import { channingActions } from '../../../lib/helper';

import { withAuth } from '../../../lib/auth';
import { ROLES } from '../../../config';
import HondaDropdown from '../../../components/honda/inputs/HondaDropdown';
import BasicSelect from '../../../components/forms/main/selects/BasicSelect';

const { Option } = Select;

function handleChangeSelect(value) {
    console.log(`selected ${value}`);
}

class ContentsRanks extends React.Component {
    static async getInitialProps(ctx) {
        return {
            // type: ctx.query.type
        };
    }

    constructor(props) {
        super(props);

        this.META = {
            title: 'Xếp Hạng Nội Dung',
        };

        this.state = {
            data: [],
            sortDesc: 1,
            searchName: '',
            page: 1,
            limit: 10,
            options: [
                '09/09/2019 - 16/09/2019',
                '16/09/2019 - 23/09/2019',
                '23/09/2019 - 30/09/2019',
                '30/09/2019 - 07/10/2019',
                '07/10/2019 - 14/10/2019',
                '14/10/2019 - 21/09/2019',
            ],
            dataSource: [],
        };

        const { type } = this.props;
    }

    componentDidMount() {
        // this.performSearch = _.debounce(this.performSearch.bind(this), 1000);
        // this.queryOrders();
    }

    componentDidUpdate(prevProps, prevState) {
        // if (prevState.page !== this.state.page) {
        //     this.queryOrders();
        // }
    }

    showModalAddPrize = () => {
        this.setState({
            visible: true,
        });
    };

    handleOkAddPrize = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancelAddPrize = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    onAddPrize = e => {
        console.log(e.target.value);
    };

    getSelectedWeek = e => {
        console.log(e.target.value);
    };

    onChangeSearchTerm = e => {
        this.setState({
            searchName: e.target.value,
        });
        this.performSearch();
    };

    onChangePage = (page, pageSize) => {
        this.setState({ page });
    };

    performSearch() {
        this.setState({ page: 1 });
        this.queryOrders();
    }

    queryOrders() {
        const { searchName, page, limit } = this.state;
        // this.props.orderActions.getOrders(searchName, page, limit);
        this.props.orderActions.getOrders(searchName);
    }

    render() {
        const { form, type, orders, ordersCount } = this.props;
        const { searchName, options } = this.state;

        const columns = [
            {
                title: 'NỘI DUNG',
                key: 'thumbnail',
                render: record => <img src={record.thumbnail} />,
            },
            {
                title: 'HỌ TÊN',
                key: 'fullName',
                render: record => (
                    <>
                        {record.firstName} {record.lastName}
                    </>
                ),
            },
            {
                title: 'SỐ LƯỢT BÌNH CHỌN',
                key: 'likes',
                render: record => <>{record.likes.length}</>,
            },
            {
                title: 'NGÀY TẠO',
                key: 'createdAt',
                render: record => <>{record.createdAt}</>,
            },
            {
                title: '',
                key: 'action',
                render: (text, record) => (
                    <>
                        <Button className="button" onClick={() => this.onClickDelete(record)}>
                            XÓA
                        </Button>
                        <Button className="button" onClick={() => this.onClickSetRange(record)}>
                            XẾP HẠNG
                        </Button>
                    </>
                ),
            },
        ];

        return (
            <>
                <Head>
                    <title>{this.META.title}</title>
                </Head>
                <CmsLayout title={this.META.title}>
                    <div className="cmsTemplate-list cmsUsers-orders">
                        <Row className="action">
                            <div className="colLeft">
                                <Input.Search
                                    placeholder="Họ tên"
                                    onSearch={this.performSearch}
                                    onChange={this.onChangeSearchTerm}
                                    allowClear
                                    className="search"
                                    value={searchName}
                                />

                                {/* <Select
                                    defaultValue={this.state.sortDesc}
                                    className="sort"
                                    onSelect={this.onChangeSorting}
                                >
                                    <Select.Option value={1}>MỚI NHẤT</Select.Option>
                                    <Select.Option value={0}>CŨ NHẤT</Select.Option>
                                </Select> */}

                                <Modal
                                    title="Thêm Nội Dung Xếp Hạng"
                                    visible={this.state.visible}
                                    onOk={this.handleOkAddPrize}
                                    onCancel={this.handleCancelAddPrize}
                                >
                                    <Row>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            Hạng Đặc Biệt
                                        </Col>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            <Input />
                                        </Col>

                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            Hạng 1
                                        </Col>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            <Input />
                                        </Col>

                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            Hạng 2
                                        </Col>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            <Input />
                                        </Col>

                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            Hạng 3
                                        </Col>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            <Input />
                                        </Col>

                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            Hạng 4
                                        </Col>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            <Input />
                                        </Col>

                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            Hạng 5
                                        </Col>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            <Input />
                                        </Col>
                                    </Row>
                                </Modal>
                            </div>

                            <div className="colRight">
                                {/* <Button className="createButton" onClick={this.showCreateOrderModal}>
                                    <Icon type="plus" /> THÊM ĐƠN HÀNG
                                </Button> */}
                                {/* <HondaDropdown /> */}

                                {this.state.dataSource.length >= 6 ? (
                                    <Button style={{ display: 'margin: 0 0.5rem' }} type="primary" disabled>
                                        Thêm
                                    </Button>
                                ) : (
                                    <Button
                                        style={{ display: 'margin: 0 0.5rem' }}
                                        type="primary"
                                        onClick={this.showModalAddPrize}
                                    >
                                        Thêm
                                    </Button>
                                )}

                                <div>
                                    <BasicSelect onChange={this.getSelectedWeek} selectData={options} width="120px" />
                                </div>
                            </div>
                        </Row>

                        <Row className="result">
                            <Table
                                size="middle"
                                columns={columns}
                                dataSource={orders}
                                rowKey="_id"
                                bordered={false}
                                loading={false}
                                pagination={false}
                                expandedRowRender={this.expandedRowRender}
                            />
                        </Row>
                        <Row>
                            <Pagination
                                onChange={this.onChangePage}
                                pageSize={this.state.limit}
                                total={ordersCount}
                                className="pagination"
                                size="small"
                                current={this.state.page}
                            />
                        </Row>
                    </div>
                </CmsLayout>
            </>
        );
    }
}

const contents = Form.create()(ContentsRanks);
export default withAuth(contents);
