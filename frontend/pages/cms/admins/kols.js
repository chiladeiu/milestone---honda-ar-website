import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { Input, Row, Col, Table, Card, Pagination, Icon, Select, Button, Form, Tag, Modal } from 'antd';

import Head from 'next/head';
import _ from 'lodash';

import '../../../layout/CmsTemplate.less';
import './kols.less';

import CmsLayout from '../../../layout/CmsLayout';
import Utils from '../../../lib/Utils';

// import { bindAdminActions } from '../../../redux/actions/admin';
// import { bindContentActions } from '../../../redux/actions/content';
import { channingActions } from '../../../lib/helper';
import BasicSelect from '../../../components/forms/main/selects/BasicSelect';
import { withAuth } from '../../../lib/auth';
import { ROLES, TIMEZONE_DIFF, TIMEZONE } from '../../../config';
import { KOLVideosAPI } from '../../../api/kol-videos';

class Kols extends React.Component {
    static async getInitialProps(ctx) {
        return {
            // type: ctx.query.type
        };
    }

    constructor(props) {
        super(props);

        this.META = {
            title: 'Bình Chọn Kols',
        };

        this.state = {
            data: [],
            sortDesc: 1,
            searchName: '',
            page: 1,
            limit: 10,
            votes: [],
            loading: false,
            exporting: false,
        };

        const { type } = this.props;
    }

    async componentDidMount() {
        // this.performSearch = _.debounce(this.performSearch.bind(this), 1000);
        // this.queryOrders();

        this.setState({ loading: true });
        const response = await KOLVideosAPI.adminGetVoted();
        this.setState({ votes: response.data.payload, loading: false });
    }

    componentDidUpdate(prevProps, prevState) {
        // if (prevState.page !== this.state.page) {
        //     this.queryOrders();
        // }
    }

    onChangeSearchTerm = e => {
        this.setState({
            searchName: e.target.value,
        });
        this.performSearch();
    };

    onChangePage = (page, pageSize) => {
        this.setState({ page });
    };

    performSearch() {
        this.setState({ page: 1 });
        this.queryOrders();
    }

    queryOrders() {
        const { searchName, page, limit } = this.state;
        // this.props.orderActions.getOrders(searchName, page, limit);
        this.props.orderActions.getOrders(searchName);
    }

    expandedRowRender = record => {
        const columns = [
            { align: 'left', title: 'Tên', dataIndex: 'name', key: 'name' },
            { align: 'right', title: 'Số lượng', dataIndex: 'quantity', key: 'quantity' },
            {
                align: 'right',
                title: 'Đơn giá',
                dataIndex: 'price',
                key: 'price',
                render: price => Utils.formatNumberToVND(price),
            },
            {
                align: 'right',
                title: 'Điểm',
                dataIndex: 'point',
                key: 'point',
                render: point => Utils.formatWithCommas(point),
            },
        ];
        return <Table columns={columns} dataSource={record.products} pagination={false} />;
    };

    render() {
        const { form, type, orders, ordersCount } = this.props;
        const { votes, loading, exporting } = this.state;

        const columns = [
            {
                title: 'VIDEO',
                key: 'video',
                dataIndex: 'video',
            },
            {
                title: 'USER ID',
                key: 'userId',
                dataIndex: 'user.id',
            },
            {
                title: 'THÔNG TIN FACEBOOK',
                key: 'facebookName',
                dataIndex: 'user.facebookName',
            },
            {
                title: 'THÔNG TIN NGƯỜI DÙNG',
                key: 'name',
                dataIndex: 'user',
                render: user => (
                    <>
                        {user.name}
                        <br />
                        {user.email}
                        <br />
                        {user.phone}
                    </>
                ),
            },
            {
                title: 'NGÀY VOTE',
                key: 'createdAt',
                dataIndex: 'createdAt',
                render: timestamp => moment.unix(timestamp).format('DD/MM/YYYY - HH:mm:ss'),
            },
        ];

        return (
            <>
                <Head>
                    <title>{this.META.title}</title>
                </Head>
                <CmsLayout title={this.META.title}>
                    <div className="cmsTemplate-list cmsUsers-orders">
                        <Row className="action">
                            <Col>
                                <div className="colLeft">
                                    {/* <Input.Search
                                    placeholder="Họ tên"
                                    onSearch={this.performSearch}
                                    onChange={this.onChangeSearchTerm}
                                    allowClear
                                    className="search"
                                    value={searchName}
                                /> */}
                                    {/* <Select
                                    defaultValue={this.state.sortDesc}
                                    className="sort"
                                    onSelect={this.onChangeSorting}
                                >
                                    <Select.Option value={1}>MỚI NHẤT</Select.Option>
                                    <Select.Option value={0}>CŨ NHẤT</Select.Option>
                                </Select> */}
                                    {this.state.votes.length} dòng
                                </div>
                            </Col>

                            <Col>
                                <div className="colRight">
                                    {/* <Button className="createButton" onClick={this.showCreateOrderModal}>
                                    <Icon type="plus" /> THÊM ĐƠN HÀNG
                                </Button> */}
                                    {/* <div style={{ float: 'right' }}>
                                        <BasicSelect />
                                    </div> */}
                                    <Button
                                        loading={exporting}
                                        style={{ float: 'right', marginRight: '10px' }}
                                        onClick={async () => {
                                            this.setState({ exporting: true });
                                            await KOLVideosAPI.export();
                                            this.setState({ exporting: false });
                                        }}
                                    >
                                        Xuất Excel
                                    </Button>
                                </div>
                            </Col>
                        </Row>

                        <Row className="result">
                            <Table
                                size="middle"
                                columns={columns}
                                dataSource={votes}
                                rowKey="id"
                                bordered={false}
                                loading={loading}
                                pagination={false}
                            />
                        </Row>
                        {/* <Row>
                            <Pagination
                                onChange={this.onChangePage}
                                pageSize={this.state.limit}
                                total={ordersCount}
                                className="pagination"
                                size="small"
                                current={this.state.page}
                            />
                        </Row> */}
                    </div>
                </CmsLayout>
            </>
        );
    }
}

const kols = Form.create()(Kols);
export default withAuth(kols);
