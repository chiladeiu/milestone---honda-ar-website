import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { Input, Row, Col, Table, Card, Pagination, Icon, Select, Button, Form, Tag, Modal } from 'antd';

import Head from 'next/head';
import _ from 'lodash';

import '../../../layout/CmsTemplate.less';
import './kols-ranks.less';

import CmsLayout from '../../../layout/CmsLayout';
import Utils from '../../../lib/Utils';

// import { bindAdminActions } from '../../../redux/actions/admin';
// import { bindContentActions } from '../../../redux/actions/content';
import { channingActions } from '../../../lib/helper';
import BasicSelect from '../../../components/forms/main/selects/BasicSelect';
import { withAuth } from '../../../lib/auth';
import { ROLES } from '../../../config';

class ContentsRanks extends React.Component {
    static async getInitialProps(ctx) {
        return {
            // type: ctx.query.type
        };
    }

    constructor(props) {
        super(props);

        this.META = {
            title: 'KOLS Rank',
        };

        this.state = {
            visible: false,
            data: [],
            sortDesc: 1,
            searchName: '',
            page: 1,
            limit: 10,
            dataSource: [
                {
                    thumbnail: 'ABC',
                    firstName: 'DEF',
                    lastName: 'GHI',
                    likes: 'JKL',
                    createdAt: 'MNO',
                },
                {
                    thumbnail: 'ABC',
                    firstName: 'DEF',
                    lastName: 'GHI',
                    likes: 'JKL',
                    createdAt: 'MNO',
                },
                {
                    thumbnail: 'ABC',
                    firstName: 'DEF',
                    lastName: 'GHI',
                    likes: 'JKL',
                    createdAt: 'MNO',
                },
            ],
        };

        const { type } = this.props;
    }

    componentDidMount() {
        // this.performSearch = _.debounce(this.performSearch.bind(this), 1000);
        // this.queryOrders();
        const { data } = this.state;
    }

    componentDidUpdate(prevProps, prevState) {
        // if (prevState.page !== this.state.page) {
        //     this.queryOrders();
        // }
    }

    showModalAddKOL = () => {
        this.setState({
            visible: true,
        });
    };

    handleOkAddKOL = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    handleCancelAddKOL = e => {
        console.log(e);
        this.setState({
            visible: false,
        });
    };

    onAddKOL = e => {
        console.log(e.target.value);
    };

    onChangeSearchTerm = e => {
        this.setState({
            searchName: e.target.value,
        });
        this.performSearch();
    };

    onChangePage = (page, pageSize) => {
        this.setState({ page });
    };

    performSearch() {
        this.setState({ page: 1 });
        this.queryOrders();
    }

    queryOrders() {
        const { searchName, page, limit } = this.state;
        // this.props.orderActions.getOrders(searchName, page, limit);
        this.props.orderActions.getOrders(searchName);
    }

    expandedRowRender = record => {
        const columns = [
            { align: 'left', title: 'Tên', dataIndex: 'name', key: 'name' },
            { align: 'right', title: 'Số lượng', dataIndex: 'quantity', key: 'quantity' },
            {
                align: 'right',
                title: 'Đơn giá',
                dataIndex: 'price',
                key: 'price',
                render: price => Utils.formatNumberToVND(price),
            },
            {
                align: 'right',
                title: 'Điểm',
                dataIndex: 'point',
                key: 'point',
                render: point => Utils.formatWithCommas(point),
            },
        ];
        return <Table columns={columns} dataSource={record.products} pagination={false} />;
    };

    render() {
        const { form, type, orders, ordersCount } = this.props;
        const { searchName } = this.state;

        const columns = [
            {
                title: 'NỘI DUNG',
                key: 'thumbnail',
                render: record => <img src={record.thumbnail} />,
            },
            {
                title: 'HỌ TÊN',
                key: 'fullName',
                render: record => (
                    <>
                        {record.firstName} {record.lastName}
                    </>
                ),
            },
            {
                title: 'SỐ LƯỢT BÌNH CHỌN',
                key: 'likes',
                render: record => <>{record.likes.length}</>,
            },
            {
                title: 'NGÀY TẠO',
                key: 'createdAt',
                render: record => <>{record.createdAt}</>,
            },
            {
                title: '',
                key: 'action',
                render: (text, record) => (
                    <>
                        <Button className="button" onClick={() => this.onClickDelete(record)}>
                            XÓA
                        </Button>
                        <Button className="button" onClick={() => this.onClickSetRange(record)}>
                            XẾP HẠNG
                        </Button>
                    </>
                ),
            },
        ];

        return (
            <>
                <Head>
                    <title>{this.META.title}</title>
                </Head>
                <CmsLayout title={this.META.title}>
                    <div className="cmsTemplate-list cmsUsers-orders">
                        <Row className="action">
                            <div className="colLeft">
                                <Input.Search
                                    placeholder="Họ tên"
                                    onSearch={this.performSearch}
                                    onChange={this.onChangeSearchTerm}
                                    allowClear
                                    className="search"
                                    value={searchName}
                                />
                                {/* <Select
                                    defaultValue={this.state.sortDesc}
                                    className="sort"
                                    onSelect={this.onChangeSorting}
                                >
                                    <Select.Option value={1}>MỚI NHẤT</Select.Option>
                                    <Select.Option value={0}>CŨ NHẤT</Select.Option>
                                </Select> */}
                            </div>

                            <div className="colRight">
                                {/* <Button className="createButton" onClick={this.showCreateOrderModal}>
                                    <Icon type="plus" /> THÊM ĐƠN HÀNG
                                </Button> */}

                                {this.state.dataSource.length >= 4 ? (
                                    <Button type="primary" disabled>
                                        Thêm KOL
                                    </Button>
                                ) : (
                                    <Button type="primary" onClick={this.showModalAddKOL}>
                                        Thêm KOL
                                    </Button>
                                )}

                                <Modal
                                    title="Thêm KOL Xếp Hạng"
                                    visible={this.state.visible}
                                    onOk={this.handleOkAddKOL}
                                    onCancel={this.handleCancelAddKOL}
                                >
                                    <Row>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            Hạng 1
                                        </Col>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            <Input />
                                        </Col>

                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            Hạng 2
                                        </Col>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            <Input />
                                        </Col>

                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            Hạng 3
                                        </Col>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            <Input />
                                        </Col>

                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            Hạng 4
                                        </Col>
                                        <Col style={{ margin: '0.5rem 0' }} span={12}>
                                            <Input />
                                        </Col>
                                    </Row>
                                </Modal>

                                <BasicSelect />
                            </div>
                        </Row>

                        <Row className="result">
                            <Table
                                size="middle"
                                columns={columns}
                                dataSource={this.state.dataSource}
                                rowKey="_id"
                                bordered={false}
                                loading={false}
                                pagination={false}
                                expandedRowRender={this.expandedRowRender}
                            />
                        </Row>
                        {/* <Row>
                            <Pagination
                                onChange={this.onChangePage}
                                pageSize={this.state.limit}
                                total={ordersCount}
                                className="pagination"
                                size="small"
                                current={this.state.page}
                            />
                        </Row> */}
                    </div>
                </CmsLayout>
            </>
        );
    }
}

const kols = Form.create()(ContentsRanks);
export default withAuth(kols);
