import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { Input, Row, Col, Table, Card, Pagination, Icon, Select, Button, Form, Tag, Modal, Spin } from 'antd';

import Head from 'next/head';
import _ from 'lodash';

import '../../../layout/CmsTemplate.less';
import './users.less';

import CmsLayout from '../../../layout/CmsLayout';
import Utils from '../../../lib/Utils';

// import { bindAdminActions } from '../../../redux/actions/admin';
// import { bindContentActions } from '../../../redux/actions/content';
import { channingActions } from '../../../lib/helper';

import { withAuth } from '../../../lib/auth';
import { ROLES } from '../../../config';
import HondaDropdown from '../../../components/honda/inputs/HondaDropdown';
import BasicSelect from '../../../components/forms/main/selects/BasicSelect';
import { UsersAPI } from '../../../api/users';

const { Option } = Select;

function handleChangeSelect(value) {
    console.log(`selected ${value}`);
}

class Users extends React.Component {
    static async getInitialProps(ctx) {
        return {
            // type: ctx.query.type
        };
    }

    constructor(props) {
        super(props);

        this.META = {
            title: 'Quản Lý Người Dùng',
        };

        this.state = {
            data: [],
            loading: false,
            totalUsers: 0,
            pageLimit: 100,
            currentPage: 0,
        };

        const { type } = this.props;
    }

    async fetchUsers(lastUserId = null) {
        const { data, pageLimit, currentPage } = this.state;

        this.setState({ loading: true });

        const response = await UsersAPI.get(0);
        const fetchedUsers = response.data.payload;
        this.setState({
            loading: false,
            data: fetchedUsers,
            exporting: false,
        });
    }

    // onChange = page => {
    //     const { pageLimit, data, totalUsers } = this.state;

    //     if (page * pageLimit >= data.length && data.length < totalUsers) {
    //         this.fetchUsers(data[data.length - 1].id);
    //     }

    //     this.setState({
    //         currentPage: page,
    //     });
    // };

    async componentDidMount() {
        // this.performSearch = _.debounce(this.performSearch.bind(this), 1000);

        // const response = await UsersAPI.count();
        // this.setState({ totalUsers: response.data.payload });

        await this.fetchUsers();
    }

    componentDidUpdate(prevProps, prevState) {
        // if (prevState.page !== this.state.page) {
        //     this.queryOrders();
        // }
    }

    onChangeSearchTerm = e => {
        this.setState({
            searchName: e.target.value,
        });

        this.performSearch();
    };

    onChangePage = (page, pageSize) => {
        this.setState({ page });
    };

    performSearch() {
        this.setState({ page: 1 });
        this.queryOrders();
    }

    render() {
        const { form, type } = this.props;
        const { data, loading, totalUsers, pageLimit, currentPage, exporting } = this.state;

        const columns = [
            {
                title: 'Id',
                key: 'id',
                dataIndex: 'id',
            },
            {
                title: 'THÔNG TIN',
                key: 'info',
                render: user => (
                    <div>
                        {user.phone}
                        <br />
                        {user.name}
                        <br />
                        {user.email}
                    </div>
                ),
            },
            {
                title: 'FACEBOOK',
                key: 'facebook',
                render: user => (
                    <div>
                        {user.facebookId}
                        <br />
                        {user.facebookName}
                        <br />
                        {user.facebookEmail}
                    </div>
                ),
            },
            {
                title: 'NGÀY TẠO',
                key: 'createdAt',
                dataIndex: 'createdAt',
                render: timestamp => moment.unix(timestamp).format('DD/MM/YYYY - HH:mm:ss'),
            },
        ];

        return (
            <>
                <Head>
                    <title>{this.META.title}</title>
                </Head>
                <CmsLayout title={this.META.title}>
                    <div className="cmsTemplate-list cmsUsers-orders">
                        {data.length > 0 ? (
                            <>
                                <Row className="action">
                                    <Col>
                                        <div className="colLeft">
                                            {data.length} dòng
                                            {/* <Input.Search
                                    placeholder="Họ tên"
                                    onSearch={this.performSearch}
                                    onChange={this.onChangeSearchTerm}
                                    allowClear
                                    className="search"
                                    value={searchName}
                                /> */}
                                            {/* <Select
                                    defaultValue={this.state.sortDesc}
                                    className="sort"
                                    onSelect={this.onChangeSorting}
                                >
                                    <Select.Option value={1}>MỚI NHẤT</Select.Option>
                                    <Select.Option value={0}>CŨ NHẤT</Select.Option>
                                </Select> */}
                                        </div>
                                    </Col>

                                    <Col>
                                        <div className="colRight">
                                            <Button
                                                loading={exporting}
                                                onClick={async () => {
                                                    this.setState({ exporting: true });
                                                    await UsersAPI.export();
                                                    this.setState({ exporting: false });
                                                }}
                                            >
                                                Xuất Excel
                                            </Button>
                                        </div>
                                    </Col>
                                </Row>

                                <Row className="result">
                                    <Table
                                        size="middle"
                                        columns={columns}
                                        dataSource={data}
                                        rowKey="id"
                                        bordered={false}
                                        loading={loading}
                                        pagination={{
                                            pageSize: 100,
                                            position: 'both',
                                        }}
                                    />
                                </Row>
                                {/* <Row>
                            <Button loading={loading} onClick={() => this.fetchUsers(data[data.length - 1].id)}>
                                Fetch More
                            </Button>
                        </Row> */}
                            </>
                        ) : (
                            <Spin />
                        )}
                    </div>
                </CmsLayout>
            </>
        );
    }
}

const users = Form.create()(Users);
export default withAuth(users);
