import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';
import { Input, Row, Col, Table, Card, Pagination, Icon, Select, Button, Form, Tag, Modal, Spin } from 'antd';

import Head from 'next/head';
import _ from 'lodash';

import '../../../layout/CmsTemplate.less';
import './contents.less';

import CmsLayout from '../../../layout/CmsLayout';
import Utils from '../../../lib/Utils';

// import { bindAdminActions } from '../../../redux/actions/admin';
// import { bindContentActions } from '../../../redux/actions/content';
import { channingActions, getMediaURL } from '../../../lib/helper';

import { withAuth } from '../../../lib/auth';
import { ROLES, CMSRole, ROUTES } from '../../../config';
import HondaDropdown from '../../../components/honda/inputs/HondaDropdown';
import BasicSelect from '../../../components/forms/main/selects/BasicSelect';
import { UserVideosAPI } from '../../../api/user-videos';
import { userInfo } from 'os';
import { HEAD_DATA } from '../../../head-data';

const { Option } = Select;

function handleChangeSelect(value) {
    console.log(`selected ${value}`);
}

class Contents extends React.Component {
    static async getInitialProps(ctx) {
        return {
            // type: ctx.query.type
        };
    }

    constructor(props) {
        super(props);

        this.META = {
            title: 'Quản Lý Nội Dung',
        };

        this.state = {
            data: [],
            loading: false,
            totalVideos: 0,
            pageLimit: 20,
            currentPage: 0,
            exporting: false,
            selectedHEAD: '',
            selectedProvince: '',
        };

        const { type } = this.props;
    }

    async fetchUserVideos(lastVideoId = null) {
        const { data, pageLimit, currentPage } = this.state;

        this.setState({ loading: true });

        const response = await UserVideosAPI.adminFetch(0);
        const fetchedVideos = response.data.payload;

        this.setState({
            loading: false,
            pageLimit: fetchedVideos.length,
            data: fetchedVideos,
            currentPage: Math.floor(data.length / pageLimit),
        });
    }

    // onChange = page => {
    //     const { pageLimit, data, totalUsers } = this.state;

    //     if (page * pageLimit >= data.length && data.length < totalUsers) {
    //         this.fetchUserVideos(data[data.length - 1].id);
    //     }

    //     this.setState({
    //         currentPage: page,
    //     });
    // };

    async componentDidMount() {
        await this.fetchUserVideos();
    }

    componentDidUpdate(prevProps, prevState) {
        // if (prevState.page !== this.state.page) {
        //     this.queryOrders();
        // }
    }

    // onChangeSearchTerm = e => {
    //     this.setState({
    //         searchName: e.target.value,
    //     });
    //     this.performSearch();
    // };

    // onChangePage = (page, pageSize) => {
    //     this.setState({ page });
    // };

    // performSearch() {
    //     this.setState({ page: 1 });
    //     this.queryOrders();
    // }

    // queryOrders() {
    //     const { searchName, page, limit } = this.state;
    //     // this.props.orderActions.getOrders(searchName, page, limit);
    //     this.props.orderActions.getOrders(searchName);
    // }

    onDeleteVideo(record) {
        Modal.confirm({
            title: 'Cảnh báo',
            content: `Bạn đang vô hiệu video "${record.title}"`,
            onOk: async () => {
                await UserVideosAPI.adminDelete(record.id);
                await this.fetchUserVideos();
            },
        });
    }

    expandedRowRender = record => {
        const columns = [
            { title: 'ID', key: 'userId', dataIndex: 'user.id' },
            { title: 'TÊN', key: 'name', dataIndex: 'user.name' },
            {
                title: 'NGÀY VOTE',
                key: 'createdAt',
                dataIndex: 'createdAt',
                render: timestamp => moment.unix(timestamp).format('DD/MM/YYYY - HH:mm:ss'),
            },
        ];

        return _.isEmpty(record.voted) ? null : (
            <Table columns={columns} dataSource={record.voted} pagination={false} />
        );
    };

    exportUserVideo = async () => {
        this.setState({ exporting: true });
        await UserVideosAPI.export();
        this.setState({ exporting: false });
    };

    render() {
        const { form, type, me } = this.props;
        const {
            data,
            loading,
            totalVideos,
            pageLimit,
            currentPage,
            exporting,
            selectedProvince,
            selectedHEAD,
        } = this.state;

        let filteredData = data;

        if (!_.isEmpty(selectedProvince)) {
            filteredData = data.filter(v => _.isEqual(_.get(v, 'headData.province', ''), selectedProvince));
        }

        if (!_.isEmpty(selectedHEAD)) {
            filteredData = filteredData.filter(v => _.isEqual(_.get(v, 'headData.headCode', ''), selectedHEAD));
        }

        const columns = [
            {
                title: 'ID',
                key: 'id',
                dataIndex: 'id',
            },
            {
                title: 'TIÊU ĐỀ',
                key: 'title',
                dataIndex: 'title',
            },
            {
                title: 'NỘI DUNG',
                key: 'thumbnail',
                render: record => (
                    <a target="blank" href={`${ROUTES.CONTENT}?videoId=${record.id}`}>
                        <img src={getMediaURL(record.thumbnail)} />
                    </a>
                ),
            },
            {
                title: 'THÔNG TIN NGƯỜI DÙNG',
                key: 'userInfo',
                dataIndex: 'user',
                render: record => (
                    <>
                        {record.name}
                        <br />
                        {record.phone}
                        <br />
                        {record.email}
                    </>
                ),
            },
            {
                title: 'THÔNG TIN FACEBOOK',
                key: 'userFacebook',
                dataIndex: 'user',
                render: record => (
                    <>
                        {record.facebookId}
                        <br />
                        {record.facebookName}
                        <br />
                        {record.facebookEmail}
                    </>
                ),
            },
            {
                title: 'SỐ LƯỢT BÌNH CHỌN',
                key: 'voteCount',
                dataIndex: 'voteCount',
            },
            {
                title: 'HEAD',
                key: 'head',
                dataIndex: 'headData',
                render: headData => (
                    <>
                        {_.get(headData, 'headCode', '')} <br />
                        {_.get(headData, 'province', '')} <br />
                        {_.get(headData, 'headName', '')} <br />
                    </>
                ),
            },
            {
                title: 'NGÀY TẠO',
                key: 'createdAt',
                dataIndex: 'createdAt',
                render: timestamp => moment.unix(timestamp).format('DD/MM/YYYY - HH:mm:ss'),
            },
            {
                title: '',
                key: 'action',
                render: (text, record) =>
                    me.role === CMSRole.Admin && record.isValid ? (
                        <>
                            <Button className="button" onClick={() => this.onDeleteVideo(record)}>
                                VÔ HIỆU
                            </Button>
                        </>
                    ) : (
                        <Tag color="red">ĐÃ VÔ HIỆU</Tag>
                    ),
            },
        ];

        return (
            <>
                <Head>
                    <title>{this.META.title}</title>
                </Head>
                <CmsLayout title={this.META.title}>
                    {data.length > 0 ? (
                        <div className="cmsTemplate-list cmsAdmins-contents">
                            <Row className="action">
                                <Col>
                                    <div className="colLeft">
                                        <b style={{ marginRight: '10px' }}>HEAD</b>
                                        <Select
                                            style={{ width: '150px', marginRight: '10px' }}
                                            className="selectHeadTown"
                                            value={selectedProvince}
                                            onSelect={selectedProvince =>
                                                this.setState({ selectedProvince, selectedHEAD: '' })
                                            }
                                        >
                                            <Select.Option value={''}>TỈNH/TẤT CẢ</Select.Option>
                                            {_.uniq(HEAD_DATA.map(h => h.province)).map(province => (
                                                <Select.Option value={province}>{province}</Select.Option>
                                            ))}
                                        </Select>
                                        <Select
                                            style={{ width: '200px', marginRight: '30px' }}
                                            className="selectHeadHead"
                                            value={selectedHEAD}
                                            onSelect={selectedHEAD => this.setState({ selectedHEAD })}
                                        >
                                            <Select.Option value={''}>HEAD/TẤT CẢ</Select.Option>
                                            {HEAD_DATA.filter(h => h.province === selectedProvince).map(head => (
                                                <Select.Option value={head.headCode}>{head.headName}</Select.Option>
                                            ))}
                                        </Select>
                                    </div>
                                </Col>
                                <Col>
                                    <div className="colLeft">
                                        {data.length} dòng
                                        {/* <Input.Search
                                        placeholder="Họ tên"
                                        onSearch={this.performSearch}
                                        onChange={this.onChangeSearchTerm}
                                        allowClear
                                        className="search"
                                        value={searchName}
                                    /> */}
                                    </div>
                                </Col>

                                <Col>
                                    <div className="colRight">
                                        <Button loading={exporting} onClick={this.exportUserVideo}>
                                            Xuất Excel
                                        </Button>
                                    </div>
                                </Col>
                            </Row>

                            <Row className="result">
                                <Table
                                    size="middle"
                                    columns={columns}
                                    dataSource={filteredData}
                                    rowKey="id"
                                    bordered={false}
                                    expandedRowRender={this.expandedRowRender}
                                    loading={loading}
                                    pagination={{
                                        pageSize: 50,
                                        position: 'both',
                                    }}
                                />
                            </Row>
                        </div>
                    ) : (
                        <Spin />
                    )}
                </CmsLayout>
            </>
        );
    }
}

const contents = Form.create()(Contents);

export default withAuth(contents);

// export default connect(
//     state => {
//         // const { data, totalOrders } = state.contents;
//         // return {
//         //     contents: data,
//         //     contentsCount: totalContents
//         // };
//     },
//     {},
//     // dispatch => channingActions({}, dispatch, bindAdminActions, bindOrderActions)
// )(contents);

// export default withAuth(
//     connect(
//         state => {
//             const { data, totalOrders } = state.order;
//             return {
//                 orders: data,
//                 ordersCount: totalOrders
//             };
//         },
//         dispatch => channingActions({}, dispatch, bindAdminActions, bindOrderActions)
//     )(orders),
//     ROLES.ADMIN
// );
