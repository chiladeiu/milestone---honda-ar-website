import React from 'react';
import { connect } from 'react-redux';

import { Form, Modal, Spin } from 'antd';

import AuthLayout from '../../layout/AuthLayout';

import Login from '../../components/forms/auth/Login';
// import SignUp from '../../components/forms/auth/SignUp';
// import RecoverPassword from '../../components/forms/auth/RecoverPassword';

import Head from '../../components/forms/Head';
import { SITE_URL, ROUTES, IS_DEV, CMSRole } from '../../config';

// import { login } from '../../lib/auth';
import Router from 'next/router';

import { bindMeActions } from '../../redux/actions/me';

class Auth extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLogingin: false,
            email: IS_DEV ? 'fractalcompany123@gmail.com' : '',
            password: IS_DEV ? 'ZdScVMJryzX3Ea5x' : '',
        };
    }

    componentDidMount() {
        this.props.form.setFieldsValue({
            email: IS_DEV ? 'fractalcompany123@gmail.com' : '',
            password: IS_DEV ? 'ZdScVMJryzX3Ea5x' : '',
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const { me } = this.props;
        if (me.role !== prevProps.me.role) {
            if (me.role === CMSRole.Admin || me.role === CMSRole.Viewer) {
                Router.push(ROUTES.CMS_ADMINS_CONTENTS);
            }
        }
    }

    onLogin = () => {
        this.props.form.validateFields(async (err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                const { email, password } = values;
                this.setState({ isLogingin: true });

                const { meActions } = this.props;
                const fbresponse = await meActions.loginAdmin(email, password);
                console.log('fb admin', fbresponse);

                this.setState({ isLogingin: false });
            }
        });
    };

    render() {
        const { me } = this.props;

        if (!me.isInitFirebase) return <Spin />;

        return (
            <div style={{ overflowX: 'hidden' }}>
                <Head title="" image="/static/shared/fb-img.png" url={SITE_URL} description="" />
                <AuthLayout>
                    <div className="auth">
                        <div className="authBox">
                            {/* {this.props.page === 'login' && ( */}
                            <Login form={this.props.form} isLogingin={this.state.isLogingin} onLogin={this.onLogin} />
                            {/* )} */}
                            {/* {this.props.page === 'signup' && <SignUp form={this.props.form}/>} */}
                            {/* {this.props.page === 'recover-password' && <RecoverPassword form={this.props.form}/>}
                            <p style={{ textAlign: 'center', textTransform: 'uppercase' }}>ROLE <b>{this.ROLE}</b></p> */}
                        </div>
                    </div>
                </AuthLayout>
            </div>
        );
    }
}

const auth = Form.create()(Auth);

export default connect(
    state => ({
        me: state.me,
    }),
    dispatch => bindMeActions({}, dispatch),
)(auth);
