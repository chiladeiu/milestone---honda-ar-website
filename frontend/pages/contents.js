import React from 'react';
import { connect } from 'react-redux';
import { channingActions, getMediaURL } from '../lib/helper';
import { Button, Icon, Row, Col, Carousel, Input, Modal, Tabs, Radio, Affix, Divider, message } from 'antd';
import Link from 'next/link';
import Router from 'next/router';
import _ from 'lodash';
import moment from 'moment';

import './contents.less';

import MainLayout from '../layout/MainLayout';
import Head from '../components/forms/Head';
import BasicSearch from '../components/forms/main/inputs/BasicSearch';
import BasicTab from '../components/forms/main/selects/BasicTab';
import PrizeItem from '../components/honda/prizes/PrizeItem';
import VoteItem from '../components/honda/votes/VoteItem';
import ProcessItem from '../components/honda/process/ProcessItem';
import ProcessItemReverse from '../components/honda/process/ProcessItemReverse';
import SimpleFooter from '../components/forms/main/footers/SimpleFooter';

import { SITE_NAME, SITE_URL, ROUTES, CURRENT_WEEK_ID, MAIN_METAIMAGE } from '../config';

import ContentItem from '../components/honda/contents/ContentItem';
import CustomTitle from '../components/honda/titles/CustomTitle';
import { bindUserVideosActions } from '../redux/actions/user-videos';
import HondaPopupModal from '../components/honda/modal/HondaPopupModal';
import { bindMeActions } from '../redux/actions/me';

const { TextArea, Search } = Input;
const { TabPane } = Tabs;

class Contents extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedTab: 'feed',
            isFetchedAll: false,
            isFetching: false,
            filteredVideos: [],
        };

        this.onSearch = this.onSearch.bind(this);
    }

    async onSearch(searchName) {
        if (this.state.isFetching) return;

        if (!_.isEmpty(searchName) && _.size(searchName) < 3) {
            message.info('Tên phải nhiều hơn 3 kí tự');
            return;
        }

        this.setState({ isFetching: true });

        const { userVideosActions } = this.props;
        if (_.isEmpty(searchName)) {
            userVideosActions.resetFetchedVideos();
            await userVideosActions.fetchVideos();
            this.setState({ isFetchedAll: false, isFetching: false });
            return;
        }

        userVideosActions.resetFetchedVideos();
        await userVideosActions.searchByName(searchName);
        this.setState({ isFetchedAll: true, isFetching: false });
    }

    componentDidUpdate(prevProps, prevState) {
        if (!_.isEqual(this.props.videos, prevProps.videos)) {
            this.filterVideos();
        }
    }

    filterVideos() {
        const { videos } = this.props;

        const filteredVideos = videos.reduce((result, video) => {
            const weekId = CURRENT_WEEK_ID(video.createdAt);
            const currentWeekData = result[weekId] || [];
            currentWeekData.push(video);
            result[weekId] = currentWeekData;

            return result;
        }, {});

        this.setState({ filteredVideos: _.values(filteredVideos).reverse() });
    }

    handleUserVote = video => {
        const { userVideosActions, me } = this.props;

        if (me.isLoggedIn === false) {
            this.setState({ visibleLoginModal: true });
            return;
        }

        if (_.includes(video.voted, me.uid)) {
            // TODO: hiện đã vote
            message.info('Bạn đã bình chọn');
            return;
        }

        if (CURRENT_WEEK_ID(moment().unix()) !== CURRENT_WEEK_ID(video.createdAt)) {
            // TODO: hiện đã vote
            message.info('Video đã quá hạn bình chọn');
            return;
        }

        userVideosActions.voteVideo(video.id).then(async () => {
            // await userVideosActions.fetchMostVotedVideos(CURRENT_WEEK_ID(moment().unix()));
        });
    };

    async componentDidMount() {
        const { videos, userVideosActions } = this.props;
        userVideosActions.resetFetchedVideos();
        await userVideosActions.fetchVideos();

        this.filterVideos();

        message.config({ maxCount: 1 });
    }

    handleLoadMore = async () => {
        if (this.state.isFetching) return;

        try {
            const { videos, userVideosActions } = this.props;

            this.setState({ isFetching: true });
            const data = await userVideosActions.fetchVideos(videos[videos.length - 1].id);

            this.setState({
                isFetching: false,
                ...(data.length === 0 && { isFetchedAll: true }),
            });
        } catch (error) {
            this.setState({ isFetching: false });
        }
    };

    onLoginFacebook = async () => {
        const { meActions } = this.props;

        try {
            const data = await meActions.loginWithFacebook();
            // console.log('FB login', data);

            this.setState({ locking: true });

            await meActions.loginFirebase();
            this.setState({ locking: false, visibleLoginModal: false });
        } catch (error) {
            console.log('[Index]', error);
            this.setState({ locking: false, visibleLoginModal: false });
        }
    };

    render() {
        const { me } = this.props;
        const { searchName, isFetchedAll, isFetching, filteredVideos } = this.state;

        return (
            <div style={{ overflowX: 'hidden' }}>
                <Head
                    title="Honda Wave RSX"
                    image={MAIN_METAIMAGE}
                    url={`${SITE_NAME}contents`}
                    description="Trải nghiệm xe Wave RSX cùng công nghệ thực tế ảo AR"
                />
                <MainLayout>
                    <div className="contents">
                        <div className="contentsBox">
                            <Row className="title">
                                <CustomTitle text="Tất cả các bài dự thi" />
                            </Row>
                            <Row className="search">
                                <Input.Search
                                    placeholder="Tìm kiếm theo tên người dự thi"
                                    onSearch={this.onSearch}
                                    allowClear
                                />
                            </Row>
                            <Row gutter={30}>
                                {filteredVideos.map(videos => {
                                    return (
                                        <>
                                            {videos.map(video => (
                                                <Col xxl={8} xl={8} lg={8} md={12} sm={12} xs={12} key={video.id}>
                                                    <ContentItem
                                                        voted={me.isLoggedIn && _.includes(video.voted, me.data.id)}
                                                        onVote={() => this.handleUserVote(video)}
                                                        video={video}
                                                    />
                                                </Col>
                                            ))}
                                            <Divider />
                                        </>
                                    );
                                })}
                            </Row>
                            {!isFetchedAll && (
                                <div className="loadMoreButton easeIn">
                                    <Row>
                                        <Button
                                            className={isFetching ? 'grayscale' : ''}
                                            // loading={isFetching}
                                            onClick={this.handleLoadMore}
                                        >
                                            <img width="200px" src="/static/main/shared/button-xem-them.svg" />
                                        </Button>
                                    </Row>
                                </div>
                            )}
                        </div>
                    </div>
                </MainLayout>

                {this.state.locking && (
                    <div className="locking">
                        <p>
                            Đang đăng nhập...
                            <br />
                            <Icon type="loading" />
                        </p>
                    </div>
                )}

                <HondaPopupModal
                    visible={this.state.visibleLoginModal}
                    onOk={this.closeLoginModal}
                    onCancel={this.closeLoginModal}
                    title="Đăng nhập"
                    classname="loginBox"
                    content={
                        <>
                            <Row type="flex" justify="center">
                                <Col span={24}>
                                    <p>Bạn phải đăng nhập trước khi bình chọn.</p>
                                </Col>
                                <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
                                    <div className="loginButton facebook" onClick={this.onLoginFacebook}>
                                        <div className="icon">
                                            <img src="/static/main/shared/fb-icon.png" />
                                        </div>
                                        <div className="text">
                                            Đăng nhập bằng <b>Facebook</b>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </>
                    }
                />
            </div>
        );
    }
}

export default connect(
    state => ({
        videos: state.userVideos.data,
        me: state.me,
    }),
    dispatch => channingActions({}, dispatch, bindUserVideosActions, bindMeActions),
)(Contents);
