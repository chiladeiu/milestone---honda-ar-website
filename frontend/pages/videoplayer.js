import React from 'react';
import { connect } from 'react-redux';
import { channingActions, getMediaURL } from '../lib/helper';
import { Button, Icon, Row, Col, Carousel, Input, Modal, Tabs, Radio, Affix } from 'antd';
import Link from 'next/link';
import { withRouter } from 'next/router';

import './videoplayer.less';

import MainLayout from '../layout/MainLayout';
import Head from '../components/forms/Head';
import BasicSearch from '../components/forms/main/inputs/BasicSearch';
import BasicTab from '../components/forms/main/selects/BasicTab';
import PrizeItem from '../components/honda/prizes/PrizeItem';
import VoteItem from '../components/honda/votes/VoteItem';
import ProcessItem from '../components/honda/process/ProcessItem';
import ProcessItemReverse from '../components/honda/process/ProcessItemReverse';
import SimpleFooter from '../components/forms/main/footers/SimpleFooter';

import { SITE_NAME, SITE_URL, ROUTES, MAIN_METAIMAGE } from '../config';

import ContentItem from '../components/honda/contents/ContentItem';
import CustomTitle from '../components/honda/titles/CustomTitle';

const { TextArea, Search } = Input;
const { TabPane } = Tabs;

class VideoPlayer extends React.Component {
    static async getInitialProps(ctx) {
        const { source } = ctx.query;

        return {
            source,
        };
    }

    componentDidMount() {
        const { source, router } = this.props;

        if (process.browser) {
            router.push(`${ROUTES.VIDEO_PLAYER}`, `${ROUTES.VIDEO_PLAYER}?source=${source}`, {
                shallow: true,
            });
        }
    }

    render() {
        const { source } = this.props;

        return (
            <div style={{ overflowX: 'hidden' }}>
                <Head
                    title="Honda Wave RSX"
                    image={MAIN_METAIMAGE}
                    url={SITE_URL}
                    description="Trải nghiệm xe Wave RSX cùng công nghệ thực tế ảo AR"
                />
                <div className="videoPlayer">
                    <div className="videoPlayerBox">
                        <video controls preload="auto">
                            <source src={source} type="video/mp4" />
                        </video>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(VideoPlayer);
