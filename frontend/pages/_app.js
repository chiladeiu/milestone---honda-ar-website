import App, { Container } from 'next/app';
import React from 'react';
import withReduxStore from '../lib/with-redux-store';
import { Provider } from 'react-redux';
import { initI18n } from '../I18n';
import { generateLanguageSet, setRootApp } from '../lang';
import { getToken } from '../lib/auth';
import { setToken, getMeInformation, updateFirebaseAuthState } from '../redux/actions/me';
import { loadfbSDK, facebookCheckLoginState, initFacebook, FacebookSDK } from '../lib/wrapper/facebook';
import { loadClientScript, loadScriptAsync } from '../lib/helper';
import { FirebaseSDK } from '../lib/wrapper/firebase';
// import { ZaloSDK } from '../lib/wrapper/zalo';
// import { ZALO_CONFIG } from '../config';

class MainApp extends App {
    static async getInitialProps({ Component, ctx }) {
        return {
            pageProps: Component.getInitialProps ? await Component.getInitialProps(ctx) : {},
        };
    }

    constructor() {
        super();
        this.state = {
            isReady: false,
        };

        if (process.browser) {
            loadClientScript(
                document,
                'script',
                'facebook-jssdk',
                'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.3',
            );
            // loadClientScript(document, 'script', 'zalo-jssdk', 'https://zjs.zdn.vn/zalo/sdk.js');
            // loadClientScript(document, 'script', 'zalo-plugins-jssdk', 'https://sp.zalo.me/plugins/sdk.js');
        }
    }

    componentDidMount() {
        FacebookSDK.initialize();
        FirebaseSDK.initialize();

        const { reduxStore } = this.props;
        FirebaseSDK.setAuthChangedCallback(user => reduxStore.dispatch(updateFirebaseAuthState(user)));

        initI18n().then(() => {
            setRootApp(this);
            this.setState({ isReady: true });
        });
    }

    applyLanguage() {
        this.setState({ isReady: false });
        setTimeout(() => this.setState({ isReady: true }), 100);
    }

    render() {
        if (process.browser && !this.state.isReady) {
            return null;
        }

        const { Component, pageProps, reduxStore } = this.props;
        return (
            <Container>
                <div id="fb-root"></div>
                <div
                    dangerouslySetInnerHTML={{
                        __html: `<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PHPCWK5"
                        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                        `,
                    }}
                />
                <Provider store={reduxStore}>
                    <Component {...pageProps} />
                </Provider>
            </Container>
        );
    }
}

export default withReduxStore(MainApp);
