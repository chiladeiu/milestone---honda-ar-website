import React from 'react';
import { connect } from 'react-redux';
import { channingActions, getMediaURL } from '../lib/helper';
import {
    Button,
    Icon,
    Row,
    Col,
    Carousel,
    Input,
    Modal,
    Tabs,
    Radio,
    Affix,
    BackTop,
    Form,
    Upload,
    message,
    Progress,
    Select,
    Alert,
    Checkbox,
} from 'antd';
import Link from 'next/link';
import _ from 'lodash';
import moment from 'moment';

import './index.less';

import MainLayout from '../layout/MainLayout';
import Head from '../components/forms/Head';
import BasicSearch from '../components/forms/main/inputs/BasicSearch';
import BasicTab from '../components/forms/main/selects/BasicTab';
import ContentItem from '../components/honda/contents/ContentItem';
import HondaTitle from '../components/honda/contents/HondaTitle';
import KolContentItem from '../components/honda/contents/KolContentItem';
import PrizeIntroItem from '../components/honda/prizes/PrizeIntroItem';
import PrizeItem from '../components/honda/prizes/PrizeItem';
import PrizeTextItem from '../components/honda/prizes/PrizeTextItem';
import HondaTitleDropdown from '../components/honda/inputs/HondaTitleDropdown';
import CarouselProject from '../components/forms/main/carousel/CarouselProjects';
import UpdateUserInfoModal from '../components/honda/modal/UpdateUserInfoModal';
import LabelSmall from '../components/honda/inputs/LabelSmall';
import LabelLarge from '../components/honda/inputs/LabelLarge';
import scrollToComponent from 'react-scroll-to-component-ssr';
import HondaPopupModal from '../components/honda/modal/HondaPopupModal';
import MisthyModal from '../components/honda/modal/MisthyModal';
import Media from 'react-media';

import RankTitle from '../components/honda/titles/RankTitle';
import CustomTitle from '../components/honda/titles/CustomTitle';
// import LoginTitle from '../components/honda/titles/LoginTitle';
// import KolsTitle from '../components/honda/titles/KolsTitle';
// import MediumTitle from '../components/honda/titles/MediumTitle';
// import AwardTitle from '../components/honda/titles/AwardTitle';

import {
    SITE_NAME,
    SITE_URL,
    ROUTES,
    PRIZE_TITLE,
    AR_URL,
    AR_URL_2,
    CURRENT_WEEK_ID,
    MISTHY_VIDEO,
    MAIN_METAIMAGE,
    IS_STDIO,
} from '../config';

import { UsersAPI } from '../api/user-videos';
import { MeAPI } from '../api/me';
import { AuthAPI } from '../api/auth';
import { bindMeActions } from '../redux/actions/me';
import { bindKOLVideosActions } from '../redux/actions/kol-videos';
import { bindUserVideosActions } from '../redux/actions/user-videos';
import { bindRewardsActions } from '../redux/actions/rewards';
import { ZaloSDK } from '../lib/wrapper/zalo';
import Router, { withRouter } from 'next/router';
import { hidden } from 'ansi-colors';
import { HEAD_DATA } from '../head-data';

const { TextArea, Search } = Input;
const { TabPane } = Tabs;

class Index extends React.Component {
    constructor(props) {
        super(props);

        this.bannerRef = React.createRef();
        this.prizesRef = React.createRef();
        this.prizeBorderTopRef = React.createRef();
        this.processRef = React.createRef();
        this.kolsRef = React.createRef();
        this.kolsBorderBottomRef = React.createRef();
        this.kolsBorderTopRef = React.createRef();
        this.awardRef = React.createRef();
        this.termRef = React.createRef();
        this.termBorderTopRef = React.createRef();
        this.termBorderBottomRef = React.createRef();
        this.loginRef = React.createRef();
        this.footerRef = React.createRef();

        this.state = {
            flowStage: 0, // 0: not login, 1: need update info, 2: upload video, 3: uploaded
            selectedTab: 'feed',
            loggedIn: false,

            prizePolygon: 'polygon(50% 0%, 100% 0, 100% 100%, 0 100%, 0 0%)',
            footerPolygon: 'polygon(50.1% 0, 100% 61.9%, 100% 100%, 0 100%, 0 59.5%)',

            visibleModalRequestData: false,
            visibleCompleteUpdateInfo: false,
            visibleMisthyModal: false,
            isVisibleUploadModal: false,
            isVisibleChooseColorModal: false,
            isVisibleGuideModal: false,

            visibleLoginModal: false,
            visibleUpdateInfoModal: false,

            backTop: 'none',

            isAlreadyJoinedContest: false,

            selectedFile: null,
            uploading: false,

            isFirstLoad: true,

            // For updating user profile
            name: '',
            phone: '',
            email: '',
            isInvalidData: false,

            intervalCounter: 0,
            uploadedVideo: '',
            processProgress: 0,
            videoTitle: '',

            locking: false,

            enableSelectHead: false,
            selectedProvince: '',
            selectedHEAD: '',
        };

        this.onLoginFacebook = this.onLoginFacebook.bind(this);
        this.onLoginZalo = this.onLoginZalo.bind(this);
    }

    componentDidMount() {
        this.setState({ isFirstLoad: false });

        this.updateBorderPolygon();
        this.intervalCounter = 0;

        this.updateInterval = setInterval(() => {
            const { intervalCounter } = this.state;
            this.updateBorderPolygon();

            if (intervalCounter === 5) {
                clearInterval(this.updateInterval);
            }

            this.setState({ intervalCounter: intervalCounter + 1 });
        }, 1000);

        window.addEventListener('resize', this.updateBorderPolygon);

        window.addEventListener('scroll', this.getWindowsSize);

        const { kolVideosActions, userVideosActions, me } = this.props;
        kolVideosActions.fetchVideos();

        userVideosActions.resetFetchedVideos();
        userVideosActions.fetchMostVotedVideos(CURRENT_WEEK_ID(moment().unix()));

        message.config({ maxCount: 1 });

        if (me.isLoggedIn) {
            this.setState({ flowStage: 1 });
        }
    }

    showLoginModal = () => {
        this.setState({
            visibleLoginModal: true,
        });
    };

    closeLoginModal = () => {
        this.setState({
            visibleLoginModal: false,
        });
    };

    // flowStage
    // 0 -> login
    // 1 -> join
    // 2 -> request info
    // 3 -> upload
    // verifyUserData() {
    //     const { me } = this.props;
    //     const { flowStage } = this.state;
    //     const { email, name, phone } = me.data;

    //     if (me.isLoggedIn !== true) {
    //         this.setState({ flowStage: 0 });
    //         console.log('Not logged in');
    //     } else {
    //         this.setState({ flowStage: 1 });
    //     }
    // }

    componentDidUpdate(prevProps, prevState) {
        const { me } = this.props;

        if (me.isLoggedIn !== prevProps.me.isLoggedIn) {
            if (me.isLoggedIn) this.setState({ flowStage: 1 });
            else this.setState({ flowStage: 0 });
        }
    }

    componentWillUnmount() {
        clearInterval(this.updateInterval);
        window.removeEventListener('resize', this.updateBorderPolygon);
    }

    scrollToTop = () => {
        scrollToComponent(this.bannerRef, { offset: 0, align: 'top' });
    };

    async onLoginFacebook() {
        const { meActions } = this.props;

        try {
            const data = await meActions.loginWithFacebook();
            // console.log('FB login', data);

            this.setState({ locking: true });

            await meActions.loginFirebase();
            this.setState({ locking: false, visibleLoginModal: false });
        } catch (error) {
            console.log('[Index]', error);
            this.setState({ locking: false, visibleLoginModal: false });
        }
    }

    showGuideModal = () => {
        this.setState({
            isVisibleGuideModal: true,
        });
    };

    closeGuideModal = () => {
        this.setState({
            isVisibleGuideModal: false,
        });
    };

    showChooseColorModal = () => {
        this.setState({
            isVisibleChooseColorModal: true,
        });
    };

    closeChooseColorModal = () => {
        this.setState({
            isVisibleChooseColorModal: false,
        });
    };

    async onLoginZalo() {
        const { meActions } = this.props;
        ZaloSDK.login();
    }

    onUpdateUserProfile = async (usingModal = false) => {
        const { email, name, phone } = this.state;
        const { meActions } = this.props;

        if (_.isEmpty(email.trim()) || _.isEmpty(name.trim()) || _.isEmpty(phone.trim())) {
            this.setState({ isInvalidData: true });
            return;
        }

        await meActions.updateProfile({ email, name, phone });

        if (usingModal) {
            this.setState({ isInvalidData: false, visibleUpdateInfoModal: false });
        } else {
            this.setState({ isInvalidData: false, visibleCompleteUpdateInfo: true, flowStage: 4 });
        }
    };

    verifyUserProfile = (usingModal = false) => {
        const { me } = this.props;
        console.log(me.data);
        const { email, name, phone } = me.data;

        if (_.isEmpty(email) || _.isEmpty(name) || _.isEmpty(phone)) {
            if (!usingModal) this.setState({ flowStage: 3 });
            return false;
        }

        return true;
    };

    onFileChangeHandler = event => {
        console.log(event.target.files[0]);
        this.setState({ selectedFile: event.target.files[0] });
    };

    handleUpload = async () => {
        const { meActions } = this.props;
        const { selectedFile, videoTitle, selectedProvince, selectedHEAD } = this.state;

        if (_.isEmpty(videoTitle.trim())) {
            message.error('Tiêu đề phải có ít nhất 1 kí tự');
            return;
        }

        if (!_.isEmpty(selectedProvince) && _.isEmpty(selectedHEAD)) {
            message.error('HEAD không được để trống');
            return;
        }

        const isLt40M = selectedFile.size / 1024 / 1024 < 40;
        if (!isLt40M) {
            message.error('Kích thước file phải nhỏ hơn 40MB');
            return;
        }

        this.setState({ uploading: true });

        // Reset progress bar
        meActions.uploadVideoProgressChange(0);

        await meActions.updateProfile({ videoTitle, headCode: selectedHEAD });
        await meActions.uploadVideo(selectedFile);

        const recheckProfile = async () => {
            await meActions.getProfile();
            const { isProcessingVideo, mostRecentVideo, processingStep } = this.props.me.data;

            console.log('isProcessingVideo', isProcessingVideo);
            console.log('processingStep', processingStep);

            if (!_.isUndefined(isProcessingVideo) && !isProcessingVideo) {
                console.log('Process done');
                this.setState(
                    {
                        // uploading: false,
                        isVisibleUploadModal: true,
                        mostRecentVideo,
                        processProgress: 80,
                    },
                    () => {
                        console.log(this.props.me.data.joinWeeklyContest, this.state.flowStage);
                    },
                );
            } else {
                this.setState({ processProgress: Math.round((processingStep / 8) * 100 * 0.8) });
                setTimeout(recheckProfile, 2000);
            }
        };

        setTimeout(recheckProfile, 2000);
    };

    handleKOLVote = async videoId => {
        console.log('[KOL]', videoId);

        const { me, kolVideosActions, meActions } = this.props;
        if (!me.isLoggedIn) {
            // TODO: Phú scroll tới phần đăng nhập
            // scrollToComponent(this.loginRef, { offset: 0 });
            // message.warning('Bạn phải đăng nhập trước.');

            this.setState({ visibleLoginModal: true });
            return;
        }

        if (!this.verifyUserProfile(true)) {
            console.log('Verify failed');
            this.setState({ visibleUpdateInfoModal: true });
            return;
        }

        try {
            await kolVideosActions.voteVideo(videoId);
            await meActions.getProfile();
        } catch (error) {
            message.warning('Bạn đã bình chọn.');
        }
    };

    handleUserVote = video => {
        const { userVideosActions, me } = this.props;

        if (me.isLoggedIn === false) {
            this.setState({ visibleLoginModal: true });
            return;
        }

        if (_.includes(video.voted, me.uid)) {
            // TODO: hiện đã vote
            message.info('Bạn đã bình chọn');
            return;
        }

        if (CURRENT_WEEK_ID(moment().unix()) !== CURRENT_WEEK_ID(video.createdAt)) {
            // TODO: hiện đã vote
            message.info('Video đã quá hạn bình chọn');
            return;
        }

        userVideosActions.voteVideo(video.id).then(async () => {
            // await userVideosActions.fetchMostVotedVideos(CURRENT_WEEK_ID(moment().unix()));
        });
    };

    closeCompleteUpdateInfoNotification = () => {
        this.setState({
            visibleCompleteUpdateInfo: false,
        });
    };

    closeUploadModal = e => {
        this.setState(
            {
                isVisibleUploadModal: false,
            },
            () => {
                let videoUrl = `${SITE_URL}content?videoId=${this.props.me.data.mostRecentVideo}`;
                Router.push(videoUrl);
            },
        );
    };

    showMisthyModal = () => {
        this.setState({ visibleMisthyModal: true });
    };

    closeMisthyModal = () => {
        this.setState({ visibleMisthyModal: false });
    };

    getWindowsSize = () => {
        if (window.pageYOffset > 200) {
            this.setState({
                backTop: 'flex',
            });
        } else {
            this.setState({
                backTop: 'none',
            });
        }
    };

    updateBorderPolygon = () => {
        let prizesRect = this.prizesRef.getBoundingClientRect();
        let prizesBorderTopRect = this.prizeBorderTopRef.getBoundingClientRect();
        let prizesHeightRatio = (prizesBorderTopRect.height / prizesRect.height) * 100;

        let footerRect = this.footerRef.getBoundingClientRect();
        let footerBorderBottomRect = this.prizeBorderTopRef.getBoundingClientRect();
        let footerHeightRatio = (footerBorderBottomRect.height / footerRect.height) * 100;

        this.setState({
            prizePolygon: `polygon(49.2% ${prizesHeightRatio * 0.71}%, 100% 0.1%, 100% ${100 -
                0.25 * prizesHeightRatio}%, 0 100%, 0 ${prizesHeightRatio * 0.378}%)`,
            footerPolygon: `polygon(50.1% 0, 100% ${footerHeightRatio *
                0.6446}%, 100% 100%, 0 100%, 0 ${footerHeightRatio * 0.6193}%)`,
        });
    };

    _renderLogInForm = () => {
        const { isLoggedIn } = this.props.me;

        // if (isLoggedIn) return null;

        return (
            <div className="loginForm">
                <div className="title">
                    <CustomTitle text="Đăng nhập" />
                </div>

                <Row type="flex" justify="center">
                    <Col xxl={10} xl={10} lg={10} md={10} sm={12} xs={24}>
                        <div className="loginButton facebook" onClick={this.onLoginFacebook}>
                            <div className="icon">
                                <img src="/static/main/shared/fb-icon.png" />
                            </div>
                            <div className="text">
                                Đăng nhập bằng <b>Facebook</b>
                            </div>
                        </div>
                    </Col>

                    {/* <Col xxl={10} xl={10} lg={10} md={10} sm={12} xs={24}>
                        <div className="loginButton zalo" onClick={this.onLoginZalo}>
                            <div className="icon">
                                <img src="/static/main/pc/zl-icon.svg" />
                            </div>
                            <div className="text">
                                Đăng nhập bằng <b>Zalo</b>
                            </div>
                        </div>
                    </Col> */}
                </Row>
            </div>
        );
    };

    _renderUpdateInfoForm = () => {
        const { isLoggedIn, data } = this.props.me;

        // if (!isLoggedIn || !(_.isEmpty(data.email) || _.isEmpty(data.name) || _.isEmpty(data.phone))) return null;
        const { email, name, phone, isInvalidData } = this.state;

        return (
            <div className="updateInfoForm">
                <div className="title">
                    <CustomTitle text="Cập nhật thông tin" />
                </div>
                <div className="infoFields">
                    <Row>
                        <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={10} style={{ margin: '0.5rem 0' }}>
                            <p>Họ & Tên</p>
                        </Col>
                        <Col xxl={18} xl={18} lg={18} md={18} sm={18} xs={14} style={{ margin: '0.5rem 0' }}>
                            <Input onChange={e => this.setState({ name: e.target.value })} />
                        </Col>

                        <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={10} style={{ margin: '0.5rem 0' }}>
                            <p>Số Điện Thoại</p>
                        </Col>
                        <Col xxl={18} xl={18} lg={18} md={18} sm={18} xs={14} style={{ margin: '0.5rem 0' }}>
                            <Input onChange={e => this.setState({ phone: e.target.value })} />
                        </Col>

                        <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={10} style={{ margin: '0.5rem 0' }}>
                            <p>Email</p>
                        </Col>
                        <Col xxl={18} xl={18} lg={18} md={18} sm={18} xs={14} style={{ margin: '0.5rem 0' }}>
                            <Input onChange={e => this.setState({ email: e.target.value })} />
                        </Col>

                        <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={10} style={{ margin: '0.5rem 0' }}></Col>
                        <Col
                            xxl={18}
                            xl={18}
                            lg={18}
                            md={18}
                            sm={18}
                            xs={14}
                            style={{ margin: '0.5rem 0', color: '#fff' }}
                        >
                            <i style={{ color: 'red' }}>
                                *Vui lòng điền đầy đủ và chính xác mọi thông tin để nhãn hàng liên hệ trong trường hợp
                                bạn giành được giải thưởng cuộc thi.
                            </i>
                            <br />
                            {isInvalidData && <i style={{ color: 'red' }}>*Tất cả các thông tin là bắt buộc</i>}
                        </Col>
                    </Row>
                </div>
                <a onClick={() => this.onUpdateUserProfile()}>
                    <img className="easeIn agreeUpdate" src="/static/main/shared/hoan-tat-dang-ky.svg" />
                </a>
            </div>
        );
    };

    _renderUploadVideo = () => {
        const { isLoggedIn, data, isProcessingVideo } = this.props.me;

        const { selectedFile, uploading, processProgress, videoTitle, selectedProvince, selectedHEAD } = this.state;
        const { uploadProgress } = this.props.me;

        const combineProgress = uploadProgress * 0.2 + processProgress;

        return (
            <div className="uploadVideoForm">
                <div className="title">
                    <CustomTitle text="Đăng tải video dự thi" />
                </div>

                <div className="uploadTray">
                    {/* <div className="checkIcon">
                        <Icon theme="filled" type="check-circle" />
                    </div> */}

                    {uploading ? (
                        <Progress
                            strokeColor={{
                                from: '#f00',
                                to: '#f00',
                            }}
                            percent={Math.round(combineProgress)}
                        />
                    ) : (
                            <>
                                <Row style={{ width: '100%' }}>
                                    <Col xxl={5} xl={5} lg={6} md={12} sm={12} xs={10}></Col>
                                    <Col xxl={19} xl={19} lg={18} md={12} sm={12} xs={14} style={{ paddingBottom: '10px' }}>
                                        <span style={{ color: 'red' }}>
                                            Video dự thi hợp lệ phải có người tham dự và xe Honda Wave RSX trong khung hình.
                                            Ban Tổ Chức sẽ rà soát và loại các video dự thi không hợp lệ.
                                    </span>
                                    </Col>

                                    <Col xxl={5} xl={5} lg={6} md={12} sm={12} xs={10}>
                                        <b>Đăng tải video</b>
                                    </Col>
                                    <Col xxl={19} xl={19} lg={18} md={12} sm={12} xs={14}>
                                        <label htmlFor="file">
                                            <div className="uploadInputFieldUpload">
                                                <p>{selectedFile ? selectedFile.name : null}</p>
                                                <div className="button">...</div>
                                            </div>
                                        </label>
                                        <input
                                            id="file"
                                            type="file"
                                            name="file"
                                            accept="video/*"
                                            onChange={this.onFileChangeHandler}
                                            style={{ visibility: 'hidden' }}
                                        />
                                    </Col>

                                    <Col xxl={5} xl={5} lg={6} md={12} sm={12} xs={10}>
                                        <b>Tựa đề video</b>
                                    </Col>
                                    <Col xxl={19} xl={19} lg={18} md={12} sm={12} xs={14}>
                                        <Input
                                            className="uploadInputFieldTitle"
                                            value={videoTitle}
                                            onChange={e => this.setState({ videoTitle: e.target.value })}
                                        />
                                    </Col>

                                    <Col xxl={5} xl={5} lg={6} md={12} sm={12} xs={10} style={{ paddingTop: '30px' }}>
                                        {/* <Checkbox
                                        className="checkboxSelectHEAD"
                                        defaultValue={this.state.enableSelectHead}
                                        onChange={e => {
                                            this.setState({ enableSelectHead: e.target.checked });

                                            if (!e.target.checked) {
                                                this.setState({ selectedProvince: '', selectedHEAD: '' });
                                            } else {
                                                this.setState({
                                                    selectedProvince: HEAD_DATA[0].province,
                                                    selectedHEAD: HEAD_DATA[0].headCode,
                                                });
                                            }
                                        }}
                                    >
                                        <span style={{ color: '#fff' }}>Chọn HEAD</span>
                                    </Checkbox> */}
                                        <Select
                                            className={`selectHeadEnable ${this.state.enableSelectHead ? 'active' : ''}`}
                                            value={this.state.enableSelectHead}
                                            onChange={value => {
                                                this.setState({ enableSelectHead: value });

                                                if (!value) {
                                                    this.setState({ selectedProvince: '', selectedHEAD: '' });
                                                } else {
                                                    this.setState({
                                                        selectedProvince: HEAD_DATA[0].province,
                                                        selectedHEAD: HEAD_DATA[0].headCode,
                                                    });
                                                }
                                            }}
                                        >
                                            <Select.Option value={false}>no HEAD</Select.Option>
                                            <Select.Option value={true}>Chọn HEAD</Select.Option>
                                        </Select>
                                    </Col>

                                    <Col xxl={19} xl={19} lg={18} md={12} sm={12} xs={14} style={{ paddingTop: '30px' }}>
                                        <Col xxl={10} xl={10} lg={10} md={10} sm={24} xs={24} style={{ marginBottom: '10px' }}>
                                            <Select
                                                className={`selectHeadTown ${this.state.enableSelectHead ? 'active' : ''}`}
                                                value={selectedProvince}
                                                onSelect={selectedProvince =>
                                                    this.setState({ selectedProvince, selectedHEAD: '' })
                                                }
                                                disabled={!this.state.enableSelectHead}
                                            >
                                                {_.uniq(HEAD_DATA.map(h => h.province)).map(province => (
                                                    <Select.Option value={province}>{province}</Select.Option>
                                                ))}
                                            </Select>
                                        </Col>
                                        <Col xxl={1} xl={1} lg={1} md={1} sm={0} xs={0} style={{ marginBottom: '10px' }}></Col>
                                        <Col xxl={10} xl={10} lg={10} md={10} sm={24} xs={24} style={{ marginBottom: '10px' }}>
                                            <Select
                                                className={`selectHeadHead ${this.state.enableSelectHead ? 'active' : ''}`}
                                                value={selectedHEAD}
                                                onSelect={selectedHEAD => this.setState({ selectedHEAD })}
                                                disabled={!this.state.enableSelectHead}
                                            >
                                                {HEAD_DATA.filter(h => h.province === selectedProvince).map(head => (
                                                    <Select.Option value={head.headCode}>{head.headName}</Select.Option>
                                                ))}
                                            </Select>
                                        </Col>
                                    </Col>
                                </Row>

                                <Row className="buttonsField">
                                    <Col xxl={5} xl={5} lg={6} md={0} sm={0} xs={0}></Col>
                                    <Col xxl={9} xl={9} lg={9} md={12} sm={12} xs={12}>
                                        <div className="back">
                                            <Button onClick={() => this.setState({ flowStage: 1 })}>
                                                <img width="99%" src="/static/main/shared/button-quay-lai.svg" />
                                            </Button>
                                        </div>
                                    </Col>

                                    <Col xxl={9} xl={9} lg={9} md={12} sm={12} xs={12}>
                                        <div className="agree">
                                            <Button
                                                type="primary"
                                                onClick={this.handleUpload}
                                                disabled={_.isNull(selectedFile)}
                                                loading={uploading}
                                                style={{ marginTop: 16 }}
                                            >
                                                <img width="99%" src="/static/main/shared/button-dong-y.svg" />
                                            </Button>
                                        </div>
                                    </Col>
                                </Row>
                            </>
                        )}
                </div>
            </div>
        );
    };

    _renderJoinNowForm = () => {
        const { joinWeeklyContest = true } = this.props.me.data;

        return (
            <div className="joinNowForm">
                <div className="title">
                    <CustomTitle text="Tham gia cuộc thi ngay" />
                </div>
                <div className="buttonsField">
                    <Row type="flex" justify="center">
                        <Col xxl={6} xl={6} lg={6} md={6} sm={8} xs={24}>
                            <a>
                                <div>
                                    <img
                                        onClick={this.showChooseColorModal}
                                        className="easeIn"
                                        src="/static/main/shared/button-tao-video.svg"
                                    />
                                </div>
                            </a>
                        </Col>
                        <Col xxl={6} xl={6} lg={6} md={6} sm={8} xs={24}>
                            <div
                                onClick={() =>
                                    message.warning('Đã hết thời gian tham gia cuộc thi.')
                                }
                            >
                                <img
                                    style={
                                        { WebkitFilter: 'grayscale(100%)', filter: 'grayscale(100%' }
                                    }
                                    src="/static/main/shared/button-dang-tai-video.svg"
                                />
                            </div>
                        </Col>
                        <Col span={24}>
                            {!joinWeeklyContest && (
                                <p style={{ color: '#f00', textAlign: 'center' }}>
                                    Đã hết thời gian tham gia cuộc thi.
                                </p>
                            )}
                        </Col>
                    </Row>
                </div>
            </div>
        );
        // } else {
        //     return (
        //         <div className="joinNowForm">
        //             <div className="title">
        //                 <HondaTitle size={3} title="Cảm ơn bạn đã tham gia tuần này" />
        //             </div>
        //         </div>
        //     );
        // }
    };

    render() {
        const { isFirstLoad, flowStage } = this.state;
        const {
            bundles,
            me,
            form,
            kolVideos,
            userVideos,
            kolRewards,
            userRewards,
            router,
            userVideosActions,
        } = this.props;

        return (
            <div style={{ overflowX: 'hidden' }}>
                {IS_STDIO && <Alert message="Cảnh báo" description="Sử dụng cấu hình STDIO" type="warning" showIcon />}
                <Head
                    title="Honda Wave RSX"
                    image={MAIN_METAIMAGE}
                    url={SITE_URL}
                    description="Trải nghiệm xe Wave RSX cùng công nghệ thực tế ảo AR"
                />
                <MainLayout
                    refs={{
                        prizesRef: this.prizesRef,
                        processRef: this.processRef,
                        loginRef: this.loginRef,
                    }}
                    isAlreadyJoinedContest={this.state.isAlreadyJoinedContest}
                >
                    <div className="index">
                        <div className="indexBox">
                            <div onClick={this.scrollToTop} style={{ display: this.state.backTop }} className="backTop">
                                <img src="/static/main/shared/arrow.png" />
                            </div>

                            <section className="banner" ref={ref => (this.bannerRef = ref)}>
                                <div className="bannerCanvas">
                                    <div className="text">
                                        <div className="textWrapper">
                                            <p>Trải nghiệm xe RSX</p>
                                        </div>
                                        <div className="textWrapper">
                                            <p>cùng công nghệ thực tế ảo AR</p>
                                        </div>
                                    </div>
                                    <img src="/static/main/pc/bike.png" className="bike" />
                                    <img src="/static/main/shared/title.svg" className="title" />
                                    <img src="/static/main/pc/slogan.png" className="slogan" />
                                    <div className="durationText">Từ 10/09/2019 đến 22/10/2019</div>
                                    <img src="/static/main/shared/logoRsx.png" className="logoRsx" />
                                    <a className="bannerButton termButton" onClick={this.showChooseColorModal}>
                                        <img className="easeIn" src="/static/main/shared/button-tao-video.svg" />
                                    </a>

                                    {/* <a
                                        className="bannerButton termButton"
                                        onClick={() => scrollToComponent(this.termRef, { offset: 0 })}
                                    >
                                        <img className="easeIn" src="/static/main/shared/button-dieu-le.svg" />
                                    </a>
                                    <a
                                        className="bannerButton joinButton"
                                        onClick={() =>
                                            me.data.joinWeeklyContest === true
                                                ? scrollToComponent(this.loginRef, { offset: 0 })
                                                : me.data.joinWeeklyContest === undefined
                                                ? scrollToComponent(this.loginRef, { offset: 0 })
                                                : message.warning('Bạn đã tham gia tuần này.')
                                        }
                                    >
                                        <img
                                            className={this.state.isAlreadyJoinedContest ? 'grayscale' : 'easeIn'}
                                            src="/static/main/shared/button-tham-gia-ngay.svg"
                                        />
                                    </a> */}
                                </div>
                            </section>

                            <section
                                className="prizes"
                                ref={ref => (this.prizesRef = ref)}
                                style={{
                                    clipPath: this.state.prizePolygon,
                                    WebkitClipPath: this.state.prizePolygon,
                                }}
                            >
                                <div className="prizesBackground" />
                                <div className="borderTop" ref={ref => (this.prizeBorderTopRef = ref)}></div>
                                <div className="prizesBox">
                                    <div className="title">
                                        <CustomTitle text="Giải thưởng cực chất" />
                                    </div>

                                    <div className="weeklyPrize">
                                        <h3 className="subTitle">Giải thưởng tuần</h3>
                                        <Row gutter={20} type="flex" justify="center">
                                            <Col xxl={8} xl={8} lg={8} md={8} sm={14} xs={18}>
                                                <PrizeIntroItem thumbnail="/static/main/shared/prizes-camera.png" />
                                                <div className="prizeName">
                                                    <RankTitle rank={1} />
                                                    <p style={{ color: '#fff' }}>Máy ảnh Sony DSC - HX90V</p>
                                                </div>
                                            </Col>
                                            <Col xxl={8} xl={8} lg={8} md={8} sm={14} xs={18}>
                                                <PrizeIntroItem thumbnail="/static/main/shared/prizes-earsphone.png" />
                                                <div className="prizeName">
                                                    <RankTitle rank={2} />
                                                    <p style={{ color: '#fff' }}>Tai nghe Bluetooth Sony WI-SP500</p>
                                                </div>
                                            </Col>
                                            <Col xxl={8} xl={8} lg={8} md={8} sm={14} xs={18}>
                                                <PrizeIntroItem thumbnail="/static/main/shared/prizes-card.png" />
                                                <div className="prizeName">
                                                    <RankTitle rank={3} />
                                                    <p style={{ color: '#fff' }}>
                                                        Thẻ cào điện thoại trị giá 500.000 VND
                                                    </p>
                                                </div>
                                            </Col>
                                        </Row>
                                    </div>

                                    <div className="bestPrize">
                                        <h3 className="subTitle">Giải chung cuộc</h3>
                                        <div className="bestPrizeItem">
                                            <div className="bestPrizeItemBox" />
                                            <div className="name">01 xe Honda Wave RSX 2019</div>
                                        </div>
                                    </div>
                                </div>

                                <div className="borderBottom" />
                            </section>

                            <section className="process" ref={ref => (this.processRef = ref)}>
                                <div className="borderTop" />
                                <div className="processBackground" />
                                <div className="processBox">
                                    <div className="title">
                                        <Media query="(max-width: 500px)">
                                            {matches =>
                                                matches ? (
                                                    <p>
                                                        SÁNG TẠO VIDEO theo
                                                        <br /> chất riêng CỦA BẠN
                                                    </p>
                                                ) : (
                                                        <p>SÁNG TẠO VIDEO THEO chất riêng CỦA BẠN</p>
                                                    )
                                            }
                                        </Media>
                                        <CustomTitle text="& tham gia ngay" />
                                    </div>
                                    <div className="processCanvas">
                                        <div className="line" />

                                        <div className="stepThumbnail stepThumbnail1" />
                                        <div className="stepDot stepDot1" />
                                        <div className="stepText stepText1">
                                            <h3>ĐĂNG KÝ TÀI KHOẢN DỰ THI</h3>
                                            {/* <p>Đăng ký tài khoản bằng Facebook hoặc Zalo.</p> */}
                                            <p>Đăng ký tài khoản bằng Facebook.</p>
                                        </div>

                                        <div className="stepThumbnail stepThumbnail2" />
                                        <div className="stepDot stepDot2" />
                                        <div className="stepText stepText2">
                                            <h3>TẠO VIDEO CỦA BẠN CÙNG CÔNG NGHỆ THỰC TẾ ẢO</h3>
                                            <p>
                                                Sử dụng thiết bị có hệ điều hành iOS 12.0 trở lên và Android 7.0 trở
                                                lên.
                                                <br />
                                                <img
                                                    src="/static/main/shared/button-xem-huong-dan.svg"
                                                    onClick={this.showGuideModal}
                                                    style={{ width: '49%', cursor: 'pointer' }}
                                                />
                                                <img
                                                    src="/static/main/shared/button-xem-video.svg"
                                                    onClick={this.showMisthyModal}
                                                    style={{ width: '49%', cursor: 'pointer' }}
                                                    onClick={this.showMisthyModal}
                                                />
                                            </p>
                                            <img
                                                src="/static/main/shared/misthy.jpg"
                                                style={{ cursor: 'pointer' }}
                                                onClick={this.showMisthyModal}
                                            />
                                            <MisthyModal
                                                visible={this.state.visibleMisthyModal}
                                                onOk={this.showMisthyModal}
                                                onCancel={this.closeMisthyModal}
                                                source={MISTHY_VIDEO}
                                            />
                                            <Modal
                                                visible={this.state.isVisibleGuideModal}
                                                onCancel={this.closeGuideModal}
                                                onOk={this.closeGuideModal}
                                                footer={null}
                                                title={'HƯỚNG DẪN CHI TIẾT'}
                                                centered
                                            >
                                                <p>
                                                    1. Nhấn vào nút Tạo video
                                                    <br />
                                                    2. Trình duyệt chuyển sang trang đăng nhập Facebook và mở ứng dụng
                                                    Facebook Camera.
                                                    <br />
                                                    3. Chọn chế độ Quay phim hoặc Boomerang (lưu ý: Video hợp lệ là
                                                    video được quay có hai yếu tố: hình ảnh của bạn và hình ảnh xe RSX
                                                    thực tế ảo AR).
                                                    <br />
                                                    4. Quay phim và chỉnh sửa bằng hiệu ứng có sẵn của nền tảng
                                                    Facebook.
                                                    <br />
                                                    5. Lưu video bạn đã tạo về máy (bạn có thể biên tập, chỉnh sửa thêm
                                                    hiệu ứng bằng ứng dụng chỉnh sửa video để tạo video ưng ý).
                                                    <br />
                                                    6. Chọn video đẹp nhất để post lên trang microsite của chương trình
                                                    (mỗi người chơi chỉ được đăng 1 video trong 1 tuần).
                                                    <br />
                                                    7. Share về trang facebook cá nhân kêu gọi bình chọn (mỗi tài khoản
                                                    đăng nhập website chỉ được bình chọn một lần và được chia sẻ không
                                                    giới hạn cho 01 bài dự thi).
                                                </p>
                                            </Modal>
                                        </div>

                                        <div className="stepThumbnail stepThumbnail3" />
                                        <div className="stepDot stepDot3" />
                                        <div className="stepText stepText3">
                                            <h3>ĐĂNG TẢI BÀI DỰ THI Chia sẻ & kêu gọi bình chọn</h3>
                                            <p>
                                                Chia sẻ video của bạn về trang cá nhân cùng hashtag: #HondaWaveRSX
                                                #RucsuctreXungdamme #RisingXpression. Đừng quên kêu gọi bạn bè bình chọn
                                                cho video của bạn trên website của chương trình.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="borderBottom"></div>
                            </section>

                            <section
                                className="kols"
                                ref={ref => {
                                    this.kolsRef = ref;
                                }}
                            >
                                <div
                                    className="borderTop"
                                    ref={ref => {
                                        this.kolsBorderTopRef = ref;
                                    }}
                                />
                                <div className="kolsBackground" />
                                <div className="kolsBox">
                                    <div className="title">
                                        <p style={{ textAlign: 'center' }}>
                                            <Media query="(max-width: 767.98px)">
                                                {matches =>
                                                    matches ? (
                                                        <>
                                                            Bạn yêu thích
                                                            <br /> phong cách nào nhất?
                                                        </>
                                                    ) : (
                                                            <>Bạn yêu thích phong cách nào nhất?</>
                                                        )
                                                }
                                            </Media>
                                        </p>
                                        <CustomTitle
                                            text={
                                                <Media query="(max-width: 767.98px)">
                                                    {matches =>
                                                        matches ? (
                                                            <>
                                                                Bình chọn &<br /> Trúng thưởng hàng tuần
                                                            </>
                                                        ) : (
                                                                <>Bình chọn & Trúng thưởng hàng tuần</>
                                                            )
                                                    }
                                                </Media>
                                            }
                                        />
                                        <p style={{ margin: '1rem 0' }}>4 thẻ cào 200.000 VND mỗi tuần</p>
                                    </div>
                                    <Row style={{ padding: '0 20px 30px 0' }} gutter={50}>
                                        {!_.isEmpty(kolVideos) &&
                                            kolVideos.map((video, index) => (
                                                <>
                                                    <Col
                                                        xxl={12}
                                                        xl={12}
                                                        lg={12}
                                                        md={12}
                                                        sm={12}
                                                        xs={24}
                                                        key={video.id}
                                                    >
                                                        <KolContentItem
                                                            thumbnail={getMediaURL(video.thumbnail)}
                                                            rank={index + 1}
                                                            link={getMediaURL(video.path)}
                                                            voteCount={video.voteCount}
                                                            voted={
                                                                me.isLoggedIn && _.includes(me.data.kolVoted, video.id)
                                                            }
                                                            onVote={() => this.handleKOLVote(video.id)}
                                                        />
                                                    </Col>
                                                </>
                                            ))}
                                    </Row>
                                    <>
                                        <Row>
                                            <HondaTitleDropdown
                                                text="Danh sách trúng thưởng"
                                                textExtra="GIẢI BÌNH CHỌN VIDEO"
                                                onFilterChange={weekId => {
                                                    this.props.rewardsActions.fetchKOLRewards(weekId);
                                                }}
                                                skipCurrentWeek={false}
                                            />
                                        </Row>
                                        <Row gutter={10}>
                                            {!_.isEmpty(kolRewards) &&
                                                kolRewards.map(user => (
                                                    <Col
                                                        key={user.id}
                                                        xxl={6}
                                                        xl={6}
                                                        lg={6}
                                                        md={12}
                                                        sm={12}
                                                        xs={12}
                                                    >
                                                        <LabelSmall
                                                            prizeRank="Bình Chọn"
                                                            name={user.name}
                                                            textExtra="THẺ CÀO 200.000 VND"
                                                        />
                                                    </Col>
                                                ))}
                                        </Row>
                                    </>
                                </div>

                                <div
                                    className="borderBottom"
                                    ref={ref => {
                                        this.kolsBorderBottomRef = ref;
                                    }}
                                />
                            </section>

                            <section className="award" ref={ref => (this.awardRef = ref)}>
                                <div className="awardBackground" />
                                <div className="awardBox">
                                    <div className="title">
                                        <CustomTitle
                                            text={
                                                <Media query="(max-width: 500px)">
                                                    {matches =>
                                                        matches ? (
                                                            <>
                                                                Video dự thi
                                                                <br /> được yêu thích nhất
                                                            </>
                                                        ) : (
                                                                <>Video dự thi được yêu thích nhất</>
                                                            )
                                                    }
                                                </Media>
                                            }
                                        />
                                    </div>
                                    <Row>
                                        <HondaTitleDropdown
                                            text=""
                                            onFilterChange={weekId => {
                                                userVideosActions.fetchMostVotedVideos(weekId);
                                            }}
                                            skipCurrentWeek={false}
                                        />
                                    </Row>
                                    {/* <Row gutter={30}>
                                        <CarouselProject
                                            background="../static/main/pc/bg-award.jpg"
                                            color="#fff"
                                            contents={this.state.contents}
                                        />
                                    </Row> */}
                                    <Row gutter={24}>
                                        {userVideos.map((video, index) => (
                                            <Col key={video.id} xxl={8} xl={8} lg={8} md={8} sm={12} xs={12}>
                                                <ContentItem
                                                    index={index + 1}
                                                    voted={me.isLoggedIn && _.includes(video.voted, me.data.id)}
                                                    onVote={() => this.handleUserVote(video)}
                                                    video={video}
                                                />
                                            </Col>
                                        ))}
                                    </Row>
                                    <Row style={{ textAlign: 'center' }}>
                                        <Link href="/contents">
                                            <a>
                                                <img
                                                    className="easeIn"
                                                    width="200px"
                                                    src="/static/main/shared/button-tat-ca-video.svg"
                                                />
                                            </a>
                                        </Link>
                                    </Row>
                                    <>
                                        <div className="title">
                                            <CustomTitle text="DANH SÁCH VIDEO ĐẠT GIẢI" />
                                        </div>
                                        <Row>
                                            <HondaTitleDropdown
                                                text="Giải thưởng"
                                                onFilterChange={weekId => {
                                                    this.props.rewardsActions.fetchUserRewards(weekId);
                                                }}
                                                skipCurrentWeek={false}
                                            />
                                        </Row>
                                        {!_.isEmpty(userRewards) && userRewards.length === 5 && (
                                            <>
                                                <Row style={{ marginBottom: '3rem' }} gutter={50}>
                                                    {userRewards.slice(0, 2).map(reward => (
                                                        <Col
                                                            key={reward.video.id}
                                                            xxl={12}
                                                            xl={12}
                                                            lg={12}
                                                            md={12}
                                                            sm={24}
                                                            xs={24}
                                                        >
                                                            <PrizeItem
                                                                thumbnail={getMediaURL(reward.video.thumbnail)}
                                                                link={getMediaURL(reward.video.path)}
                                                                majorText={reward.video.user.name}
                                                                minorText={PRIZE_TITLE[reward.place]}
                                                                voteCount={reward.video.voteCount}
                                                                voted={
                                                                    me.isLoggedIn &&
                                                                    _.includes(reward.video.voted, me.data.id)
                                                                }
                                                            />
                                                        </Col>
                                                    ))}
                                                </Row>
                                                <Row gutter={20}>
                                                    {userRewards.slice(2).map(reward => (
                                                        <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={24}>
                                                            <PrizeItem
                                                                thumbnail={getMediaURL(reward.video.thumbnail)}
                                                                link={getMediaURL(reward.video.path)}
                                                                majorText={reward.video.user.name}
                                                                minorText={PRIZE_TITLE[reward.place]}
                                                                voteCount={reward.video.voteCount}
                                                                rank={3}
                                                                voted={
                                                                    me.isLoggedIn &&
                                                                    _.includes(reward.video.voted, me.data.id)
                                                                }
                                                            />
                                                        </Col>
                                                    ))}
                                                </Row>
                                            </>
                                        )}
                                    </>

                                    {/* <Row>
                                        <HondaTitleDropdown text="Giải chung cuộc" />
                                    </Row>

                                    <Row type="flex" justify="center">
                                        <Col xxl={14} xl={14} lg={14} md={14} sm={20} xs={24}>
                                            <PrizeItem
                                                thumbnail="/static/main/pc/hd-test.jpg"
                                                link="https://google.com"
                                                majorText="Trần Hoàn Lê"
                                                minorText="Giải nhì"
                                            />
                                        </Col>
                                    </Row> */}
                                </div>
                            </section>

                            <section
                                className="term"
                                ref={ref => {
                                    this.termRef = ref;
                                }}
                            >
                                <div
                                    className="borderTop"
                                    ref={ref => {
                                        this.termBorderTopRef = ref;
                                    }}
                                />
                                <div className="termBackground" />
                                <div className="termBox">
                                    <div className="title">
                                        <CustomTitle text="Thể lệ cuộc thi" />
                                    </div>

                                    <div className="termContent">
                                        <h3>1. GIỚI THIỆU CHUNG:</h3>
                                        <div>
                                            <p>
                                                Honda Wave RSX mới với thiết kế trẻ trung và cá tính hơn, bổ sung nhiều
                                                tiện ích sẽ không chỉ là phương tiện đi lại, mà còn là một người bạn
                                                đồng hành cùng cá tính của bạn. Chất riêng của bạn là gì? Hãy để Honda
                                                Wave RSX giúp bạn thể hiện chất riêng trọn vẹn nhất.
                                            </p>
                                            <p>
                                                Từ ngày 10/9/2019 - 22/10/2019, Honda Việt Nam hân hạnh mang đến cho bạn
                                                cuộc thi Sáng tạo video cùng xe Honda Wave RSX - Rực Sức Trẻ, Xứng Đam
                                                Mê.
                                            </p>
                                            <p>
                                                Cách tham gia rất đơn giản, truy cập website
                                                https://www.honda-rsx.com.vn, sáng tạo video của bạn với ứng dụng Camera
                                                tích hợp công nghệ AR (Thực tế ảo tương tác) đăng tải trên website và
                                                kêu gọi bạn bè cùng tham gia bình chọn để đạt các giải thưởng giá trị.
                                            </p>
                                        </div>
                                        <Row style={{ textAlign: 'center' }}>
                                            <Link href="/terms">
                                                <a>
                                                    <img
                                                        className="easeIn"
                                                        width="200px"
                                                        src="/static/main/shared/button-xem-them.svg"
                                                    />
                                                </a>
                                            </Link>
                                        </Row>
                                    </div>
                                </div>
                                <div
                                    className="borderBottom"
                                    ref={ref => {
                                        this.termBorderBottomRef = ref;
                                    }}
                                />
                            </section>

                            <section className="login" ref={ref => (this.loginRef = ref)}>
                                <div className="loginBackground" />
                                <div className="loginBox">
                                    {flowStage === 0 && this._renderLogInForm()}
                                    {flowStage === 1 && this._renderJoinNowForm()}
                                    {flowStage === 3 && this._renderUpdateInfoForm()}
                                    {flowStage === 4 && this._renderUploadVideo()}
                                    {/* {this._renderUploadVideo()} */}
                                </div>
                            </section>

                            <section
                                className="footer"
                                ref={ref => {
                                    this.footerRef = ref;
                                }}
                                style={{
                                    clipPath: this.state.footerPolygon,
                                    WebkitClipPath: this.state.footerPolygon,
                                }}
                            >
                                <div className="footerBackground" />
                                <div
                                    className="borderTop"
                                    ref={ref => {
                                        this.termBorderTopRef = ref;
                                    }}
                                />

                                <div className="footerBox">
                                    <Row type="flex" justify="space-between" gutter="10">
                                        <Col xxl={3} xl={3} lg={2} md={2} sm={2} xs={4}>
                                            <div className="footerColumn">
                                                <img src="/static/main/shared/logo-honda.png" />
                                            </div>
                                        </Col>

                                        <Col xxl={21} xl={21} lg={22} md={22} sm={22} xs={20}>
                                            <Col xxl={18} xl={18} lg={18} md={18} sm={24} xs={24}>
                                                <div className="footerColumn">
                                                    <h3>
                                                        <span style={{ marginRight: '30px' }}>HONDA WAVE RSX 2019</span>
                                                        <span>Hotline: 0909298235</span>
                                                    </h3>
                                                    <ul>
                                                        <li>
                                                            <Link href={ROUTES.HOME}>
                                                                <a>TRANG CHỦ</a>
                                                            </Link>
                                                        </li>
                                                        <li>
                                                            <div
                                                                onClick={() =>
                                                                    scrollToComponent(this.prizesRef, { offset: 0 })
                                                                }
                                                            >
                                                                <a>GIẢI THƯỞNG</a>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div
                                                                onClick={() =>
                                                                    scrollToComponent(this.processRef, { offset: 0 })
                                                                }
                                                            >
                                                                <a>CUỘC THI SÁNG TẠO VIDEO</a>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <Link href={ROUTES.CONTENTS}>
                                                                <a>VIDEO DỰ THI</a>
                                                            </Link>
                                                        </li>
                                                        <li>
                                                            <Link href={ROUTES.TERMS}>
                                                                <a>THỂ LỆ</a>
                                                            </Link>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </Col>
                                            <Col xxl={6} xl={6} lg={6} md={6} sm={24} xs={24}>
                                                <div className="footerColumn">
                                                    <ul>
                                                        <li>
                                                            <img src="/static/main/shared/qr-code.jpg" />
                                                        </li>
                                                        <li>www.honda-rsx.com.vn</li>
                                                    </ul>
                                                </div>
                                            </Col>
                                        </Col>
                                    </Row>
                                </div>
                            </section>
                        </div>
                        {isFirstLoad === true && <div className="overlay" />}
                    </div>
                </MainLayout>

                <HondaPopupModal
                    visible={this.state.isVisibleChooseColorModal}
                    onOk={this.closeChooseColorModal}
                    onCancel={this.closeChooseColorModal}
                    footer={null}
                    classname="chooseColor"
                    title="Chọn màu xe bạn thích"
                    content={
                        <>
                            <div className="containBox">
                                <a href={AR_URL}>
                                    <img src="./static/main/shared/rsx-red.png" />
                                    <p>Màu đỏ</p>
                                </a>

                                <a href={AR_URL_2}>
                                    <img src="./static/main/shared/rsx-gray.png" />
                                    <p>Màu xám</p>
                                </a>
                            </div>
                        </>
                    }
                />

                <HondaPopupModal
                    visible={this.state.visibleCompleteUpdateInfo}
                    onOk={this.closeCompleteUpdateInfoNotification}
                    onCancel={this.closeCompleteUpdateInfoNotification}
                    classname="completeUpdateInfo"
                    content={
                        <>
                            <Row>
                                <Col>
                                    <Icon type="check-circle" style={{ color: '#009A07', fontSize: '6rem' }} />
                                </Col>
                            </Row>
                            <b>Bạn đã đăng ký tài khoản thành công!</b>
                            <h3>Hãy Tạo video của bạn</h3>
                            {/* <a className="redCharacters">Tạo video của bạn</a> */}
                        </>
                    }
                />

                <UpdateUserInfoModal
                    visible={this.state.visibleModalRequestData}
                    onOk={this.handleOk}
                    onCancel={this.handleaCancel}
                    form={form}
                />

                <HondaPopupModal
                    // title="Basic Modal"
                    visible={this.state.isVisibleUploadModal}
                    onCancel={this.closeUploadModal}
                    onOk={this.closeUploadModal}
                    footer={null}
                    content={
                        <div className="uploadModal">
                            <Icon type="check-circle" />
                            <b>Bạn đã đăng tải video thành công!</b>
                            <br />
                            <a href={SITE_URL + 'content?videoId=' + this.state.mostRecentVideo}>
                                {SITE_URL + 'content?videoId=' + this.state.mostRecentVideo}
                            </a>
                            <p>
                                Chia sẻ video của bạn về trang cá nhân cùng hashtag:
                                <br /> #HondaRSX #RucsuctreXungdamme #RisingXpression.
                            </p>
                            <p>
                                Đừng quên kêu gọi bạn bè bình chọn cho video của bạn trên website của chương trình nhé!
                            </p>
                        </div>
                    }
                />

                {this.state.isFirstLoad && <div className="overlay" />}
                {this.state.locking && (
                    <div className="locking">
                        <p>
                            Đang đăng nhập...
                            <br />
                            <Icon type="loading" />
                        </p>
                    </div>
                )}

                <HondaPopupModal
                    visible={this.state.visibleLoginModal}
                    onOk={this.closeLoginModal}
                    onCancel={this.closeLoginModal}
                    title="Đăng nhập"
                    classname="loginBox"
                    content={
                        <>
                            <Row type="flex" justify="center">
                                <Col span={24}>
                                    <p>Bạn phải đăng nhập trước khi bình chọn.</p>
                                </Col>
                                <Col xxl={24} xl={24} lg={24} md={24} sm={24} xs={24}>
                                    <div className="loginButton facebook" onClick={this.onLoginFacebook}>
                                        <div className="icon">
                                            <img src="/static/main/shared/fb-icon.png" />
                                        </div>
                                        <div className="text">
                                            Đăng nhập bằng <b>Facebook</b>
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                        </>
                    }
                />

                <HondaPopupModal
                    visible={this.state.visibleUpdateInfoModal}
                    onOk={() => this.setState({ visibleUpdateInfoModal: false })}
                    onCancel={() => this.setState({ visibleUpdateInfoModal: false })}
                    title="Nhập thông tin liên hệ"
                    content={
                        <>
                            <Row type="flex" justify="start">
                                <Col span={24} style={{ textAlign: 'left' }}>
                                    <p>
                                        Bình chọn phong cách mà bạn yêu thích sẽ quay số trúng thưởng hàng tuần, vui
                                        lòng nhập thông tin để chúng tôi liên hệ trong trường hợp bạn trúng giải trước
                                        khi tiến hành bình chọn.
                                    </p>
                                </Col>
                                <Col
                                    xxl={6}
                                    xl={6}
                                    lg={6}
                                    md={6}
                                    sm={6}
                                    xs={10}
                                    style={{ textAlign: 'left', margin: '0.5rem 0' }}
                                >
                                    <p>Họ & Tên</p>
                                </Col>
                                <Col xxl={18} xl={18} lg={18} md={18} sm={18} xs={14} style={{ margin: '0.5rem 0' }}>
                                    <Input onChange={e => this.setState({ name: e.target.value })} />
                                </Col>

                                <Col
                                    xxl={6}
                                    xl={6}
                                    lg={6}
                                    md={6}
                                    sm={6}
                                    xs={10}
                                    style={{ textAlign: 'left', margin: '0.5rem 0' }}
                                >
                                    <p>Số Điện Thoại</p>
                                </Col>
                                <Col xxl={18} xl={18} lg={18} md={18} sm={18} xs={14} style={{ margin: '0.5rem 0' }}>
                                    <Input onChange={e => this.setState({ phone: e.target.value })} />
                                </Col>

                                <Col
                                    xxl={6}
                                    xl={6}
                                    lg={6}
                                    md={6}
                                    sm={6}
                                    xs={10}
                                    style={{ textAlign: 'left', margin: '0.5rem 0' }}
                                >
                                    <p>Email</p>
                                </Col>
                                <Col xxl={18} xl={18} lg={18} md={18} sm={18} xs={14} style={{ margin: '0.5rem 0' }}>
                                    <Input onChange={e => this.setState({ email: e.target.value })} />
                                </Col>

                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={10} style={{ margin: '0.5rem 0' }}></Col>
                                <Col
                                    xxl={18}
                                    xl={18}
                                    lg={18}
                                    md={18}
                                    sm={18}
                                    xs={14}
                                    style={{ textAlign: 'right', margin: '0.5rem 0' }}
                                >
                                    <Button onClick={this.onUpdateUserProfile} className="updateInfoSendButton">
                                        Gửi
                                    </Button>
                                </Col>
                                {this.state.isInvalidData && (
                                    <Col span={24}>
                                        <i style={{ color: 'red' }}>* Tất cả thông tin là bắt buộc.</i>
                                    </Col>
                                )}
                            </Row>
                        </>
                    }
                />
            </div>
        );
    }
}

const index = Form.create()(Index);

export default withRouter(
    connect(
        state => ({
            me: state.me,
            kolVideos: state.kolVideos.data,
            userVideos: state.userVideos.data,
            kolRewards: state.rewards.kol.data,
            userRewards: state.rewards.user.data,
        }),
        dispatch =>
            channingActions(
                {},
                dispatch,
                bindMeActions,
                bindKOLVideosActions,
                bindUserVideosActions,
                bindRewardsActions,
            ),
    )(index),
);
