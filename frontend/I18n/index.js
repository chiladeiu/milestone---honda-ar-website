import vi from './languages/vi';
import en from './languages/en';

import i18next from 'i18next';

export const initI18n = () => i18next.init({
    lng: 'en', // language to use
    fallbackLng: 'en',
    interpolation: {
        escapeValue: false
    },
    resources: {
        en: {
            translation: en // 'common' is our custom namespace
        },
        vi: {
            translation: vi
        }
    }
});
