import moment from 'moment';

export const IS_STDIO = false;
// export const IS_DEV = process.env.NODE_ENV === 'development';
export const IS_DEV = false;

export const SITE_NAME = 'RSX Honda';
export const SITE_URL = 'https://www.honda-rsx.com.vn/';

// ############
// FIREBASE CONFIG
// ############
export const PROJECT_CODE = IS_STDIO ? 'honda-ar-ac35d' : 'honda-wave-rsx';
export const API_URL = IS_DEV
    ? `http://localhost:5000/${PROJECT_CODE}/asia-east2/`
    : `https://asia-east2-${PROJECT_CODE}.cloudfunctions.net/`;
export const STORAGE_URL = `https://firebasestorage.googleapis.com/v0/b/${PROJECT_CODE}.appspot.com/o/`;

export const FIREBASE_CONFIG = IS_STDIO
    ? {
        apiKey: 'AIzaSyCns45BIAR0xXZ7W3WWWLNZ9p1RaGIfOsM',
        authDomain: 'honda-ar-ac35d.firebaseapp.com',
        databaseURL: 'https://honda-ar-ac35d.firebaseio.com',
        projectId: 'honda-ar-ac35d',
        storageBucket: 'honda-ar-ac35d.appspot.com',
        messagingSenderId: '805837778807',
        appId: '1:805837778807:web:5b7f411b4f30e1d4',
    }
    : {
        apiKey: 'AIzaSyArgovTKyy0YwpBrMi4w5Cno1oS5qrjMoQ',
        authDomain: 'honda-wave-rsx.firebaseapp.com',
        databaseURL: 'https://honda-wave-rsx.firebaseio.com',
        projectId: 'honda-wave-rsx',
        storageBucket: 'honda-wave-rsx.appspot.com',
        messagingSenderId: '749018799155',
        appId: '1:749018799155:web:df56afb48d0b1568',
    };
// ############
// FIREBASE CONFIG END
// ############

export const AR_URL = 'https://www.facebook.com/fbcameraeffects/tryit/2835776059784862/';
export const AR_URL_2 = 'https://www.facebook.com/fbcameraeffects/tryit/2206494026116621/';

export const MISTHY_VIDEO =
    'https://firebasestorage.googleapis.com/v0/b/honda-wave-rsx.appspot.com/o/kols%2Fmisthy.mp4?alt=media';

export const MAIN_METAIMAGE = 'https://www.honda-rsx.com.vn/static/metaimage.png';

export const LOGIN_URL = `${API_URL}auth/login`;

export const FB_APPID = '479552689258009';
export const GA_ID = '';

export const ROUTES = {
    HOME: '/',
    TERMS: '/terms',
    CONTENTS: '/contents',
    CONTENT: '/content',
    VIDEO_PLAYER: '/videoplayer',

    CMS_AUTH: '/cms/auth',
    CMS_ADMINS_CONTENTS: '/cms/admins/contents',
    CMS_ADMINS_USERS: '/cms/admins/users',
    CMS_ADMINS_KOLS: '/cms/admins/kols',
    CMS_ADMINS_CONTENTS_RANKS: '/cms/admins/contents-ranks',
    CMS_ADMINS_KOLS_RANKS: '/cms/admins/kols-ranks',
};

export const ROLES = {
    ADMIN: 'admin',
    SUPERADMIN: 'superadmin',
    USER: 'user',
};

export const ZALO_CONFIG = {
    appId: '2970099256358239421',
    appSecret: 'ALn1Q7GT9e5OjYNOGN95',
    redirectUrl: `${SITE_URL}zlcb`,
};

export const FIREBASE_TOKEN_KEY = 'w4z325qXBt';

export const DEFAULT_USER_FETCH_LIMIT = 6;
export const MOST_VOTED_FETCH_LIMIT = 6;

export const EVENT_BEGIN_DATE = 1568080800;
export const WEEK_IN_SECONDS = 604800;
export const MAX_WEEK = 6;
export const WEEK_COUNT = Math.min(Math.floor((moment().unix() - EVENT_BEGIN_DATE) / WEEK_IN_SECONDS) + 1, MAX_WEEK);
export const TIMEZONE_DIFF = 2520; // VN at GMT +7
export const TIMEZONE = 'Asia/Ho_Chi_Minh';

export const WEEK_DATA = [...new Array(WEEK_COUNT)].map((x, idx) => ({
    start: EVENT_BEGIN_DATE + idx * WEEK_IN_SECONDS,
    end: EVENT_BEGIN_DATE + (idx + 1) * WEEK_IN_SECONDS,
}));

// console.log(WEEK_DATA);
// console.log(WEEK_COUNT);

export const CURRENT_WEEK_ID = currentTimestamp =>
    WEEK_DATA.findIndex(week => week.start <= currentTimestamp && currentTimestamp < week.end);

export const PRIZE_TITLE = ['', 'Nhất', 'Nhì', 'Ba', 'Ba', 'Ba'];

// CMS Role
export const CMSRole = {
    Viewer: 'viewer',
    Admin: 'admin',
};
